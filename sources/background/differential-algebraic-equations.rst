.. _background-dae:

================================
Differential Algebraic Equations
================================

A differential algebraic equation (:term:`DAE`) is a collection of differential relations and
algebraic constraints. As mentioned in :ref:`introduction-modelling_and_computation` the :term:`DAE`
framework is well suited for systems modelling, and in the words of Stephen L. Campbell,
:cite:p:`Campbell2015`:

  | One major reason given for the usefulness of DAEs is that they are the initial way that many complex systems are most naturally modeled. This is especially true in chemical, electrical, and mechanical engineering and with models formed by interconnecting various submodels.

:term:`DAE`\ s have seen application in modelling chemical processes, electrical circuits, multibody
systems and more (:cite:p:`Daoutidis2014`, :cite:p:`Riaza2013`, :cite:p:`Arnold2017`,
:cite:p:`Jansen2014`). Considering the similarities between the 0-dimensional tunnel networks of
hydropower production planning and circuit modelling, and that the goal of the report is to model
multiple bodies of water moving through these networks, and finally that the model is simply a
subcomponent of the larger watercourse system, the :term:`DAE` would seem like good fit. Here I give
a short introduction, and restate the definitions needed for the definition of the hybrid automaton,
which is discussed in :ref:`background-hst`. See :cite:p:`Brenan1996` or
:cite:p:`Kunkel2006` for a thorough treatment of :term:`DAE`\ s, or :cite:p:`Simeon2017` for a
historical tour. I will follow :cite:p:`Hindmarsh2005` and use :math:`x_d` to refer to the
differential variables, and :math:`x_a` to refer to the algebraic variables.

.. target:: definition-dae
   :label: Definition 2.2.1 (Differential Algebraic Equations)

**Definition 2.2.1 (Differential Algebraic Equations)**:

  A system of differential algebraic equations is a set of equations:

  .. math::

     F(t,x,\dot{x}) = 0 

  Where:

  - :math:`F \colon \mathbb{I} \times \mathbb{D}_x \times \mathbb{D}_{\dot{x}} \to \mathbb{C}^m`
  - :math:`\mathbb{I} \subseteq \mathbb{R}` is a (compact) interval;
  - :math:`\mathbb{D}_x,\mathbb{D}_{\dot{x}} \subseteq \mathbb{C}^n` are open;
  - :math:`m,n \in \mathbb{N}`.

The definition in :cite:p:`Kunkel2006` of the :term:`DAE` in its most general form was restated in
:ref:`definition-dae`, and is a daunting formalism. A :term:`DAE` can be classified according to its
to its shape as in :ref:`definition-dae-regular`. Note that the definition of regularity used in
this report is a superficial classification, and does not imply anything about the solvability. In
:cite:p:`Kunkel2006` regularity is defined in opposition to singularity which is a much more useful
classification. :term:`DAE`\ s can also be classified according to the structure and complexity of
:math:`F`. Some common forms are tabulated in :numref:`table-background-dae-morphology`, which was
reproduced from :cite:p:`Castell1998`. A :term:`DAE` that was real, regular, nonlinear,
semi-explicit and linear in its derivative, and would take the form of equation
:eq:`equation-dae-nseld`.

.. math::
   :label: equation-dae-nseld

   \dot{x}_d &= f(t,x_d,\dot{x}_d,x_a) \\
   0 &= g(t,x_d,x_a)

.. target:: definition-dae-regular
   :label: Definition 2.2.2 (The regularity of a DAE)

**Definition 2.2.2 (The regularity of a DAE)**

  A :term:`DAE` is regular if :math:`m = n`, and irregular otherwise.

Before even doing a computation, it is interesting to know whether or not the computation can be
expected to give a sensible result. An important quality of a :term:`DAE`\ s in this regard is its
solvability, which is treated in for example :cite:p:`Brenan1996` and :cite:p:`Kunkel2006`. A
solvable :term:`DAE` guarantees that a solution exists, and it is up to the computation to find
it. Unique solvability guarantees that the solution is unambiguous. The approach taken in this report
is one of practical computation in the context of system modelling, and Cellier's perspective on "The
Solvability Issue" in :cite:p:`Cellier2006` bears repeating:

  | To us, solvability is a non–issue. It is the typical worry of a mathematician who puts the mathematical formulation first, and then tries to interpret the ramifications of that formulation. ... Saying that a DAE is unsolvable is equivalent to saying that the phenomenon described by it is “defying causality” in the sense that the outcome of an experiment is non–deterministic, which in turn is almost equivalent to saying that the phenomenon is non–physical.

This does not detract from the usefulness of solvability. It would be ideal to build models that are
uniquely solvable by construction, but this still needs to be shown, and accidents happen. Perhaps
the modeller has made some unfortunate modelling decision, like choosing an ill-suited set of
variables, as is the case in the pendulum model discussed in :cite:p:`Cellier2006`. Diagnosing
unsolvable or ambiguous models as often, and as early as possible is particularly useful in a
context of system modelling whose framewroks make it easy to accidentally formulate a non-physical
model. Kunkel's definition of solvability in :cite:p:`Kunkel2006` is reproduced here in
:ref:`definition-dae-solvability`, where a problem is solvable if it has at least one solution. In
:cite:p:`Brenan1996` solvability implies uniqueness of solution. The latter definition might be more
in line with Cellier's remark. Solvability is tied to the initial value problem, which imposes an
initial constraint on :math:`x`:

.. math::
   :label: equation-dae-init

   x(t_0) = x_0

.. target:: definition-dae-solvability
   :label: Definition 2.2.3 (The solvability of a DAE)

**Definition 2.2.3 (The solvability of a DAE)**:

  Let :math:`C^k(\mathbb{I},\mathbb{C}^n)` denote the vector space of all :math:`k`-times
  continuously differentiable functions from the real interval :math:`\mathbb{I}` into the complex
  vector space :math:`\mathbb{C}^n`.

  - A function is called a solution of :ref:`definition-dae` if the :term:`DAE` is satisfied pointwise.
  - The function :math:`x \in C^1(\mathbb{I},\mathbb{C}^n)` is called a solution of the initial value problem with
    initial conditions :math:`x_0`, if it furthermore satisfies equation :eq:`equation-dae-init`.
  - An initial condition is called consistent with :math:`F`, if the associated initial value
    problem has at least one solution.

.. _table-background-dae-morphology:

.. table:: Morphology of common forms of :term:`DAE` 

   +---------+--------------+-----------------+-----------------------------------------------------------+
   |Linear   |Semi explicit |Constant         || :math:`\dot{x}_d + B_{11}x_d + B_{12}x_a = f_1(t)`       |
   |         |              |                 || :math:`B_{21}x_d + B_{22}(t)x_a = f_2(t)`                |
   |         |              +-----------------+-----------------------------------------------------------+
   |         |              |Variable time    || :math:`\dot{x}_d+B_{11}(t)x_d + B_{12}(t)x_a = f_1(t)`   |
   |         |              |                 || :math:`B_{21}(t)x_d+B_{22}(t)x_a = f_2(t)`               |
   |         +--------------+-----------------+-----------------------------------------------------------+
   |         |Fully implicit|Constant         |:math:`A\dot{x} + Bx = f(t)`                               |
   |         |              +-----------------+-----------------------------------------------------------+
   |         |              |Variable time    |:math:`A(t)\dot{x} + B(t)x = f(t)`                         |
   +---------+--------------+-----------------+-----------------------------------------------------------+
   |Nonlinear|Semi explicit |General          || :math:`f(t,x_d,x_a,\dot{x_d}) = 0`                       |
   |         |              |                 || :math:`0 = g(t,x_d,x_a)`                                 |
   |         |              +-----------------+-----------------------------------------------------------+
   |         |              |Linear derivative|| :math:`\dot{x}_d = f(t,x_d,x_a)`                         |
   |         |              |                 || :math:`0 = g(t,x_d,x_a)`                                 |
   |         +--------------+-----------------+-----------------------------------------------------------+
   |         |Fully implicit|General          |:math:`f(t,\dot{x},x) = 0`                                 |
   |         |              +-----------------+-----------------------------------------------------------+
   |         |              |Linear derivative|:math:`A(t,x)\dot{x} + f(t,x) = 0`                         |
   +---------+--------------+-----------------+-----------------------------------------------------------+

.. raw:: latex

   \clearpage

The solvability of a :term:`DAE` can be investigated in terms "the indices" of a :term:`DAE`, like
the differentiation index, perturbation index, tractability index and more (see :cite:p:`Gear1990`
for a through treatment of indices). Kunkel remarks on the plethora of these classifiers in
:cite:p:`Kunkel2006`:

  | Unfortunately, the simultaneous development of the theory in many different research groups has led to a large number of slightly different existence and uniqueness results, particularly based on different concepts of the so-called index. The general idea of all these index concepts is to measure the degree of smoothness of the problem that is needed to obtain existence and uniqueness results.

An index is an indicator of how difficult a :term:`DAE` is to solve. In this report only the
differentiation will briefly be discussed, as the computational software used specifies the
requirements on input :term:`DAE`\ s, for certain computations, in terms of such an index. In
particular the computation of consistent initial conditions in :cite:p:`Hindmarsh2005` is restricted
to semi-explicit index-one models.

.. target:: definition-dae-index
   :label: Definition 2.2.4 (The Differentiation Index of a DAE)

**Definition 2.2.4 (The Differentiation Index of a DAE)**:
  
  The minimum number of times that all or part of :ref:`definition-dae` must be differentiated, with
  respect to :math:`t`, in order to determine :math:`\dot{x}` as a continuous function of :math:`x`
  and :math:`t`, is the index of the DAE. An index :math:`0` model is called an implicit
  :term:`ODE`, and the :term:`ODE`-form of a :term:`DAE`, is called the underlying :term:`ODE` of
  the :term:`DAE`.

**Example 2.2.1 (The Differentiation Index of a DAE)**

  Consider a regular :term:`DAE` with :math:`n = 3`, and:

  .. math::

    F = \begin{cases}
    \dot{x}_0 - f(x_1,x_2) \\
    \dot{x}_1 - g(x_0) \\
    x_1 - x_2 
    \end{cases}

  It is not possible to determine :math:`\dot{x}` as a function of :math:`x` and :math:`t`, as
  :math:`\dot{x}_2` does not appear in any of the 3 equations; :math:`\frac{\partial F}{\partial
  \dot{x}}` is singular. Differentiating the last equation yields, :math:`\dot{x}_1 + \dot{x}_2 =
  0`, this new :math:`\frac{\partial F}{\partial \dot{x}}` is no longer singular. The :term:`DAE`
  can be formulated as an :term:`ODE` with simple algebraic manipulation:

  .. math::

     \dot{x}_0 &= f(x_1,x_2) \\
     \dot{x}_1 &= g(x_0) \\
     \dot{x}_2 &= -g(x_0)

  The differentiation index of :math:`F` was thus 1.

.. _background-dae-simulation:

Simulation of a DAE
-------------------

While a :term:`DAE` is an expressive formalism, and as such is less computationally tractable than
more specialised formalisms, practical computation is doable and there exists open-source
computational software for this purpose. One example is IDA in SUNDIALS from
:cite:p:`Hindmarsh2005`, which solves the initial value problem of a regular :term:`DAE`. When
formulating the initial value problem for a :term:`DAE`, one must ensure that the initial values
satisfy the algebraic constraints of the model, :cite:p:`Pantelides1988`:

  | The initial values for variables in mixed Differential-Algebraic (DAE) systems must satisfy not only the original equations in the system but also their differentials with respect to time.

And the problem of finding such values is called the consistent initialisation problem, :cite:p:`Petzold1991`:

  | ... given specified information about the initial state of the problem that is sufficient to specify a unique solution to a DAE, determine the complete initial vector :math:`(x(t_0), \dot{x}(t_0))` corresponding to this unique solution.

IDA can compute consistent initial conditions for semi-explicit index-one models based on an initial
guess; it computes :math:`\dot{x}_d(t_0)` and :math:`x_a(t_0)`, given :math:`x_d(t_0)` and a guess
:math:`x_a(t_0)`. This capability makes practical computation more ergonomic, in particular when one
is working with :term:`HA` whose initial value problems involves zero or more :term:`DAE` initial
value problems. There also exists algorithmic techniques for transforming :term:`DAE`\ s into
:term:`ODE`\ s, see for example :cite:p:`Otter2017` or :cite:p:`Cellier2006`. The :term:`ODE` can in
turn be solved by one of the many open-source software packages available for :term:`ODE`\ s.

.. raw:: latex
  
  \clearpage

..  LocalWords:  Riaza Brenan Kunkel Simeon Hindmarsh mathbb subseteq numref Castell nseld Cellier
..  LocalWords:  Cellier's Kunkel's init pointwise eq clearpage frac Pantelides Petzold
