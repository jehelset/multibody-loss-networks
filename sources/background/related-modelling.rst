.. _background-related_modelling:

=================
Related Modelling
=================

Hydropower Plant Models
-----------------------

The computational models found in the context of production planning of hydropower production are
often 0-dimensional in the sense of :cite:p:`Yang2019`; their units (and the other elements of the
tunnel system) are modelled as vertices or edges in a graph, and their variables are defined in
terms of said graph. The structure of the graph is often constrained according to some typical
hydropower plant taxonomy, usually some variation of the turnip-like structure discussed in
:ref:`introduction-tunnel_systems`. :cite:p:`Kishor2007` classifies hydropower plant models in terms
of the complexity of their continuous dynamics (linearity and elasticity of water columns), as well
as the types of components that make up the plant (whether or not the topology includes surge
tanks), and assumes a fixed topology.

Short-term Hydro Optimisation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An example of a computational model of a hydropower plant is found in :cite:p:`Skjelbred2019`, which
contains a computational model for short-term optimisation of production plans. The computational
model is an optimisation problem (a :term:`MILP`) defined in terms of the discretised steady-state
dynamics of the tunnel system. This report is not concerned with economic optimisation or optimal
control, only the representation of the topological structure of the tunnel system is of
interest. This model assumes turnip-like topology, and separates the tunnel system of the plant from
the tunnel system of the upstream reservoir-tree. Plants are considered in isolation, and their
tunnel systems are divided into 4 parts, as seen in
:numref:`figure-background-similar-models-0`. These are the main tunnel, the penstocks, the units
and the outlet. It is assumed that the graph of the hydropower plant has exactly one sink and one
source, and that the topology can be decomposed in an upstream and downstream tree, whose leaves are
the units of the plant. The two resulting computational models of the reservoir-tree and the plant
are later combined to construct the computational model of the entire tunnel system. The two
separate models are solved in turn, until a satisfactory output from both are achieved. It is also
assumed that there is a single body of water in the entire tunnel system.

.. graphviz::
   :align: center
   :caption: Fixed plant topology with 4 parts.
   :name: figure-background-similar-models-0
           
   digraph {
       rankdir = LR
       node [label="",fixedsize=true,shape="box",style="filled"]
       edge [arrowsize=0.3]

       subgraph cluster_0{
         bgcolor = "cornsilk"
	 label = "Main tunnel"
	 tunnel_0[width=0.4,height=0.1,fillcolor="grey"]
       }
       subgraph cluster_1{
         bgcolor = "cornsilk"
	 label = "Penstocks"
	 tunnel_1,tunnel_2 [width=0.4,height=0.1,fillcolor="grey"]
       }
       subgraph cluster_2{
         bgcolor = "cornsilk"
	 label = "Units"
	 unit_0,unit_1,unit_2 [width=0.2,height=0.2,shape="diamond",fillcolor="orange"]
       }
       subgraph cluster_3{
         bgcolor = "cornsilk"
	 label = "Outlet"
	 tunnel_3[width=0.4,height=0.1,fillcolor="grey"]
       }

       tunnel_0:e->tunnel_1:w
       tunnel_0:e->tunnel_2:w
       tunnel_1:e->unit_0:w
       tunnel_2:e->unit_1:w
       tunnel_2:e->unit_2:w
       unit_0:e->tunnel_3:w
       unit_1:e->tunnel_3:w
       unit_2:e->tunnel_3:w

   }

Wastewater Control
------------------

In :cite:p:`Ocampo2010` wastewater systems are represented in terms of a typed directed graph which
serves as the basis for constructing a hybrid system theoretical model, in particular an
:term:`MLD`. The wastewater system is decomposed into subsystems, some of which are modelled with
hybrid dynamics. The :term:`MLD` is then used as a basis for :term:`MPC`. Ocampo's wastewater model
leveraged hybrid dynamics to model what is called "temporary flow paths", :cite:p:`Ocampo2010`:

  | "The presence of intense precipitation causes some sewer mains and virtual tanks to surpass their limits. When this happens, any excess above the maximum volume flows to another tank downstream. In this way, temporary flow paths are triggered that depend on the system state and inputs. Since this behaviour is observed in most parts of the sewer network, a modelling methodology is needed that can consider and incorporate overflows and other logical dynamics"

The modelling approach of :cite:p:`Ocampo2010` was one of the initial inspirations of the one
planned in this report, even though this report will use another hybrid system theoretical
framework. The focus in this report is on modelling multiple bodies of water in the tunnel systems
of a hydropower producing watercourse. These bodies can also been seen as defining temporary flow
paths between the reservoirs and rivers that are connected to the tunnel system. One would need to
model the same type of flood behaviour in reservoirs when constructing a model of an entire
watercourse, which is needed in by some of the computations of production planning.

As mentioned the hybrid subsystems of :cite:p:`Ocampo2010` are defined in terms of the vertices of a
graph. There is a one-to-one mapping between subsystem and vertex. The discrete dynamics of a
particular subsystem only affects its own continuous dynamics. In this report however, the discrete
and continuous description of the system is not overlapping. There is no one-to-one mapping between
the elements of the graph and a hybrid subsystem. Instead there is a one-to-many mapping between
elements and discrete variables, and similar for continuous variables. The discrete variables
associated with the elements of the graph, together, define the discrete dynamics of the entire tunnel
system. The discrete state in turn determines the continuous dynamics of the entire tunnel
system. The inclusion or exclusion of a particular continuous relation can be dependent on the
discrete valuation of more than one discrete variable, even though the discrete variables involved
in such a computation are, topologically speaking, close to each other.

Flow Networks
-------------

:cite:p:`Jansen2014` presents a unified modelling approach for what they term flow networks. It
abstracts from the specific content of the flow. A flow can be a current, water, gas, blood and so
on. It represents the system in terms of a directed graph, and then defines the dynamics that govern
the systems with reference to this graph. It also introduces switching elements, vertices in the
flow network that are able to break or connect flow; elements that can be modeled as hybrid
subsystems like in :cite:p:`Ocampo2010`. The approach taken in :cite:p:`Jansen2014` is similar to
the one taken here, even though this report focuses specifically on water-networks. The model
presented here introduces two orthogonal refinements of the models in :cite:p:`Jansen2014`, in
particular it introduces the discrete representation of the direction of flow, as well as multibody
versus single-bodied flow networks. The model in :cite:p:`Jansen2014` is described in terms of
:term:`DAE`, this report instead intends to develop the model within a hybrid system theoretical
framework where only the continuous dynamics of the model are represented in terms of a :term:`DAE`.

Water Networks
--------------

:cite:p:`Jansen2015` establishes global unique solvability for a what is called a quasi-stationary
water network:

  | We assume the water network to be dominated by laminar flows, which allows us to consider the water motion as a one-dimensional flow along the length of the pipes. Furthermore we assume a network with significant time-dependent changes of flow, but without hydraulic shocks.

The modelling approach is similar to the one in :cite:p:`Jansen2014`. The continuous
dynamics of this model are similar to the ones worked towards in this report. In
:cite:p:`Jansen2014` the ports of the model are pressure and demand nodes. The pressure at pressure
nodes and flow through demand nodes are exogenous variables. This report only considers pressure
nodes, which here are modeled both in terms of discrete and continuous variables describing discrete
submersion and continuous pressure. When simulating a full hydropower producing watercourse it is
often useful to model demand nodes. In particular catchment flow can enter a hydropower through
tunnel inserts, and this flow is sometimes modeled by inflow forecasting. When waterbodies in the
tunnel system are explicitly modelled, the need for demand nodes disappears. Instead the demand node
can be modelled as a minuscule reservoir outside of the tunnel system, and in the event that the
tunnel is full, the catchment flow would flood. The results established in :cite:p:`Jansen2015`
would be useful for defining and analysing the continuous dynamics worked towards in this report.

.. raw:: latex
  
  \clearpage

..  LocalWords:  todo multibody Riaza DAEF Kishor Skjelbred al MILP numref rankdir grey
..  LocalWords:  fixedsize arrowsize subgraph bgcolor cornsilk fillcolor Ocampo MPC
