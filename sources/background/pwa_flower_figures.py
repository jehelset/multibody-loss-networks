import numpy as np
import matplotlib.pyplot as plot
import pathlib

plot.rcParams['font.size'] = 14

def figure_path(name):
  try:
    return (pathlib.Path(__file__).parent.absolute() / name).with_suffix('.svg')
  except NameError:
    return (pathlib.Path('sources/background') / name).with_suffix('.svg')

x0 = np.arange(-10, 10.1, 0.1)
x1_max = [ 10.0 for x in x0 ]
x1_min = [ -10.0 for x in x0 ]

def figure0():

  f0 = -x0
  f1 = x0

  figure = plot.figure()
  axes = plot.axes()
  axes.plot(x0,f0, color = 'black')
  axes.fill_between(x0,f0,x1_max,color = '#377eb880')
  axes.plot(x0,f1, color = 'black')
  axes.fill_between(x0,x1_min,f1,color = '#377eb880')
  axes.set_xlabel('$x_0$')
  axes.set_ylabel('$x_1$')

  axes.annotate('$ x_0 + x_1 \geq 0$',xy=(0.0,5.0),horizontalalignment='center')
  axes.annotate('$-x_0 + x_1 \geq 0$',xy=(0.0,-5.0),horizontalalignment='center')
  axes.annotate('$X_0$',xy=(5.0,0),horizontalalignment='center')
  axes.grid()

  axes.set_xlim(-10,10)
  axes.set_ylim(-10,10)

  figure.savefig(figure_path(f'pwa-flower-figure-0'),dpi=600,bbox_inches='tight',format='svg',transparent = True)
  plot.close(figure)
  
figure0()

def figure1():

  A = np.array([[-0.1,5],[-1,-0.1]])

  grid = [ np.linspace(-10.0,10.0,num = 20) for i in range(2) ]

  quiver_grid = np.meshgrid(*grid)
  quiver_data = ([],[])
  for a in range(len(quiver_grid[0])):
    quiver_data[0].append([])
    quiver_data[1].append([])
    for b in range(len(quiver_grid[0][a])):
      x = np.array([quiver_grid[i][a][b] for i in range(2)])
      dx = A.dot(x)
      for k in range(2):
        quiver_data[k][-1].append(dx[k])


  figure = plot.figure()
  axes = plot.axes()
  quiver = plot.quiver(quiver_grid[0],
                       quiver_grid[1],
                       quiver_data[0],
                       quiver_data[1],
                       pivot='mid',
                       color='#ee6e00',width=0.003)
  axes.set_xlabel('$x_0$')
  axes.set_ylabel('$x_1$')
  axes.grid()

  axes.set_xlim(-10,10)
  axes.set_ylim(-10,10)

  figure.savefig(figure_path(f'pwa-flower-figure-1'),dpi=600,bbox_inches='tight',format='svg',transparent = True)
  plot.close(figure)
  
figure1()
