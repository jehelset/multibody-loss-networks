.. _background-hst:

====================
Hybrid System Theory
====================

Hybrid system theoretical models are, roughly speaking, models with both continuous and discrete
dynamics, whose trajectories are interleavings of continuous and discrete trajectories. The
continuous dynamics are at any point in continuous time determined by the discrete state, and the
discrete dynamics are in turn driven by the evolution of the continuous state. Here I give a brief
introduction to some hybrid system theoretical frameworks, and the definitions necessary for the
subsequent modelling. See :cite:p:`Schaft2000` or :cite:p:`Lunze2009` for a more comprehensive
introduction to hybrid system theory.

.. _background-hst-mld:

Mixed Logical Dynamical Systems
-------------------------------

First consider the Mixed Logical Dynamical (:term:`MLD`) from :cite:p:`Bemporad1999`:

.. target:: definition-mld
   :label: Definition 2.3.1 (Mixed Logical Dynamical System)

**Definition 2.3.1 (Mixed Logical Dynamical System)**:

  .. math::

     x(t + 1) &= A_tx(t) + B_{1t}u(t) + B_{2t}\delta(t) + B_{3t}z(t) \\
     y(t) &= C_tx(t) + D_{1t}u(t) + D_{2t}\delta(t) + D_{3t}z(t) \\
     E_{2t}\delta(t) + E_{3t}z(t) &\leq E_{1t}u(t) + E_{4t}x(t) + E_{5t}

  - where :math:`t \in \mathbb{Z}`, :math:`x = \begin{bmatrix} x_c \\ x_l \end{bmatrix}`, :math:`x_c
    \in \mathbb{R}^{n_c}`, :math:`x_l \in \mathbb{B}^{n_l}`, :math:`n \triangleq n_c + n_l` is the
    state of the system;
  - and :math:`y = \begin{bmatrix}y_c \\ y_l \end{bmatrix}`, :math:`y_c \in \mathbb{R}^{p_c}`,
    :math:`y_l \in \{0,1\}^{p_l}`, :math:`p \triangleq p_c + p_l`, is the output vector;
  - and :math:`u = \begin{bmatrix}u_c \\ u_l \end{bmatrix}`, :math:`u_c \in \mathbb{R}^{m_c}`,
    :math:`u_l \in \{0,1\}^{m_l}`, :math:`m \triangleq m_c + m_l` is the command input.
  - :math:`\delta \in \{0,1\}^{r_l}` and :math:`z \in \mathbb{R}^r_c` represent respectively auxiliary logical and continuous variables.

The :term:`MLD` is a discretised-time framework specified with an eye on computational tractability:

  | We restrict the dynamics to be linear and discrete-time in order to obtain computationally tractable control schemes...
  | - :cite:p:`Bemporad1999`

Nevertheless, the discrete variables of the framework can be separated into discrete and discretised
continuous variables that express discrete and continuous relations. The discrete dynamics of the
system models logical relations with algebraic equalities and inequalities (see equations **2a**
through **2f**, and **4a** through **4e** in :cite:p:`Bemporad1999`). This representation is
computationally tractable, but it is not conceptually efficient.

Consider the relation :math:`[ f(x) \leq 0 ] \rightarrow [\delta = 1]`, where :math:`[ f(x) \leq 0
]` is the assertion that :math:`f(x) \leq 0`, and similar for :math:`[\delta = 1]`. It is a very
direct expression of a logical relation. In a :term:`MLD` model it would be represented as
:math:`f(x) \leq \epsilon + (m - \epsilon)\delta`, where :math:`\epsilon` is some small tolerance,
and :math:`m \triangleq \min_{x \in \mathcal{X}} f(x)` for some bounded set :math:`\mathcal{X}`
assumed to contain :math:`x`. This is a more indirect representation, and it is also coupled to the
actual computation, through :math:`\epsilon` which is the machine epsilon. An :term:`MLD` trades
computational tractability for conceptual efficiency, which is only natural for a computational model.

Piecewise Affine Systems
------------------------

A closely related modelling framework is the :term:`PWA`\ s, a continuous time-variant of which is
found in :cite:p:`Rantzer2000`. Its definition is restated in :ref:`definition-pwa`. This is another
computationally tractable model, for which one for example can do stability analysis and optimal
control. In addition it is shown that the framework can be used to approximate smooth non-linear
models up to arbitrary precision. This would be an example of the left-to-right model transformation
discussed in :ref:`introduction-modelling_and_computation`. Here the logical description of the
system takes the form of matrices, representing polyhedral cells that partition the state
space. Each partition has its own continuous linear dynamics. The polyhedral cells are also an
indirect way of expressing logical relations. In :ref:`pwa-example` below, the logical relation
:math:`[ x_0 + x_1 \leq 0 ] \land [ -x_0 + x_1 \leq 0]`, is represented by the matrix:

.. math::

   \begin{bmatrix} 1 & 1 & 0 \\ -1 & 1 & 0 \end{bmatrix}

This is, again, a rather indirect representation. It is a computational detail, and not an essential
aspect of the underlying logic, but it is a suitable representation for this particular
computational model.

.. target:: definition-pwa
   :label: Definition 2.3.2 (Piecewise Affine System)

**Definition 2.3.2 (Piecewise Affine System)**:

  A piecewise affine system is a set of affine systems:

  .. math::

     \begin{cases}
     \dot{x} = a_i + A_ix + B_iu \\
     y = c_i + C_ix + D_iu
     \end{cases} \quad \text{for } x \in X_i

  - where :math:`\{ X_i \}_{i \in I} \subseteq \mathbb{R}^n` partitions the state space into a a
    number of closed (possibly unbounded) polyhedral cells denoted by an index set :math:`I`,
  - and represented in terms of matrices :math:`\bar{E}_i = \begin{bmatrix} E_i & e_i
    \end{bmatrix}`, :math:`\bar{F}_i = \begin{bmatrix} F_i & f_i \end{bmatrix}`;
  - such that :math:`\bar{E}_i \bar{x} \geq 0` where :math:`x \in X_i, i \in I`;
  - and :math:`\bar{F}_i \bar{x} = \bar{F}_j \bar{x}` where :math:`x \in X_i \cap X_j`, :math:`i,j \in I`, and :math:`\bar{x} = \begin{bmatrix} x \\ 1 \end{bmatrix}`.

.. target:: pwa-example
   :label: Example (PWA Flower System)

**Example (PWA Flower System)**

  Here the :term:`PWA` flower system from **Example 1** in :cite:p:`Hedlund1999` is restated. The
  flower system cuts the two-dimensional state space into 4 polytopes:

  .. math:: 

    \begin{split}
      G_0 &= \begin{bmatrix} 1 &  1 & 0 \\ -1 &  1 & 0 \end{bmatrix} \\
      G_1 &= \begin{bmatrix} 1 & -1 & 0 \\  1 &  1 & 0 \end{bmatrix}
    \end{split}
    \quad \quad
    \begin{split}
      G_2 = \begin{bmatrix}-1 & -1 & 0 \\  1 & -1 & 0 \end{bmatrix} \\
      G_3 = \begin{bmatrix}-1 &  1 & 0 \\ -1 & -1 & 0 \end{bmatrix}
    \end{split}

  The dynamics of the system is specified as:

  .. math:: 

     \dot{x} = A_i x 

  .. math::

     A_0 = \begin{bmatrix}-0.1 & 1 \\ -5 & -0.1\end{bmatrix} \quad \quad
     A_1 = \begin{bmatrix}-0.1 & 5 \\ -1 & -0.1\end{bmatrix}

  .. math::

     i = \begin{cases} x \in X_0 \cup X_2 \; \to 0 \\ x \in X_1 \cup X_3 \; \to 1\end{cases}

  The cell defined by :math:`G_0` is shown in :numref:`figure-pwa-flower-0`, and the phase plot
  of the dynamics :math:`A_1` is shown in :numref:`figure-pwa-flower-1`.

.. figure:: pwa-flower-figure-0.svg
   :align: center
   :name: figure-pwa-flower-0

   Shaded polyhedron defined by the two inequalities of :math:`G_0`, :math:`x_0 + x_1 \leq 0` and
   :math:`-x_0 + x_1 \leq 0`.

.. figure:: pwa-flower-figure-1.svg
   :align: center
   :name: figure-pwa-flower-1

   Phase plot of the dynamics of :math:`\dot{x} = A_1 x`, active in :math:`X_1` and :math:`X_3`

Hybrid Automata
---------------

The Hybrid Automaton (:term:`HA`) is an expressive hybrid system theoretical modelling framework
which lets a modeller directly describe complex discrete dynamics and changes between different
modes of behavior in a direct manner. See :cite:p:`Kowalewski2009` for an introduction, or
:cite:p:`Soto2021` and :cite:p:`Alur2019` for some recent applications. In short, the :term:`HA`,
annotates the states and transitions of the finite state automaton from discrete system theory with
continuous constraints, actions, and dynamics. Here, unlike the :term:`MLD` and the :term:`PWA`, the
expressiveness and directness of the framework is paid for in computational tractability,
:cite:p:`DeSchutter2009`:

  | The choice of a modeling framework is a trade-off between two conflicting criteria: the modeling power and the decisive power. The modeling power indicates the size of the class of systems allowing a reformulation in terms of the chosen model description. The decisive power is the ability to prove quantitative and qualitative properties of individual systems in the framework. A model structure that is too broad (like the hybrid automaton) cannot reveal specific properties of a particular element in the model class.

In the same manner that the :term:`DAE` is a natural continuous model for complex systems, the
:term:`HA` is a natural hybrid model for complex systems. A system being initially modeled as a
:term:`HA` does not preclude the construction of simpler, more computationally tractable models,
through the left-to-right model transformations. The focus in this report is more on modelling more
than computation. The modelling framework needs to efficiently express models on the left side of
:numref:`figure-introduction-models` and serve as a basis for constructing other models. In this
context modelling power becomes more important, and the qualities of frameworks like the :term:`HA`
come into focus.

There are multiple variants of the :term:`HA` which differ in for example the specificity of its
continuous dynamics (contrast for example :cite:p:`Schaft2000` with :cite:p:`Lunze2009`), or how the
transitions are modeled. At its most general, the continuous dynamics are specified in terms of a
:term:`DAE`. The :term:`HA` from :cite:p:`Zhang2001` is reproduced in :ref:`definition-ha`.

.. target:: definition-ha
   :label: Definition 2.3.3 (Hybrid Automaton)

**Definition 2.3.3 (Hybrid Automaton)**:

  A hybrid automaton :math:`H` is a collection :math:`H = (Q,X,Init,f,D,E,G,R)`, where

  - :math:`Q` is a finite collection of discrete variables;
  - :math:`X` is a finite collection of continuous variables with :math:`X = \mathbb{R}^N`;
  - :math:`Init \subseteq Q \times X` is a set of initial states;
  - :math:`f \colon Q \times X \to X` is a vector field;
  - :math:`D \colon Q \to \mathcal{P}(X)` is a map assigning to each :math:`q \in Q` a subset :math:`X` called
    the domain of :math:`q`;
  - :math:`E \subset Q \times Q` is a set of edges;
  - :math:`G \colon E \to \mathcal{P}(X)` is a map assigning to each edge :math:`e \in E` a subset of
    :math:`X` called the guard of :math:`e`; and
  - :math:`R \to E \times X \to \mathcal{P}(X)` is a reset map, assigning to each edge :math:`e \in
    E` and each :math:`x \in X` a subset of :math:`X`

Executions of a Hybrid Automaton
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The execution of a :term:`HA` is similar to the concept of a solution of a :term:`DAE` discussed in
:ref:`background-dae`. The executions of a hybrid automaton are interleavings of discrete executions
and continuous solutions (which in this report also will be referred to as executions). Continuous
executions are constructed until the continuous state leaves the domain of the current discrete
state :math:`q`, :math:`D(q)`, and / or enters the edge of a guard :math:`G(e)`, :math:`e \in
E(q)`. This event marks the start of a discrete execution. This discrete execution happens
instantaneously in continuous time. The final state of the discrete execution marks the start of a
new continuous execution. The discrete state is sometimes referred to as the "mode" of the hybrid
system, and the construction of a discrete execution is referred to as "the mode selection problem"
in :cite:p:`Schaft2000`:

  | The problem of finding the next discrete state is called the mode selection problem.

The definitions of time trajectory and execution from :cite:p:`Zhang2001` are reproduced here in
:ref:`definition-ha-time-trajectory` and :ref:`definition-ha-execution`.

.. target:: definition-ha-time-trajectory
   :label: Definition 2.3.4 (Hybrid Time Trajectory)

**Definition 2.3.4 (Hybrid Time Trajectory)**:

  A hybrid time trajectory is a finite or infinite sequence of intervals :math:`\tau =
  \{ I_i \}^N_{i=0}`, such that:

  - :math:`I_i = [\tau_i,\tau^\prime_i]` for all :math:`0 \le i < N`,
  - if :math:`N < \infty` then either :math:`I_N=[\tau_N,\tau^\prime_N]` or :math:`I_N = [\tau_N,\tau^\prime_N)`,
  - :math:`\tau_i \le \tau_i^\prime` for all :math:`i` and :math:`\tau^\prime = \tau_{i+1}` for all :math:`0 \le i < N`.

.. target:: definition-ha-execution
   :label: Definition 2.3.5 (Hybrid Execution)

**Definition 2.3.5 (Hybrid Execution)**:

  An execution of a hybrid automaton :math:`H` is a collection :math:`\chi = (\tau,q,x)` where
  :math:`\tau` is a hybrid time trajectory, :math:`q \colon \langle \tau \rangle \top \mathbf{Q}` is a map, and
  :math:`x = \{ x^i: i \in \langle \tau \rangle \}` is a collection of :math:`C^1` maps :math:`x^i \colon I_i
  \to \mathbf{X}` such that:

  - :math:`(q(0),x^0(0)) \in Init`
  - for all :math:`i \in \langle \tau \rangle` and for all :math:`t \in I_i`, :math:`\dot{x}^i(t) = f(q(i),x^i(t))`
    and for all :math:`t \in [ \tau_i, \tau^\prime_i)`, :math:`x^i(t) \in D(q(i))`,
  - for all :math:`i \in \langle \tau \rangle`, :math:`e = (q(i),q(i+1))\in E`, :math:`x^i(\tau^\prime_i) \in G(e)`, and
    :math:`x^{i+1}(\tau_{i+1}) \in R(e,x^i(\tau^\prime_i))`.

  :math:`\mathcal{E}^M`, :math:`\mathcal{E}^*`, and :math:`\mathcal{E}^\infty` denotes the set
  of all maximal, finite and infinite executions respectively. :math:`\mathcal{E}` denotes the set
  of all executions of :math:`x_0,q_0 \in Init`.

:ref:`definition-ha-execution` sets the stage for the definition of some useful qualitative
properties of a :term:`HA`. These properties are similar to the concept of solvability of a
:term:`DAE` and are important in the context of practical computation, as they can be used to
determine whether or not an execution will be unique and finite. Before continuing on, a simple
example will demonstrate the execution of a :term:`HA`:

.. target:: ha-example
   :label: Example (HA Flower System)

**Example (HA Flower System)**

  Here :ref:`pwa-example` is formulated and simulated as a :term:`HA`. The cell :math:`X_0 \cup X_2`
  is expressed by the relation :math:`x_0^2 \leq x_1^2`, and :math:`X_1 \cup X_3` by the relation
  :math:`x_1^2 \leq x_0^2`. Assuming :math:`A_i` is defined as before, the system on :term:`HA` form
  is:

  - :math:`Q = \begin{bmatrix} q_0 \end{bmatrix}` taking values in :math:`\mathbb{Q} \in \{ 0, 1 \}`
  - :math:`X = \begin{bmatrix} x_0 \\ x_1 \end{bmatrix}` taking values in  :math:`\mathbb{X} \triangleq \mathbb{R}^2`.
  - :math:`Init = \mathbb{Q} \times \mathbb{X}`.
  - :math:`f(q) = \begin{cases}[q_0 = 0] \to A_0 x \\ [q_0 = 1] \to A_1 x \end{cases}`
  - :math:`D(q) = \begin{cases}[q_0 = 0] \to \{ x \in \mathbb{X} \mid x_0^2 \leq x_1^2 \} \\ [q_0 = 1] \to \{ x \in \mathbb{X} \mid x_1^2 \leq x_0^2 \} \end{cases}`
  - :math:`E = \{ (\begin{bmatrix}0\end{bmatrix},\begin{bmatrix}1\end{bmatrix}), (\begin{bmatrix}1\end{bmatrix},\begin{bmatrix}0\end{bmatrix}) \}`
  - :math:`G(e) = \{ x \in X \mid x_0^2 = x_1^2 \}`
  - :math:`R(e) = \emptyset`

  Snapshots of an execution starting out at :math:`x = \begin{bmatrix}1 \\ 0\end{bmatrix}` for this
  :term:`HA` is shown in the figures :numref:`figure-ha-flower-0`, :numref:`figure-ha-flower-1` and
  :numref:`figure-ha-flower-2`. The start, as well as the states, of transitions are marked with a
  small circle. The rays :math:`x_0 = x_1` and :math:`-x_0 = x_1` are drawn in dashed green and
  pink, and the phase plane of the current continuous dynamics are drawn in the background.

.. figure:: ha-flower-figure-0.svg
   :align: center
   :name: figure-ha-flower-0

   The initial state of an execution of the :term:`HA` flower system, at :math:`t = 0.00`, :math:`x =
   \begin{bmatrix} 1.00 \\ 0.00 \end{bmatrix}`, in state :math:`q = \begin{bmatrix} 0 \end{bmatrix}`.

.. figure:: ha-flower-figure-1.svg
   :align: center
   :name: figure-ha-flower-1

   The eventual state of an execution of the :term:`HA` flower system, :math:`x = \begin{bmatrix} -0.56
   \\ 0.25 \end{bmatrix}`, at :math:`t = 2.43`, in :math:`q = \begin{bmatrix} 0 \end{bmatrix}`.


.. figure:: ha-flower-figure-2.svg
   :align: center
   :name: figure-ha-flower-2

   The eventual state of an execution of the :term:`HA` flower system, :math:`x = \begin{bmatrix}
   0.03 \\ 0.09 \end{bmatrix}`, at :math:`t = 24.14`, in :math:`q = \begin{bmatrix} 1
   \end{bmatrix}`.
  
.. _results-hst-determinism:

Determinism of a Hybrid Automaton
"""""""""""""""""""""""""""""""""

Since a hybrid model combines continuous and discrete dynamics into hybrid dynamics, it also
combines the potentials for continuous and discrete nondeterminism. Not only continuous and discrete
nondeterminism in isolation, but also nondeterminism between the discrete and the continuous; hybrid
nondeterminism. The discrete form of determinism in is described in :cite:p:`Hopcroft1979` in the
context of automata theory:

  | The term "deterministic" refers to the fact that on each input there is one and only one state to which the automaton can transition from its current state. In contrast, "nondeterministic" finite automata, the subject of Section 2.3, can be in several states at once.

A deterministic finite automaton in :cite:p:`Hopcroft1979` is a 5-tuple :math:`A =
(Q,\Sigma,\delta,q_0,F)`. A similar automaton can be constructed by considering the discrete
components of the :term:`HA`. Discrete nondeterminism happens during the construction of a hybrid
execution whenever:

.. math::

   \exists e_0,e_1 \in E \times E \mid e_0 \neq e_1 \land x(t) \in G(e_0) \cap G(e_1)

The continuous form of determinism is equivalent to uniqueness of solution. Hybrid nondeterminism,
then, is the ambiguity between the continued construction of the current continuous execution, or
the beginning of a new discrete execution, by taking some discrete transition :math:`e`, where
:math:`x \in G(e)`. It is an ambiguity between the continuous and discrete dynamics of a hybrid
model. The union of these three forms of determinism is captured in :cite:p:`Zhang2003` using the
set of all executions :math:`\mathcal{E}` from :ref:`definition-ha-execution`.

.. target:: definition-ha-determinism
   :label: Definition 2.3.6 (Deterministic Hybrid Automaton)

**Definition 2.3.6 (Deterministic Hybrid Automaton)**:

  A hybrid automaton :math:`H` is called deterministic if :math:`\mathcal{E}^M(q_0,x_0)` contains at
  most one element for all :math:`(q_0,x_0) \in Init`.

.. _background-hst-zenoness:

Zenoness of a Hybrid Automaton
""""""""""""""""""""""""""""""

The definition of the hybrid execution includes infinite executions, and automata that generates
such executions are called nonblocking.
 
**Definition 2.3.7 (Blocking Hybrid Automaton)**:

  A hybrid automaton :math:`H` is called nonblocking if :math:`\mathcal{E}^\infty(q_0,x_0)` is non
  empty for all :math:`(q_0,x_0) \in Init`.

An execution can be infinite without progressing in continuous time. This is property called
zenoness, and is described for example in :cite:p:`DeSchutter2009`:

  | *Zeno behaviour* is the phenomenon that for a dynamical system an infinite number of events occur in a finite length time-interval.

:cite:p:`Zhang2001` defines it in terms of hybrid execution:

  | An execution is finite if :math:`\tau` is a finite sequence ending with a compact interval, it is called infinite if :math:`\tau` is either an infinite sequence or if :math:`\tau_\infty(\mathcal{X}) = \infty`, and it is called Zeno if it is infinite but :math:`\tau_\infty(\mathcal{X}) < \infty`. The execution time of a Zeno execution is also called the Zeno time.

The definition of zenoness from :cite:p:`Zhang2001` is reproduced in :ref:`definition-ha-zeno`.

.. target:: definition-ha-zeno
   :label: Definition 2.3.8 (Zeno Hybrid Automaton)

**Definition 2.3.8 (Zeno Hybrid Automaton)**:

  A hybrid automaton :math:`H` is Zeno if there exists :math:`(q_0,x_0) \in Init` such that all
  executions in :math:`\mathcal{E}^\infty(q_0,x_0)` are Zeno.

Zenoness causes hybrid executions to get "stuck" in an infinite discrete execution that renders the
"mode selection problem" of Schaft unsolvable, :cite:p:`Schaft2000`:

  | ... it may happen that a cycling between different modes occurs ("livelock"), and the simulator does not return to a situation in which motion according to some continuous dynamics is generated, so that effectively the simulation stops.

It is important to note that Zeno behaviour can be a consequence of both modelling and / or
computation. It is not a property of the system being modelled. In this sense it is similar to the
solvability issue of a :term:`DAE` discussed in :ref:`background-dae`, and is another "non-issue" in
the words of Cellier. However, it does have negative implications for practical computation and
analysis. It might stall computations, and give false negatives for reachability and stability
problems. Establishing that a hybrid system is zenoless is a useful result, and is something that
modellers need to strive for, and robust computations need to account for, :cite:p:`Schaft2000`:

  | In such situations the simulation software should provide a warning to the user, and if it is difficult to make a definitive choice between several possibilities perhaps the solver should even work out all reasonable options in parallel.

.. _background-hst-simulation:

Simulation of Hybrid Automata
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A model is not always perfectly constructed ex nihilo, and is rather the intermediate result of a
modelling process involving both modelling and computation. During the construction of hybrid models
with a high level of discrete detail, one needs tooling to verify, test, and explore designs.
Campbell's observations about :term:`DAE`\ s can similarly be applied to :term:`HA`\ s,
:cite:p:`Campbell1995`:

  | Being able to do engineering design and computer simulation directly on these original equations would lead to faster simulation and design, permit easier model variation, and allow for more complex models.

A modelling framework should thus be directly usable for practical computation, even though the
completed model might not necessarily be used *as* a computational model. Perhaps it will only serve
as a basis for constructing other computational models. The :term:`HA`, like the :term:`DAE`, is an
expressive formalism, that does admit practical computation. See :cite:p:`Zimmer2010` for an
overview of existing software solutions of variable structure or hybrid
systems. :cite:p:`Schaft2000` discusses approaches to the simulation of a hybrid system, such as a
:term:`HA`. One of these approaches is smoothing:

  | In this method, one tries to replace the hybrid model by a smooth model which is in some sense close to it. For instance, diodes in an electrical network may be described as ideal diodes (possibly plus some other elements), which will give rise to regime-switching dynamics, or as strongly nonlinear resistors, which gives rise to smooth dynamics.

Smoothing can be seen as a right-to-left model transformation of the type discussed in
:ref:`introduction-modelling_and_computation`. However, to smooth a model, one first needs a model
to smooth. :cite:p:`Schaft2000` also discusses a more direct approach that seems apt in the light of
Campbell's observation. This is called the event tracking method:

  | The idea is to simulate the motion in some given mode using a time-stepping method until an event is detected, either by some external signal (a discrete input, such as the turning of a switch) or by violation of some constraints on the continuous state. If such an event occurs, a search is made to find accurately the time of the event and the corresponding state values, and then the integration is restarted from the new initial time and initial condition in the "correct" mode; possibly a search has to be performed to find the correct mode.

What is here called "the correct mode" is in :ref:`definition-ha` the vector-field
:math:`f(q)`. Events happen when the continuous state of the system, :math:`x`, leaves the domain,
:math:`D(q)`, or, when :math:`x` enters the guard of an edge, :math:`G(e)`. As mentioned in
:ref:`background-dae`, there exists open-source computation software for working with :term:`DAE`\ s
directly. Some of these, like :cite:p:`Hindmarsh2005`, include root-finding capabilities. These
capabilities can be used to drive the event detection algorithms. A root does not necessarily
constitute an event, as the guards of an :term:`HA` is defined in terms of subsets, and the
continuous trajectory might have to produce several zero-crossings to enter, or leave, a subset.
Zero-crossing and event detection also poses their own set of technical challenges, but these will
be left aside in this report (see :cite:p:`Zhang2008` for an in-depth treatment).

After detecting an event and computing a new :term:`DAE`, the continuous integration must be
restarted. A :term:`DAE` can contain algebraic constraints, which complicate the consistent
initialisation problem. The execution of a :term:`HA` will need to find consistent initial values
for every continuous execution in the hybrid execution, :cite:p:`Schaft2000`:

  | In the context of hybrid systems, start-up procedures for DAE solvers should receive particular attention since re-initializations are expected to occur frequently.

When transitioning from one discrete state to another, :math:`q_0 \to q_1`, and thus from one
continuous dynamic to another, the continuous state might need to be subjected to the resets
:math:`R(e,x)` of :ref:`definition-ha`. Let :math:`x^0` and :math:`x^1`, and :math:`F^0` and
:math:`F^1` denote the continuous state and dynamics before and after a transition. It is not
necessarily the case that :math:`F^0(x^0) = 0` will imply :math:`F^1(x^0) = 0`. Even with the help
of software to find :math:`x^1_a` and :math:`\dot{x}^1_d`, as discussed in
:ref:`background-dae-simulation`, one still needs to compute :math:`x^1_d` according to the reset
map. This computation might induce zenoness in :term:`HA` that are zenoless by
:ref:`definition-ha-zeno`, :cite:p:`Schaft2000`:

  | Theoretically, the state after the jump should satisfy certain constraints exactly; finite word length effects however will cause small deviations in the order of the machine precision. Such deviations may cause an interaction with the mode selection module; in particular it may appear that a certain constraint is violated so that a new event is detected.

:cite:p:`Andersson1994` provides an informative high-level description of such an event tracking, or
event detecting, simulation algorithm of a hybrid system in Omola (which also, mutatis mutandis,
works for :term:`HA`\ s), and illustrates it in **Figure 5.10** which is reconstructed in
:numref:`figure-ha-simulation-omola`.

Another interesting algorithm, that refines the "Solve DAE problem" block of
:numref:`figure-ha-simulation-omola`, is found in :cite:p:`Zimmer2013`, where Zimmer presents the
Sol-framework. This is a framework for working with models of variable (mathematical) structure,
such as a :term:`HA`. Zimmer introduces the notion of a dynamic :term:`DAE` processor (:term:`DDP`)
whose function is to turn an implicit :term:`DAE` into an explicit :term:`ODE`, as discussed in
:ref:`background-dae`. This is done during, not before, the construction of the execution. Such an
approach is essential when working with :term:`HA`\ s of high levels of discrete detail. The number
of potential :term:`DAE`\ s makes the cost of dealing with all of them eagerly, *before* the
construction of an execution, prohibitive compared to the added cost and complexity of dealing with
them lazily and on demand *during* the construction. The lazy approach can be combined
JIT-compilation-schemes to generate efficient executable code, see :cite:p:`Chupin2021` for a recent
report using LLVM (:cite:p:`LLVM04`).

.. graphviz::
   :align: center
   :caption: The simulation algorithm for Omola Hybrid Models
   :name: figure-ha-simulation-omola

   digraph{
     label=""
     splines=ortho
     center=true
     node[style=filled;fillcolor="#f0c674";shape=box]
     v0[label="Begin";shape=ellipse]
     v1[label="Find consistent initial values"]
     v2[label="Check invariants"]
     v3[label="Any events?";shape=hexagon]
     v4[label="Solve DAE problem and\nadvance time until final\ntime or discrete event."]
     v5[label="Fire event."]
     v6[label="Final time?";shape=hexagon]
     v7[label="End";shape=ellipse]
     { rank=same;v3;v5; }
     v0 -> v1
     v1 -> v2
     v2 -> v3
     v3 -> v4[label="no"]
     v3 -> v5[label="  yes  "]
     v4 -> v6
     v5 -> v1
     v4:e -> v5:w [style=invis;rank=same]
     v6 -> v2:w[taillabel="no"]
     v6 -> v7[taillabel="yes"]
   }

.. graphviz::
   :align: center
   :caption: Dynamic processing of Sol
   :name: figure-ha-simulation-sol

   digraph{
     label=""
     center=true
     node[style=filled;fillcolor="#f0c674";shape=box]
     v0[label="Parsing"]
     v1[label="Preprocessing"]
     v2[label="Instantiation\nand Flattening";group="group0"]
     v3[label="Evaluation"]
     v4[label="Dynamic DAE\nProcessing";group="group0"]
     v5[label="Time\nIntegration";group="group1"]
     v6[label="Event\nHandling";group="group1"]
     vX0[label="";fillcolor="#000000";shape=point;style=invis;group="group1"]
     vX1[label="";fillcolor="#000000";shape=point;group="group1"]
     { rank=same;v2;v5; }
     { rank=same;vX0;v3;vX1; }
     { rank=same;v4;v6; }
     v0 -> v1
     v1 -> v2
     v2 -> v4
     v2 -> vX0[style=invis]
     vX0 -> v4[style=invis]
     vX0 -> v3[style=invis]
     v3 -> v4[dir=back]
     v2 -> v3[dir=back]
     v5 -> vX1[dir=none]
     vX1 -> v6[dir=none]
     v3 -> vX1[dir=back]
   }
  

..  LocalWords:  Schaft Lunze mld Bemporad tx leq mathbb bmatrix triangleq otuput geq Hedlund svg
..  LocalWords:  numref DeSchutter Zhang Init le infty langle rangle mathbf emptyset neq todo ntime
..  LocalWords:  fillcolor invariants nadvance invis taillabel clearpage Zimmer DDP Cellier JIT vX
..  LocalWords:  Chupin Preprocessing Instantiation nand nProcessing nIntegration nHandling dir
