from itertools import product
from matplotlib import lines
import hysj
import hysj.latex
import matplotlib.pyplot as plot
import numpy
import pathlib

plot.rcParams['font.size'] = 11

def figure_path(name):
  try:
    return (pathlib.Path(__file__).parent.absolute() / name).with_suffix('.svg')
  except NameError:
    return (pathlib.Path('sources/background') / name).with_suffix('.svg')

mathematics = hysj.mathematics

cautomatons = hysj.automatons.continuous
dautomatons = hysj.automatons.discrete
hautomatons = hysj.automatons.hybrid

csolvers = hysj.solvers.continuous
dsolvers = hysj.solvers.discrete
hsolvers = hysj.solvers.hybrid

csimulators = hysj.simulators.continuous
dsimulators = hysj.simulators.discrete
hsimulators = hysj.simulators.hybrid

regions   = range(2)
variables = range(2)
order     = 1

locations = [(0,),(1,)]
def prime(i):
  return (i + 1)%2

symbol = 0

def make_problem(initial_continuous_state):
  program = mathematics.Program()

  alpha,omega,epsilon = list(map(program.constant,[5.0,1.0,0.1]))
  zero = program.zero()
  
  A = [ [ [ program.negation(epsilon), program.multiplication([alpha,omega]) ],
          [ program.negation(omega),   program.negation(epsilon)             ] ],
        [ [ program.negation(epsilon), omega                                 ],
          [ program.multiplication([program.negation(alpha), omega]), program.negation(epsilon) ] ] ]

  t = program.next_symbol()
  z = program.next_symbol()
  q = program.next_symbol()

  Z = [ program.constant(i) for i in regions ]
  
  W = [ program.equality(q,Z[i]) for i in regions ]
  
  X = [ program.variable(independent_variable = t,order = 1,info = i + 2)
        for i in variables ]

  def f(region,variable):
    return program.addition([program.multiplication([a,x[0]])
                             for a,x in zip(A[region][variable],X) ])
    
  F = [ [ f(i,j) for j in variables ] for i in regions ]

  equations = [ [ program.equality(X[j][-1],F[i][j]) for j in variables ]
                for i in regions ]

  ray = [ program.strict_inequality(X[0][0],X[1][0]),
          program.strict_inequality(program.negation(X[0][0]),X[1][0]) ]

  abs_greater = [ program.assertion(program.strict_inequality(program.power(X[prime(i)][0],program.constant(2)),
                                                              program.power(X[i][0],program.constant(2))))
                  for i in variables ] 
  region = [ abs_greater[0],program.complement(abs_greater[0]) ]
  
  
  t_begin,t_end,t_count = (0.0,50.0,2000+1)
  t_delta  = (t_end - t_begin) / (t_count - 1)

  dvariables = dautomatons.BasicVariables(
    independent = z,
    dependent = [
      dautomatons.DependentVariable(domain = 2,operation = q)
    ])
  dautomaton = dautomatons.BasicAutomaton(
    variables = dvariables,
    transitions = [
      dautomatons.Transition(
        variable = 0,
        difference = program.one(),
        guard = program.conjunction([program.complement(W[i]),region[i]]) 
      ) for i in regions
    ])
  cvariables = cautomatons.BasicVariables(independent = t,dependent = X)
  hautomaton = hautomatons.BasicAutomaton(
    discrete_automaton = dautomaton,
    continuous_variables = cvariables,
    activities = [
      hautomatons.Activity(
        equation = equations[i][j],
        guard    = W[i]
      ) for i,j in product(regions,variables)
    ],
    actions = [])

  problem = hsimulators.Problem(
    program = program,
    automaton = hautomaton,
    state = hsolvers.BasicState(
      discrete   = dsolvers.BasicState(
        independent = 0,
        dependent = [ locations[0], [0] ]),
      continuous = csolvers.BasicValuation(
        independent = t_begin,
        dependent  = initial_continuous_state).to_state()),
    solver = hsolvers.BasicSolver(
      discrete = dsolvers.BasicSolver(),
      continuous = csolvers.BasicSolver(
        config = csolvers.BasicConfig(
          tolerance = csolvers.BasicTolerance(relative = 1.0e-2,absolute = 1.0e-3),
          step = csolvers.BasicSolver.fixed_step(step = t_delta),
          stop = t_end))))

  problem.activity_function = F
  problem.rays              = ray
  return program,problem

#NOTE: fixed-step simulation of problem for t e [0,20.0] 
def simulate(problem):
  
  solution = hsimulators.solve(problem = problem,control = lambda xs,xe:(xs,hsimulators.make_event(xe)))
  trajectories = []
  while xframe := solution():
    xsolution,xevent = xframe
    match xevent.tag:
      case dsimulators.init:
        trajectories.append((xevent.state,[]))
      case dsimulators.stop:
        trajectories.append((xevent.state,[]))
      case csimulators.step | csimulators.start | csimulators.root:
        trajectories[-1][-1].append(xevent.state.to_valuation(problem.automaton.continuous_variables))
      case csimulators.stop:
        break
      case _:
        pass
  return trajectories

def illustrate(num,problem,trajectories,frame_index):
  
  def symbol_labeler(s,d):
    return {tuple(problem.automaton.continuous_variables.independent.index()): 't'}.get(tuple(s.index()),f'x_{d - 2}')
  
  latex_renderer = hysj.latex.Renderer(problem.program,symbol_labeler)
  latex_continuous_variables = [ list(map(latex_renderer,X)) for X in problem.automaton.continuous_variables.dependent ]
  
  figure = plot.figure()
  axes   = plot.axes()

  legend_lines  = []
  legend_labels = []

  limit  = [ max([abs(s.dependent[i][0]) for t in trajectories for s in t[1]]) for i in variables ]
  margin = 0.1

  axes.set_xlim((-limit[0]-margin,limit[0]+margin))
  axes.set_ylim((-limit[1]-margin,limit[1]+margin))

  axes.grid(True)
  axes.set_xlabel(f'${latex_continuous_variables[0][0]}$')
  axes.set_ylabel(f'${latex_continuous_variables[1][0]}$')

  def colors(k):
    return ['#377eb8', '#ee6e00', '#4daf4a',
            '#f781bf', '#a65628', '#984ea3',
            '#999999', '#e41a1c', '#dede00'][k]

  def legend_lines():
    R = [ lines.Line2D([0],[0],color = colors(len(regions) + r),linestyle = '--')
          for r,_ in enumerate(problem.rays) ]
    L = [ lines.Line2D([0],[0],color = colors(l[0])) for l in locations ]
    return R + L

  def legend_labels():
    R = [ '$-x_0 + x_1 = 0$','$x_0 + x_1 = 0$' ]
    L = [ f'$X_{l[0]}$' for l in locations ]
    return R + L
  
  axes.legend(legend_lines(),legend_labels(),
              bbox_to_anchor=(0,1.02,1,0.2), loc="lower center",
              borderaxespad=0, ncol=len(problem.rays) + len(locations))

  grid       = [ numpy.linspace(-limit[i],limit[i],num = 20) for i in variables ]
  calculator = hysj.calculators.BasicCalculator(problem.program)

  #NOTE: compute the rays - jeh
  calculator[problem.automaton.continuous_variables.dependent[1][0].index()] = 0.0
  for i in range(len(problem.rays)):
    xdata = []
    ydata = []
    for x in grid[0]:
      calculator[problem.automaton.continuous_variables.dependent[0][0].index()] = x
      xdata.append(x)
      ydata.append(calculator(problem.rays[i]))
    plot.plot(xdata,ydata,color = colors(len(regions) + i),linestyle = '--')

  #NOTE: compute quiver of phase plane - jeh
  def make_quiver():
    quiver_grid = numpy.meshgrid(*grid)
    quiver_data = []
    for i in regions:
      quiver_data.append(([],[]))
      for a in range(len(quiver_grid[0])):
        quiver_data[-1][0].append([])
        quiver_data[-1][1].append([])
        for b in range(len(quiver_grid[0][a])):
          for k in variables:
            calculator[problem.automaton.continuous_variables.dependent[k][0].index()] = quiver_grid[k][a][b]
          for k in variables:
            quiver_data[-1][k][-1].append(calculator(problem.activity_function[i][k]))

    quiver = plot.quiver(quiver_grid[0],
                         quiver_grid[1],
                         quiver_data[0][0],
                         quiver_data[0][1],
                         pivot='mid',
                         color=colors(0),width=0.003)
    return quiver_data,quiver

  quiver_data,quiver = make_quiver()

  #NOTE: set up animation - jeh
  coords = sum([[(trajectories[i][0].dependent[0][0],j) for j in range(len(trajectories[i][1]))] for i in range(len(trajectories))],[])

  state_coords = sum([[(i,j) for j in range(len(trajectories[i][1]))] for i in range(len(trajectories))],[])

  execution = []
  for i in range(frame_index + 1):
    j,k = state_coords[i]
    if len(execution) == 0 or execution[-1][0] != j:
      execution.append((j,[]))
    execution[-1][-1].append(trajectories[j][-1][k])

  current_location = trajectories[execution[-1][0]][0].dependent[0][0]
  quiver.set_color(colors(current_location))
  quiver.set_UVC(quiver_data[current_location][0],quiver_data[current_location][1])

  for j,X in execution:
    color = colors(trajectories[j][0].dependent[0][0])
    xdata = [ x.dependent[0][0] for x in X ]
    ydata = [ x.dependent[1][0] for x in X ]
    axes.plot(xdata[0],ydata[0],marker='.',color=color)[0]
    axes.plot(xdata,ydata,color=color)[0]
    if j == execution[-1][0] and len(xdata) >= 2:
      axes.arrow(xdata[-2], ydata[-2],
                 xdata[-1]-xdata[-2], 
                 ydata[-1]-ydata[-2], fc=color, ec=color,
                 width = 0.01)
  plot.savefig(figure_path(f'ha-flower-figure-{num}'),dpi=600,bbox_inches='tight',format='svg',transparent = True)

try:
  program,problem = make_problem(initial_continuous_state = [[1.0,0.0],[0.0,0.0]])
  trajectories = simulate(problem = problem)
  illustrate(0,problem,trajectories,frame_index = 0)
  illustrate(1,problem,trajectories,frame_index = 100)
  illustrate(2,problem,trajectories,frame_index = 1000)
except Exception as e:
  import traceback
  print(traceback.format_exc())
