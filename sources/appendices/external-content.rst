================
External Content
================

The implementation of the modelling frameworks of :term:`SDA`, :term:`SHA`, as well as the
construction of their executions were implemented as a C++ library with python bindings in
:cite:p:`RHelset2022`. This is a git repository hosted on GitLab. The discrete model of a multibody
flow network is implemented in the same repository on the `models-multibody_loss_network
<https://gitlab.com/jehelset/hysj/-/tree/models-multibody_loss_network>`_ branch. The following
attached python-scripts used this library to generate the executions and diagrams used in this
report:

- `ha_flower_figures.py`
- `pwa_flower_figures.py`
- `discrete_model_simulation_figures.py`
- `discrete_model_state_figures.py`
- `structured_da_figures.py`

Some snippets from `ha_flower_figures.py` used to generate the figures in :ref:`ha-example` are shown in :ref:`appendix-code-listing-0`, :ref:`appendix-code-listing-1`, :ref:`appendix-code-listing-2`, and :ref:`appendix-code-listing-3`.

.. _appendix-code-listing-0:

.. literalinclude:: ../background/ha_flower_figures.py
   :language: python
   :lines: 47-50
   :linenos:
   :caption: Symbolic representation of :math:`A` 

.. _appendix-code-listing-1:

.. literalinclude:: ../background/ha_flower_figures.py
   :language: python
   :lines: 89-97
   :linenos:
   :caption: Constructing the :term:`SDA`

.. raw:: latex

   \clearpage

.. _appendix-code-listing-2:

.. literalinclude:: ../background/ha_flower_figures.py
   :language: python
   :lines: 99-108
   :linenos:
   :caption: Constructing the :term:`SHA`

.. _appendix-code-listing-3:

.. literalinclude:: ../background/ha_flower_figures.py
   :language: python
   :lines: 110-126
   :linenos:
   :caption: Constructing the initial value problem.

.. _appendix-code-listing-4:

.. literalinclude:: ../background/ha_flower_figures.py
   :language: python
   :lines: 135-149
   :linenos:
   :caption: Constructing a hybrid trajectory.


