========
Notation
========

Below is a collection of notational conventions that are considered idiosyncratic and thus are
defined explicitly.

**Definition**

  A definition is denoted :math:`A \triangleq B`, while a relation is denoted :math:`A = B`.

**Powersets**

  Powersets are denoted with :math:`\mathbb{P}`.

**Index sets**

  :math:`\{ N, \ldots, M-1 \}` is denoted :math:`N \leq \mathbb{Z} < M`, if :math:`N` is omitted it
  is taken to be zero. :math:`\mathcal{B}` is defined as the set :math:`\mathcal{B} = \mathbb{Z} <
  2`.  If a natural number is used where a set expected it is taken to mean an equivalent index
  set. :math:`x \in 5` thus means :math:`x \in Z < 5`.

**Structured and Unstructured Super and Subscripts**

  Symbols with structured super and subscript :math:`x^{i \ldots}_{j \ldots}` where :math:`i_k \in
  \mathbb{Z} < N_k` and :math:`j_k \in \mathbb{Z} < M_k` are alternatively written with an
  unstructured :math:`x^{\mathbf{i}}_{\mathbf{j}}`, with :math:`\mathbf{i}` defined as:

  .. math::

     \mathbf{i} = \sum_{k_0} (\prod_{k_1} N_{k_1}) \cdot i_{k_0}

  - with :math:`I = \{ i \ldots \}`;
  - and with :math:`K_0 = Z < |I|`, and :math:`k_0 \in K_0`;
  - and with :math:`K_1(k_0) = k_0 < Z \leq |K|`, and :math:`k_1 \in K_1(k_0)`;
  - and with :math:`N_{|I|} = 1`.

  and similarly for :math:`\mathbf{j}` and :math:`M`.
