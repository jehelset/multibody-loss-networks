.. _appendices-discrete_diagrams:

=======================
Discrete Model Diagrams
=======================

.. _appendices-discrete_diagrams-sftd:

Diagrams For Filling The Simple Network Top Down
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: ../results/discrete-model-simulation-0-0-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-0

   State diagram for :math:`G_0` at :math:`t_{0,0}`, with the tub leg control variable set high.
   
.. figure:: ../results/discrete-model-simulation-0-0-1-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-1

   State diagram for :math:`G_0` at :math:`t_{0,m}`. Here the tub :math:`c^{0,1}_0` has become a
   source, as can be seen by the flow-arrow being drawn in light blue. A branching event has been
   detected for the tub, and seeding events has been detected for the valve and incident endpipe.
  
.. figure:: ../results/discrete-model-simulation-0-1-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-2

   State diagram for :math:`G_0` at :math:`t_{1,m}`. This execution had no control signals set
   high. The discrete execution converged after the newly formed waterbody has entered the halfpipe
   :math:`c^{1,1}_{0,0}`.

.. figure:: ../results/discrete-model-simulation-0-2-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-3

   State diagram for :math:`G_0` at :math:`t_{2,m}`. Here the midpipe joint control
   :math:`q^{1,0}_0`, is set high, which indicates that the combined volume of halfpipes
   :math:`c^{1,1}_{0,j}` has filled the pipe. The execution converges by detecting a
   midpipe branch event.

.. figure:: ../results/discrete-model-simulation-0-3-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-4

   State diagram for :math:`G_0` at :math:`t_{3,m}`. This execution has no control signals set
   high, and is simply processing the events detected in :math:`\tau_3`. The waterbody fills the
   pipe, and the endpipe :math:`c^{1,2}_{0,1}` has become a source for the valve :math:`c^0_1`, as
   indicated by the light blue shading of the flow triangle. A branch event has been detected for
   the endpipe, and seding events have been detected for the valve and tub.

.. figure:: ../results/discrete-model-simulation-0-4-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-5

   State diagram for :math:`G_0` at :math:`t_{4,m}`. This execution has no control signals set
   high, and is simply processing the events detected in :math:`\tau_4`. The waterbody has now
   completely filled the network and water is in freefall into the tub :math:`c^{0,0}_1`.

.. _appendices-discrete_diagrams-dftd:

Diagrams For Draining The Simple Network Top Down
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: ../results/discrete-model-simulation-0-8-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-6

   State diagram for :math:`G_0` at :math:`t_{0,0}`, with the tub leg control variable set high.
   The upstream tub is about to run dry.
   
.. figure:: ../results/discrete-model-simulation-0-8-1-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-7

   State diagram for :math:`G_0` at :math:`t_{1,m}`. Here a tub cutting has been detected,
   as a consequence, the valve, upstream endpipe, and midpipe are receding.

.. figure:: ../results/discrete-model-simulation-0-9-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-8

   State diagram for :math:`G_0` at :math:`t_{2,m}`, with the downstream halfpipe leg control
   variable set high. The upstream tub, valve, and halfpipe have all run dry, and the downstream
   halfpipe is about to recede.

.. figure:: ../results/discrete-model-simulation-0-10-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-9

   State diagram for :math:`G_0` at :math:`t_{3,m}`. The downstream halfpipe has receded, and
   and the downstream endpipe has been cut. The source count of the valve is 0, and the
   valve and the tub are now receding.

.. figure:: ../results/discrete-model-simulation-0-11-0-figure.svg
   :align: center
   :width: 80%
   :name: figure-discrete-model-simulation-10

   State diagram for :math:`G_0` at :math:`t_{4,0}`. The network has finally run dry. Note that the
   tub leg is still wet, but the tub flow is negative. The pressure inside the valve network is
   preventing the waterbody from entering.
