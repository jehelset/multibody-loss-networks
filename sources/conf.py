# -*- coding: utf-8 -*-
import sys
import os

sys.path.append(os.path.abspath('sphinx-extensions'))

project = 'Multibody Flow Networks'
title = 'Multibody Flow Networks'
subtitle = 'A Hybrid System Theoretic Model'
author = 'John Eivind Rømma Helset'
copyright = 'No Rights Reserved'
extensions = [
  'sphinx.ext.autosectionlabel',
  'sphinx.ext.mathjax',
  'sphinx.ext.graphviz',
  'sphinx.ext.todo',
  'sphinxcontrib.bibtex',
  'sphinxcontrib.rsvgconverter',
  'named_target'
]

numfig = True
graphviz_output_format = 'svg'

templates_path = ['_templates']
exclude_patterns = ['_build']

html_theme = 'alabaster'
html_show_sourcelink = False
html_show_copyright = False
html_theme_options = {
  'sidebar_collapse': False,
  'show_powered_by': False,
  'body_max_width': 'auto',
  'page_width': 'auto'
}
html_title = 'Thesis'

latex_logo = '../documents/ntnu_logo.png'
latex_additional_files = ['latex.cls']
latex_documents = [
    ('index', 'multibody-flow-networks.tex', title, author, 'latex', True)
]


latex_elements = {
  'figure_align': 'H',
  'babel': r'\usepackage[english,norsk]{babel}',
  'preamble': r'''
  \newenvironment{abstract}{}{}
  \usepackage{abstract}
  
  \newcommand{\sphinxinstitution}{
    Department of Engineering Cybernetics
  }
  \newcommand{\sphinxsupervisor}{
    Supervisor: Sverre Hendseth
  }
  \newcommand{\sphinxacknowledgements}{
    I would like to thank the academic necromancy of Ellen Beate Hove, without whom I would not have been able to finish my degree.
  }
  \newcommand{\sphinxabstract}{
    \renewcommand{\abstractname}{Abstract}
    \begin{abstract}
  
      The decision making process of hydropower production planning is supported by a suite of
      computations, many of which involve a watercourse model. Models of tunnel system flow used in
      this context commonly assume that each tunnel system is inhabitated by a single body of water.
      Furthermore that tunnel system networks conform to some fixed topology. These assumptions can,
      for some computations, lead to inadequate results.

      Continuous models based on differential algebraic equations enables a modeller to describe
      flow through tunnel networks without the assumption of a fixed topology. The hybrid system
      theoretical framework of hybrid automata, whose continuous dynamics are described in terms of
      differential algebraic equations, enables a modeller to directly describe arbitrary
      distributions of waterbodies in tunnel networks; to model a multibody flow network. This in
      turn enables more complex patterns of flow between the reservoirs connected to a tunnel
      network.

      A multibody flow network model can be constructed at a high enough level of detail to serve as
      a basis for tunnel system flow models in the computational models used in the context of
      hydrowpower production planning. This would be a more efficient construction, than
      constructing every computational model independently.

      In this report the modelling framework of hybrid automata is refined into two new frameworks:
      structured discrete automata, and structured hybrid automata. These frameworks are tailored to
      the construction of hybrid automata, with a high level of discrete detail, such as a multibody
      flow network. The framework of structured discrete automata is then used to construct a
      discrete model of a multibody flow network. This model can be used as a basis for constructing
      a hybrid model, in the form of a structured hybrid automaton, of a multibody flow network.

    \end{abstract}
    \selectlanguage{norwegian}
    \renewcommand{\abstractname}{Sammendrag}
    \begin{abstract}
  
      Produksjonsplanlegging av vannkraft er understøtta av forskjellige beregninger. Mange av
      beregningsmodellene legger til grunn en modell av et vassdrag. Modellene som brukes av
      tunnellsystem er ofte lagd med en antagelse om at en enkelt vannkropp har tilhold i tunnellen,
      samt at tunnellsystemet har en spesifikk topologisk struktur.

      Kontinuerlige modeller basert på differensial-algebraiske likningssett lar derimot en
      modellmaker beskrive flyt gjennom generelle tunnellnettverk, uten en del topologiske
      antagelser. Det hybrid-system-teoreriske rammeverket, hybride tilstandsmaskiner, hvis
      kontinuerlige dynamikk er representert av differensial-algebraiske likningssett, lar en
      modellmaker direkte beskrive vilkårlige fordelinger av flere vannkropper i et
      tunnellsystem. Modellen kan derfor representere mer komplekse flytmønster mellom magasinene
      som er tilkobla tunnellnettverket.

      En flerkropps-flyt-nettverk-modell kan settes opp på et høyt detaljnivå, og brukes til å sette
      opp resten av beregningsmodellene som brukes innafor feltet. Dette er en mer effektiv måte å
      sette opp modeller på, enn å sette opp hver modell isolert.

      I denne rapporten blir rammeverket til den hybride tilstandsmaskinen raffinert til to nye
      modelleringsrammeverk, kalt strukturerte diskret og hybride tilstandsmaskiner. Disse er
      skreddersydd for konstruksjon av hybride modeller, som har et høyt diskret detaljnivå, som for
      eksempel et flerkropps-flyt-nettverk. Rammeverket blir så brukt til å sette opp en diskret
      modell av et flerkropps-flyt-nettverk, som kan brukes som utgangspunkt for oppsett av en
      hybrid modell. 

    \end{abstract}
    \selectlanguage{english}
  }'''
}

bibtex_bibfiles = ['bibliography.bib']
todo_include_todos = False
