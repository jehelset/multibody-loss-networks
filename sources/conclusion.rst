**********
Conclusion
**********

Practical computation is an important tool for construction of hybrid automata with a high level of
discrete detail. Structured automata is a modelling framework designed with such constructions in
mind. It decomposes the hybrid automata into a structured discrete automaton and structured hybrid
automaton. The transition guards of a structured discrete automaton are dependent on the discrete
time of an execution which enables the sequencing of transitions in time, which makes it easier to
reason about the zenoness of a model. The execution of a structured hybrid automaton is a
formalisation of the event detecting approach to simulating hybrid systems and in particular makes
the relation between a continuous root and a discrete event explicit. It is a step in the direction
of a more formalised approach to the simulation of a hybrid automaton.

A multibody flow network is a new hybrid system theoretical model combining the continuous dynamics
of the flow networks of :cite:p:`Jansen2014` with the temporary flow paths :cite:p:`Ocampo2010` to
support arbitrary distributions of waterbodies and arbitrary flow paths in a tunnel network. Its
discrete part was implemented and simulated as a structured discrete automaton, and shown to be
deterministic and zenofree. The discrete multibody flow network is able to represent arbitrary
distributions of waterbodies and patterns of flow in a network of pipes and valves, and detects the
branching, splitting, colliding, and receding of waterbodies as discrete events. It is a discrete
base from which a structured hybrid automaton of a multibody flow network can be constructed.

Structured automata, the construction of their executions, and the discrete multibody flow network,
was implemented in :cite:p:`RHelset2022`. Both the framework and the construction of executions were
implemented as C++ libraries with Python bindings. The construction of the executions were
implemented as event-based coroutines. These events are derived directly from the definition of the
execution of the automaton. Because the construction of the execution was implemented in a direct
manner, the implementation of the construction needed a symbolic representation of every single
component of its formal definition. In the continuation of an event the modeller gets direct access
to this symbolic representation in the current state of the construction of an execution. The
combination of this access, suspendable constructions, and bindings in a scripting language made it
easy to debug and test designs. It also enabled the programmatic construction of diagrams that
helped visualise complex discrete dynamics. This framework and implementation approach is
well-suited to support the construction of hybrid automata with practical computation.
