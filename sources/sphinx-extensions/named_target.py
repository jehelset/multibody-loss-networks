# put following line into your conf.py
from sphinx.util import ws_re
from docutils import nodes
from docutils.parsers.rst import Directive, directives

class NamedTarget(Directive):
    has_content = False
    required_arguments = 1
    optional_arguments = 0
    option_spec = {'label': directives.unchanged}

    def run(self):
        targetname = ws_re.sub(' ', self.arguments[0].strip())
        node = nodes.target('', '', names=[targetname])
        if 'label' in self.options:
            node['label'] = self.options['label']
        self.state.document.note_explicit_target(node)
        return [node]


def on_doctree_read(app, doctree):
    def lookup_name(id):
        for name, (docname, labelid) in app.env.domaindata['std']['anonlabels'].items():
            if labelid == id:
                return (name, docname)
        else:
            return (None, None)

    labels = app.env.domaindata['std']['labels']
    for node in doctree.traverse(nodes.target):
        if 'label' in node:
            name, docname = lookup_name(node['refid'])
            if name not in labels:
                labels[name] = (docname, node['refid'], node['label'])


def setup(app):
    app.add_directive('target', NamedTarget)
    app.connect('doctree-read', on_doctree_read)
