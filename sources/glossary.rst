========
Glossary
========

.. glossary::

   API
     Application Programming Interface

   DA
     Discrete Automaton

   DAG
     Directed Acyclic Graph

   DAE
     Differential Algebraic Equation

   DDP
     Dynamic Differential Algebraic Equation Processor

   HA
     Hybrid Automaton

   JIT
     Just In Time

   MILP
     Mixed integer linear programming.

   MLD
     Mixed Logical-Dynamical System, see :cite:p:`Lunze2009` for more.

   MPC
     Model Predictive Control

   ODE
     Ordinary Differential Equation

   PWA
     Piecewise Affine

   SDA
     Structured Discrete Automaton

   SHA
     Structured Hybrid Automaton
