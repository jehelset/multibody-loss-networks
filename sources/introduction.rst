************
Introduction
************

======================
Context and Motivation
======================

.. _introduction-tunnel_systems:

The Tunnel Systems of a Hydropower Producing Watercourse
--------------------------------------------------------

A hydropower producing watercourse is a complex system that can be represented in terms of multiple
connected subsystems consisting of reservoirs, catchments of rainwater and snowmelt, river systems,
and tunnel systems. The tunnel systems of such a watercourse are networks of gates, tunnels, units,
and valves, where a unit is either a generator or a pump. An example of a hydropower producing
watercourse based on the Novle and Røldal hydropower plants is shown as a diagram in
:numref:`figure-introduction-system-0`.

.. graphviz::
   :caption: Example hydropower producing topology based on the Novle and Røldal hydropower plants.
   :name: figure-introduction-system-0
           
   digraph {
       rankdir = LR
       node [label="",fixedsize=true,shape="box",style="filled"]
       edge [arrowsize=0.3]
       reservoir_2,reservoir_1,reservoir_0 [width=0.3,height=0.6,fillcolor="brown"]
       catchment_0,catchment_1 [shape="circle",width=0.2,height=0.2,fillcolor="forestgreen"]

       subgraph cluster_0{
             bgcolor = "cornsilk"
	     label = "Tunnel system"
	     gate_0,gate_1,gate_2 [width=0.1,height=0.1,fillcolor="grey"]
	     tunnel_0,tunnel_1,tunnel_2,tunnel_3,tunnel_4,tunnel_5,tunnel_6 [width=0.4,height=0.1,fillcolor="grey"]
	     unit_0,unit_1 [width=0.2,height=0.2,shape="diamond",fillcolor="orange"]
       }
       subgraph cluster_1{
             bgcolor = "cornsilk"
	     label = "River system"
	     river_0,river_1,river_2 [width=0.4,height=0.1,fillcolor="cyan"]
       }
       subgraph cluster_2{
             bgcolor = "cornsilk"
	     label = "River system"
	     river_3 [width=0.4,height=0.1,fillcolor="cyan"]
       }
       sea [shape="circle",width=0.2,height=0.2,fillcolor="cornflowerblue"]

       reservoir_2:e->gate_2:w
       gate_2:e->tunnel_4:w
       tunnel_4:e->tunnel_2:w

       reservoir_0:e->gate_0:w
       gate_0:e->tunnel_0:w
       tunnel_0:e->unit_0:w
       catchment_0:e->river_3:w
       river_3:e->tunnel_6:w
       tunnel_6:e->tunnel_0:w

       unit_0:e->tunnel_1:w
       tunnel_1:e->tunnel_2:w

       reservoir_1:e->gate_1:w
       gate_1:e->tunnel_3:w

       tunnel_3:e->tunnel_2:w

       tunnel_2:e->unit_1:w
       unit_1:e->tunnel_5:w
       tunnel_5:e->river_0:w
       river_0:e->river_2:w
       catchment_1:e->river_1:w
       river_1:e->river_2:w

       river_2:e->sea:w
       {
	 rank = sink;
	 Legend [shape=none, fixedsize="false", margin=0, style="", label=<
	 <TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0" CELLPADDING="6">
	  <TR><TD COLSPAN="4"><B>Legend</B></TD></TR>
	  <TR><TD>Catchment</TD><TD BGCOLOR="forestgreen"></TD><TD>Reservoir</TD><TD BGCOLOR="brown"></TD></TR>
	  <TR><TD>Tunnel</TD><TD BGCOLOR="grey"></TD><TD>Unit</TD><TD BGCOLOR="orange"></TD></TR>
	  <TR><TD>River</TD><TD BGCOLOR="cyan"></TD><TD>Sea</TD><TD BGCOLOR="cornflowerblue"></TD></TR>
	 </TABLE>
	>];
      }
   }

A tunnel system can encompass multiple hydropower plants, be connected to multiple catchments,
reservoirs and river-systems, and contain multiple waterbodies. These waterbodies can be used
independently for production. Consider, for example, a water column resting on the closed left-most
unit in :numref:`figure-introduction-system-0`, while the right-most unit is producing from one or
both of the reservoirs connected to its upstream tunnel system.

This report is partly a result of my previous work with existing models of such systems in the
context of production planning. These models are often built on two simplifying assumptions. The
first assumption is that the topology of the system conforms to a given topological structure. In
particular that the tunnel system is turnip-like (see :numref:`figure-turnip`). A turnip-like system
can be decomposed into an upstream tunnel-tree (the leaves of the turnip), a plant :term:`DAG` (the
root of the turnip), and a downstream river or reservoir. The leaf vertices of the tree are
reservoirs. The :term:`DAG` has a single source and sink, and consists of trash racks, penstocks,
units, and draft-tubes.

.. figure:: turnip.svg
   :name: figure-turnip
   :scale: 100%

   Rendition of a turnip.

An example of a turnip-plant is shown in :numref:`figure-introduction-system-1`. The upstream
tunnel-tree has two reservoirs, the plant has 2 units, and there is a single river downstream. The
system in :numref:`figure-introduction-system-0` however does not conform to this topological
structure. It is difficult to find a topological mould that is generally applicable due to the
variety of hydropower plant topologies.

.. graphviz::
   :align: center
   :caption: Turnip-like hydropower plant (see legend in :numref:`figure-introduction-system-0`).
   :name: figure-introduction-system-1
           
   digraph {
       rankdir = LR
       node [label="",fixedsize=true,shape="box",style="filled"]
       edge [arrowsize=0.3]
       reservoir_0,reservoir_1 [width=0.3,height=0.6,fillcolor="brown"]

       subgraph cluster_0{
             bgcolor = "cornsilk"
	     label = "Tunnel system"
	     gate_0,gate_1 [width=0.1,height=0.1,fillcolor="grey"]
	     tunnel_0,tunnel_1,tunnel_2, tunnel_3, tunnel_4[width=0.4,height=0.1,fillcolor="grey"]
	     unit_0,unit_1 [width=0.2,height=0.2,shape="diamond",fillcolor="orange"]
       }
       river_0 [width=0.4,height=0.1,fillcolor="cyan"]

       reservoir_0:e->gate_0:w
       gate_0:e->tunnel_0:w
       tunnel_0:e->tunnel_4:w
       reservoir_1:e->gate_1:w
       gate_1:e->tunnel_1:w
       tunnel_1:e->tunnel_4:w
       tunnel_4:e->tunnel_2:w
       tunnel_2:e->unit_0:w
       tunnel_2:e->unit_1:w
       unit_0:e->tunnel_3:w
       unit_1:e->tunnel_3:w
       tunnel_3:e->river_0:w

   }

The second assumption is that the tunnel system is fully submerged; that there is one waterbody per
tunnel system. The distribution of waterbodies in a tunnel system shape the patterns of flow, and
the patterns of flow determine the directions of waterbody growth and recession, that in turn cause
waterbodies to merge and split. The two scenarios shown in :numref:`figure-introduction-dry-gate-0`
and :numref:`figure-introduction-dry-gate-1` illustrate the problem of assuming a fixed waterbody
distribution. In the former scenario both reservoirs affect the dynamics of the waterbody in the
tunnel system. In the latter scenario only the left-most reservoir does. This effect is compounded
by large tunnels which are often found in complex tunnel systems. In :cite:p:`Kishor2007`, a review
of hydropower plant models, submersion is not included as a qualitative feature as all the reviewed
models assume a fully submerged tunnel system.

Computations based on a fixed topology, or a fixed distribution of waterbodies and fixed patterns of
flow, might produce misleading results. This can in turn lead to the implementation of ad-hoc
solutions without a formal model. A formal model is an effective form of communication, and an
ad-hoc solution will, in comparison, make a computation more difficult to communicate, understand,
correctly implement, use, and maintain.

.. figure:: dry-gate-0.svg
   :name: figure-introduction-dry-gate-0
   :scale: 200%

   Wet gate configuration.
   
.. figure:: dry-gate-1.svg
   :name: figure-introduction-dry-gate-1
   :scale: 200%

   Dry gate configuration.

.. _introduction-modelling_and_computation:

Modelling and Computation
-------------------------

  | A mathematical simulation is a coded description of an experiment with a reference (pointer) to the model to which this experiment is to be applied.
  | - Cellier, :cite:p:`Cellier1991`

  | The process of modelling concerns itself with the extraction of knowledge from the physical plant to be simulated, organizing that knowledge appropriately, and representing it in some unambiguous fashion. We call the end product of the modelling cycle the model of the system to be simulated.
  | - Cellier, :cite:p:`Cellier2006`

Modelling and simulation for Cellier, as described and visualised in **Figure 1.4** of
:cite:p:`Cellier2006`, are coded descriptions of experiments; Cellier considers the simulation in
isolation. In :cite:p:`Cellier1991` and :cite:p:`Cellier2006` a simulation, or mathematical
simulation, is used in a general sense. In this report the word computation is used instead, since
simulation has a more specific meaning in the context of production planning.
  
Technical labour processes often involve multiple interrelated computations. The models of these
computations might represent the same system, be formalised in the same theoretical framework, be
subcomputations of one another, and so on. To effectively communicate, design, implement, and
maintain the totality of computational software needed to support such a labour process one should
build on the mental model of Cellier to represent relations between multiple computations, models,
and systems; a model of models.

In :numref:`figure-introduction-models` the construction of model, as it appears in
:cite:p:`Cellier2006`, has been deconstructed into a stepwise process involving multiple systems and
computations. What was previously a singular model has become a modelling phase. Models
materialising on its left side are immediate products of system modelling. These models are
constructed using expressive and flexible frameworks, are open and composable, and are constructed
at a high enough level of detail to eventually lend their clean, formal hands to any computation
that needs them. These left-sided models will be referred to as system models. Models about to exit
on the right side of the modelling phase are products of a series of stepwise left-to-right
transformations. These models are constructed using computationally tractable frameworks, highly
specialised and closed, ready to get their hands dirty in some particular computation. These
right-sided models will be referred to as computational models. The people who construct the models
on the left side might differ from those constructing the models on the right side, who again might
differ from those that implement and maintain the computations, or those that use the computations
or work with the actual systems. This underlines the need for clear and unambiguous communication;
which in this context and in this report is taken to be synonymous with formal modelling.

The :term:`DAE` and :term:`HA` formalisms are examples of expressive frameworks for continuous and
hybrid system modelling respectively. A relevant application of :term:`DAE` is the modelling of
general flow networks described in terms of graphs in :cite:p:`Jansen2014`. A :term:`HA` can model
temporary flow paths in the same manner that an :term:`MLD` is used in
:cite:p:`Ocampo2010`. Temporary flow paths can be used to model waterbody distributions and
arbitrary flow patterns in tunnel systems. Together these frameworks seem well-suited for
constructing a watercourse system model that obviates the assumptions of fixed tunnel system
topology and fixed waterbody distribution. These assumptions might still need to make their
reappearance as the system model is transformed to one of the many computational models that involve
a watercourse.

.. graphviz::
   :align: center
   :caption: Systems, Models, and Computations.
   :name: figure-introduction-models

   digraph {
      rankdir = LR
      node [fixedsize=true,width=1.2,label="",shape="box",style="filled";fillcolor="#b5bd68"]
      edge [minlen=4,arrowsize=0.7]
      nodesep=0.3
      ranksep=0.1

      subgraph cluster_0{
        label = "Systems"
	bgcolor = "#e9ebd2"
	node[label="System"]
	system_0
	filler_0[style=invis]
	system_1
      }

      subgraph cluster_1{
        label = "Models"
	bgcolor = "#e9ebd2"
	node[label="Model"]
	{
	  rank = same
          model_0
          model_3
	}
	{
	  rank = same
	  model_1
	  model_2
	}
      }
      
      subgraph cluster_2{
        label = "Computations"
	bgcolor = "#e9ebd2"
	node[label="Computation"]
	{
          rank = same
          computation_0
	  filler_2[style=invis]
          computation_1
	}
      }

      edge[label="Modelling"]
      system_0 -> model_0
      system_1 -> model_3
      edge[label="Simplification"]
      model_0 -> model_1
      edge[label="Composition"]
      model_0 -> model_2
      model_3 -> model_2
      edge[label="Compilation"]
      model_1 -> computation_0
      model_2 -> computation_1
   }

.. _introduction-computations_of_pp:

Computations of Hydropower Production Planning
----------------------------------------------

The planning of hydropower production involves several computations that aid the decision making
process of production planners; inflow forecasting, historical inflow estimation, planning, plan
simulation, and more. A potential organisation of the construction of their computational models is
illustrated in :numref:`figure-introduction-metamodel-pp`. The watercourse model is a reoccurring
component and is involved directly in several computational models, albeit at different levels of
detail. Reusing a watercourse system model for each of these constructions is more efficient than
constructing independent watercourse models.

The corresponding computations of :numref:`figure-introduction-metamodel-pp` can be described in
terms of input and output. Simulation of production plans, roughly speaking, takes as input an
inflow forecast, a set of production plans for the plants in the watercourse and computes the
trajectories of reservoir-levels, the aggregate movement of water, as well as the production and
consumption of power. To model the aggregate movement of water it must be able to represent
temporary flow paths caused by flooding or bypassing from reservoirs, similar to the wastewater
model of :cite:p:`Ocampo2010`. It must also be able to represent arbitrary flow-patterns through
tunnel systems, as discussed in :ref:`introduction-tunnel_systems`, and this is where current models
often fall short. Estimation of historical inflow takes historical measurements of water levels,
consumption and production of power as input, and produces an estimate of historical inflow as
output. This is done by computing a steady-state tunnel system flow that matches the historical
production and consumption, and then computing the historical inflow based on the mass-balance of
reservoirs. Production planning takes, roughly speaking, inflow and price forecasts as inputs and
computes an economically efficient production plan for the watercourse for a given period as
output(see for example :cite:p:`Skjelbred2019`). The watercourse models of historical inflow
estimation and production planning can be constructed by doing further simplification of the one
used for simulation.

.. graphviz::
   :align: center
   :caption: Interrelated Models used in Hydropower Production Planning.
   :name: figure-introduction-metamodel-pp

   digraph {
      rankdir = LR
      node [fixedsize=true,width=1.5,label="",shape="box",style="filled";fillcolor="#b5bd68"]
      layout = dot

      subgraph cluster_1{
	label = "Models"
	bgcolor = "#e9ebd2"
	{
   	  rank = "same"
 	  model_0[label="Market"]
	  model_3[label="Watercourse"]
	}
	model_1[label="Plan\nSimulation"]
	model_2[label="Historical Inflow\nEstimation"]
	model_4[label="Production\nPlanning"]
	model_5[label="Inflow\nForecasting"]
      }

      edge[label=""]
      model_4 -> model_1
      model_0 -> model_4
      model_3 -> model_2
      model_3 -> model_4
      model_3 -> model_1
      model_5 -> model_4
      model_2 -> model_5
   }

.. _introduction-goals:

=====
Goals
=====

The overarching question of this report is how to effectively organise the construction,
implementation, use and maintenance of a suite of computations supporting a technical labour
process. It is assumed that an efficient construction of these computational models is done through
left-to-right transformations of system models. This report focuses on the left side of
:numref:`figure-introduction-models`; on the construction of system models and system modelling
frameworks. Particular attention will be given to their potential for practical
computation. Practical computation is a useful modelling tool for directly testing, debugging,
verifying and experimenting with designs. The particular system and labour process that frames this
question is that of a hydropower producing watercourse and the associated production planning.

The goal of this report is to construct a system model of a hydropower producing watercourse in a
suitable system modelling framework, that can serve as a basis for the efficient construction of the
associated computational models, and meet the challenges of topological complexity and waterbody
distribution. Such a system model would be efficient both in the conceptual sense of constructing
several computational models by applying different transforms to the same system models, and also in
the sense of constructing models that can be implemented directly which reaps the benefits of the
unambiguous communicative power a formal model, all the way from modeller to megawatt hour.

.. _introduction-contribution:

============
Contribution
============

This report has two contributions that I would like to highlight. Firstly it proposes a new
modelling framework called structured automata consisting of structured discrete automata and
structured hybrid automata. This hybrid system theoretical framework is yet another take on the
hybrid automaton. It is tailored to the construction of hybrid automata with a high level of
discrete detail. The execution of a structured hybrid automaton is a formalisation of the event
detecting approach to the simulation of a hybrid system, lifting the process of constructing
discrete events from continuous roots into the formal definition of a hybrid execution. Models
constructed in this framework, and the construction of executions of those models, can be directly
represented even as the set of discrete states grows large. Direct representation and computation is
a useful tool for debugging, testing, and exploring system models. This new modelling framework and
the algorithms described for constructing executions, was implemented in :cite:p:`RHelset2022`. This
implementation was used during the course of this report to construct executions, generate figures,
and to assist in system modelling. It is another simulator capable of handling variable structure
systems in the same vein as :cite:p:`Zimmer2010` and :cite:p:`Chupin2021`.

The framework of structured discrete automata was used to define the discrete part of a new hybrid
system theoretical model called a multibody flow network. This model combines the flow networks of
:cite:p:`Jansen2015` with the hybrid system theoretical approach to temporary flow paths of
:cite:p:`Ocampo2010`, and is shown to be deterministic and zenofree. A discrete multibody flow
network can represent arbitrary distributions of waterbodies and patterns of flow in a network of
valves and pipes. It also detects discrete events representing the movement, collisions, and
separations of waterbodies. This discrete model is a base on which the continuous dynamics and
continuous actions of a hybrid multibody flow model can be constructed.


