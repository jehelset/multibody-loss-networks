.. _results-sda:

============================
Structured Discrete Automata
============================

.. _results-sda-definition:

Definition of a Structured Discrete Automaton
---------------------------------------------

:cite:p:`Hopcroft1979` defines a discrete automaton in terms of states, input symbols, start state,
final state and a transition function, reproduced in equation :eq:`equation-da`. 

.. math::
   :label: equation-da
   
   A \triangleq (Q,\Sigma,\delta,q_0,F)

The :term:`HA` from :ref:`definition-ha` was defined in terms of the set of valid states
:math:`Init`. Initial and accepting states will not be a part of the definition of a :term:`SDA`,
and are instead considered to be parameters of the computational problems in which the model
appears. The symbols of the :term:`DA` in Hopcroft are not required to generate an execution of a
:term:`HA` or a :term:`SDA`, so these are also dropped. However, annotating transitions with symbols
might still be useful for modelling purposes, and symbols make their re-appearance in the
construction of the discrete model in :ref:`results-flow_network_da`.

The transitions of a :term:`HA` was defined in terms of edges, :math:`E \subset \mathbb{Q} \times
\mathbb{Q}`, guards, :math:`G \colon E \to \mathbb{P}(X)` and resets, :math:`R \to E \times X \to
\mathbb{P}(X)`. The guards and resets pertain only to the continuous dynamics of the automaton. For
highly detailed discrete automata :math:`\mathbb{Q}` is a large set, which makes it difficult to
directly represent :math:`E` in a computation. Direct computation is useful for testing and
debugging during the construction of a model. A more structured definition of a discrete automaton,
that is designed to be a starting point for constructing hybrid automata with a high level of
discrete detail, is suggested in :ref:`definition-sda`.

.. raw:: latex

   \clearpage

.. target:: definition-sda
   :label: Definition 3.1.1 (Structured Discrete Automaton)

**Definition 3.1.1 (Structured Discrete Automaton)**:

  A structured discrete automaton (:term:`SDA`) is a pair :math:`A \triangleq (Q,E)`:

  1. where :math:`Q` a finite collection of :math:`N` discrete variables, taking values in
     :math:`\mathbb{Q} \subseteq \mathbb{Z}^N`;
	   
     - with derivatives :math:`\dot{Q}` taking values in :math:`\dot{\mathbb{Q}} \subseteq
       \mathbb{Z}^N`;

  2. and :math:`E \subset \mathbb{F} \times \mathbb{G}` is a finite collection of :math:`M` transitions;
     
     - where :math:`\mathbb{F}` is the set of transition equations, :math:`f \colon \mathbf{\Psi} \to
       \mathbb{Z}` with :math:`\mathbf{\Psi} \triangleq \mathbb{I} \times \mathbb{Q} \times
       \dot{\mathbb{Q}}`;
     - and :math:`\mathbb{G}` is the set of transition guards :math:`g \colon \mathbf{\Upsilon}
       \to \mathbb{B}` with :math:`\mathbf{\Upsilon} \triangleq \mathbb{I} \times \mathbb{Q}` and
       :math:`\mathbb{I} \subseteq \mathbb{Z}`.

  The sets :math:`\mathbf{\Upsilon}` and :math:`\mathbf{\Psi}` are called the states and solutions
  of :math:`A` respectively.

The :term:`SDA` has a global set of transitions instead of the source, symbol, and target triplet of
:cite:p:`Hopcroft1979`, or a mapping from the set of states, as in for example
:cite:p:`Schaft2000`. Each transition is a tuple of an equation and a guard. This decouples the set
of transitions from the set of states, and the :term:`SDA` can be represented directly even for
highly detailed discrete dynamics.  The guard functions of :ref:`definition-ha` are lowered into the
discrete automaton, and made dependent on an independent discrete variable representing discrete
time, :math:`t \in \mathbb{I}`. Explicitly representing discrete time enables the construction of
models whose determinism (:ref:`definition-ha-determinism`) and zenoness (:ref:`definition-ha-zeno`)
are easier to reason about. The transition guards are in this report assumed to be logical
expressions over the discrete variables and boolean literals :math:`\top`, :math:`\bot`. These are
composed by a subset of the operators used in :cite:p:`Bemporad1999` from :ref:`background-hst-mld`:
conjunction, :math:`\land`, disjunction :math:`\lor`, complement, :math:`\neg`, as well as algebraic
statements, or assertions, :math:`[ a = b ]`, :math:`[ a < b ]`, and :math:`[a \leq b]`. The guards
of the :term:`HA` are functions of continuous variables, but the guards of the :term:`SDA` are
functions of discrete variables. This ensures the complete separation of the :term:`SDA` from the
:term:`SHA`. The relations between discrete variables and continuous roots will be dealt with during
the definition of a :term:`SHA`. For now recall that the :term:`MLD` of Bemporad in
:ref:`background-hst-mld` used the syntax :math:`[ f(x) > 0 ] \leftrightarrow [ \delta = 1 ]` to
bridge a (discretised) continuous relation with a discrete variable.

A transition, :math:`e \triangleq (f,g)`, is active in :math:`\upsilon \in \Upsilon` when :math:`g(\upsilon)
= \top`, and inactive when :math:`g(\upsilon) = \bot`. The active transitions of :math:`\upsilon`
are denoted :math:`E^\top(\upsilon)` and the inactive guards are denoted :math:`E^\bot(\upsilon)`.
The active and inactive transition guards, :math:`G`, and equations, :math:`F` are denoted in a
similar manner. :math:`E` and :math:`F` will be used as shorthand for :math:`E^\top` and
:math:`F^\top` respectively. The active equations form a system of :math:`m` discrete differential
algebraic equations :eq:`equation-sda-transition-equations`, which determine :math:`\dot{q}`, or
:math:`\psi`, in :math:`\upsilon`.

.. math::
   :label: equation-sda-transition-equations

   F(\psi) = 0

It is not necessarily the case that :math:`N = m` or :math:`M = m`, and it might not determine, or
uniquely determine :math:`\psi`. Equation :eq:`equation-sda-transition-equations` might for example
be underdetermined. This is dealt with by defining the solutions to :math:`F` to be the set of
:math:`\dot{q} \in \dot{\mathbb{Q}}` such that :math:`F(t,q,\dot{q}) = 0` where :math:`\dot{q}_i =
0` if :math:`\frac{\partial F}{\partial \dot{q}_i} = 0`. If :math:`\dot{q}_i` is not determined then
:math:`q_i` should remain constant. Equation :eq:`equation-sda-transition-equations` might also be
overdetermined. The significance of an overdetermined system of equations is here one of
nondeterminism; a bifurcation point in the construction of the execution of a :term:`SDA`. If
:math:`F` contains both :math:`\dot{q}_i - 1 = 0` and :math:`\dot{q}_i + 1 = 0`, the execution has
two potential branches to continue along. A similar source of nondeterminism is systems of equations
that admit multiple solutions. The equation :math:`\dot{q}^2_i - 1 = 0` has the same branches as the
previous pair of overdetermining equations. A :term:`SDA` that can produce bifurcating transition
equations is nondeterministic. In this report nondeterminism caused by overdetermination will be put
aside, and determinism will only be discussed with respect to the solvability and unique solvability
of :math:`F`. This is captured in :ref:`definition-sda-derivatives`.

.. raw:: latex

   \clearpage

.. target:: definition-sda-derivatives
   :label: Definition 3.1.2 (Discrete Derivatives)

**Definition 3.1.2 (Discrete Derivatives)**:

  Let :math:`A` be a :term:`SDA`, :math:`\upsilon \in \mathbf{\Upsilon}` be a state of :math:`A`,
  and :math:`F \triangleq F^\top(\upsilon)` be the active equations of this state.

  The discrete derivatives of :math:`A` at :math:`\upsilon` is a map :math:`\dot{\mathbf{Q}} \colon
  \mathbf{\Upsilon} \to \mathbb{P}(\dot{\mathbb{Q}})`:

  1. where :math:`\dot{\mathbf{Q}}(\upsilon)` are the elements :math:`\dot{q} \in \dot{\mathbb{Q}}`
     that satisfy :math:`F(t,q,\dot{q}) = 0`;
  2. and :math:`\dot{q}_i \triangleq 0` for any :math:`\dot{q}_i` such that :math:`\frac{\partial
     F}{\partial \dot{q}_i} = 0`

Executions of a Structured Discrete Automaton
---------------------------------------------

The structured discrete counterparts of :ref:`definition-ha-time-trajectory` and
:ref:`definition-ha-execution` are reworked into :ref:`definition-sda-execution`. The set of maximal
executions, :math:`\mathcal{E}^M` has been dropped, since an execution here instead is maximal by
definition. The time trajectory has been inlined into the definition of the execution. 

.. target:: definition-sda-execution
   :label: Definition 3.1.3 (Discrete execution)

**Definition 3.1.3 (Discrete Execution)**

  Let :math:`A` be a :term:`SDA`.

  An execution of :math:`A` is a tuple :math:`\delta \triangleq (q_0)`:

  1. with initial state :math:`q_0 \in \mathbb{Q}`;
  2. and time trajectory :math:`T \subseteq \mathbb{I}`, which is an interval with :math:`\min(T) \triangleq 0`;
  3. and state trajectory :math:`q \colon \mathbb{I} \to \mathbb{Q}`, which is a map:

     .. math::

        q(i) \triangleq \begin{cases}
          [ i = 0 ]    \to q_0                               \\
          [ i \neq 0 ] \to q(i-1) + \dot{q}_{i-1}
        \end{cases}

     where :math:`\dot{q}_j \in \dot{\mathbf{Q}}(\upsilon_j)` from :ref:`definition-sda-derivatives`;
  4. and if :math:`\exists t \in T \mid \dot{q}_t = 0` then :math:`t_m \triangleq t` and :math:`T
     \triangleq [t_0,t_m]`, else :math:`t_m \triangleq \infty` and :math:`T \triangleq [t_0,t_m)`.

  :math:`\mathcal{E}(q_0)` denotes union of all executions of :math:`A` starting from
  :math:`q_0`. :math:`\mathcal{E}^*(q_0)`, and :math:`\mathcal{E}^\infty(q_0)`
  denotes the set of all finite and infinite executions respectively. :math:`T_m` is
  used to denote a time trajectory whose last element is :math:`t_m`.

The ticker and delay automata of :ref:`example-sda-ticker` and :ref:`example-sda-delay`
demonstrate the :term:`SDA` and its execution. The delay automaton demonstrates how the independent
variable can be utilized in a guard to sequence a transition.

.. target:: example-sda-ticker
   :label: Example 3.1.1 (Ticker Automaton)

**Example 3.1.1 (Ticker Automaton)**

  A ticker automaton is an automaton :math:`A`:
  
  - with :math:`Q \triangleq \{ q_0 \}` and :math:`q \in |Z| < 2`;
  - and :math:`E \triangleq \{ (\dot{q}_0 - 1,\top) \}`.

  An execution of this automaton can be seen in :numref:`figure-sda-ticker-0`.

.. figure:: structured_da-ticker-0.svg
   :align: center
   :name: figure-sda-ticker-0

   The execution :math:`\delta \triangleq (\begin{bmatrix} 0 \end{bmatrix})` over :math:`T_9` of the simple
   ticker automaton in :ref:`example-sda-ticker`.

.. target:: example-sda-delay
   :label: Example 3.1.2 (Delay Automaton)

**Example 3.1.2 (Delay Automaton)**

  Guards and equations are functions of the independent variable :math:`t` and transitions can thus
  be sequenced in time. This is demonstrated with a delay automaton :math:`A`:
  
  - with :math:`Q \triangleq \{ q_0 \}` where :math:`q_0 \in Z < 2`,
  - and :math:`E \triangleq \{ (\dot{q}_0 - 1,[ t \geq 1 ]) \}`

  The transition :math:`e_0` will only be active when :math:`t \geq 0`. An execution of this
  automaton can be seen in :numref:`figure-sda-delay-0`. 

.. figure:: structured_da-delay-0.svg
   :align: center
   :name: figure-sda-delay-0

   The execution :math:`\delta \triangleq (\begin{bmatrix} 1 \end{bmatrix})` over :math:`T_4` of the delay
   automaton in :ref:`example-sda-delay`. Note that this execution does not satisfy the 4th point of
   :ref:`definition-sda`, and indeed this constraint was suspended to cleanly demonstrate the delay
   automaton.

One of the techniques used in the construction of the discrete model in
:ref:`results-flow_network_da` is "counting variables" which is preemptively demonstrated here. A
counting variable tracks the sum of some expression over the other variables of an automaton, and an
example of such a variable is demonstrated in :ref:`example-sda-counting`.

.. target:: example-sda-counting
   :label: Example 3.1.3 (Counting Automaton)
  
**Example 3.1.3 (Counting Automaton)**

  A counting automaton is an automaton :math:`A`:
  
  - with :math:`Q \triangleq \{ q_0, q_1 \ldots \}` with :math:`|Q| > 1`, :math:`q_0 \in Z < |Q| - 1`, and
    :math:`q_i \in Z < 2`,
  - and :math:`E \triangleq \{ e_0 \}`, where :math:`f_0`, and :math:`g_0` are functions:

    .. math::

       f_0(\upsilon) &\triangleq \dot{q}_0 - (q_0 - \displaystyle \sum_{q \in Q^+} q) \\
       g_0(\upsilon) &\triangleq [ (q_0 - \displaystyle \sum_{q \in Q^+} q) \neq 0 ]

  - with :math:`Q^+ \triangleq Q \setminus \{ q_0 \}`

  The variable :math:`q_0` tracks the number of non-zero variables in :math:`Q^+`. An execution of
  this automaton can be seen in :numref:`figure-sda-counter-0`.

.. figure:: structured_da-counter-0.svg
   :align: center
   :name: figure-sda-counter-0

   The execution :math:`\delta \triangleq (\begin{bmatrix} 0 & 1 & 0 & 1 \end{bmatrix}^T)` of the automaton
   over :math:`T_2` from :ref:`example-sda-counting`, with :math:`|Q| = 4`.

.. _results-sda-determinism:
  
Determinism of a Structured Discrete Automaton
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As discussed in :ref:`results-sda-definition` the determinism of a :term:`SDA` is dependent on the
solvability of equation :eq:`equation-sda-transition-equations`. Nondeterminism is caused by
ambiguity, and three separate sources of ambiguity were identified. One is underdetermination, which
was dealt with in :ref:`definition-sda-derivatives`. The second is due to a particular `f`, and thus
the :math:`F`\ s that include it, having multiple solutions. And the third is due to the potential
for an overdetermined :math:`F`. An overdetermined :math:`F` might have no solutions if the
overdetermining equations are not redundant. The latter two cases were both be interpreted as
bifurcations, branching the execution into several potential constructions. The former branches the
executions on the solutions of :math:`F`, and the latter branches the executions on the maximal
non-overdetermined subsets of :math:`F`. Overdetermination was set aside, and determinism will be
defined in terms of solvability, thus excluding overdetermination that leads to unsolvable
:math:`F`\ s. Solvability and determinism of a :term:`SDA` are defined in
:ref:`definition-sda-solvability` and :ref:`definition-sda-determinism`. These are the structured
discrete counterparts of :ref:`definition-dae-solvability` and :ref:`definition-ha-determinism`.

.. raw:: latex

   \clearpage

.. target:: definition-sda-solvability
   :label: Definition 3.1.4 (Discrete Solvability)

**Definition 3.1.4 (Discrete Solvability)**

  A structured discrete automaton is:

  1. locally solvable for :math:`\upsilon \triangleq (t,q)` if :math:`F(t,q,\dot{q}) = 0` is solvable.
  2. locally solvable for :math:`\Upsilon \subset \mathbf{\Upsilon}` if it is locally
     solvable :math:`\forall \upsilon \in \Upsilon`.
  3. globally solvable if it is locally solvable :math:`\forall \upsilon \in \mathbf{\Upsilon}`.

  It is uniquely solvable in each of these three cases if the solutions to :math:`F(t,q,\dot{q})`
  are unique.

.. target:: definition-sda-determinism
   :label: Definition 3.1.5 (Discrete Determinism)
  
**Definition 3.1.5 (Discrete Determinism)**

  A structured discrete automaton is:

  1. locally deterministic for :math:`q` if is uniquely locally solvable for :math:`q`.
  2. locally deterministic for :math:`Q \subset \mathbb{Q}` if is uniquely locally solvable 
     :math:`\forall q \in Q`.
  3. globally deterministic if it is uniquely globally solvable.

Two automata which demonstrate unsolvability and nondeterminism are defined in
:ref:`example-sda-nsolve` and :ref:`example-sda-ndet`.

.. target:: example-sda-nsolve
   :label: Example 3.1.4 (Unsolvable Automaton)

**Example 3.1.4 (Unsolvable Automaton)**

  An example of an unsolvable automaton is :math:`A`:

  - with :math:`Q \triangleq \{ q_0 \}` with :math:`q_0 \in Z < 2`,
  - and :math:`E \triangleq \{ (\dot{q}_0 - 1, \top), (\dot{q}_0, [t > 0]) \}`.

  For any :math:`q`, when :math:`t > 0`, :math:`f_1` is included in :math:`F` and :math:`\dot{q}_0`
  becomes overdetermined. :math:`f_0` and :math:`f_1` are not mutually redundant, :math:`F` has no
  solution, and :math:`|\dot{\mathbf{Q}}(t,q)| = 0`. In this report overdetermination was set aside,
  and the combination of this automaton and initial state has no execution. 

.. target:: example-sda-ndet
   :label: Example 3.1.5 (Nondeterministic Automaton)

**Example 3.1.5 (Nondeterministic Automaton)**

  An example of a nondeterministic automaton is :math:`A`:

  - with :math:`Q \triangleq \{ q_0 \}` with :math:`q_0 \in Z < 2`,
  - and :math:`E \triangleq \{ (q_0 - \dot{q}^2_0, \top) \}`.

  For :math:`q \triangleq [ 1 ]`, :math:`f_0` has two solutions; :math:`\dot{q}_0 = 1`, and :math:`\dot{q}_0
  = -1`. Thus :math:`|\dot{\mathbf{Q}}(t,q)| = 2`, and the execution, :math:`\delta \triangleq ([ 1 ])` is
  nondeterministic. It is locally deterministic at :math:`q \triangleq [ 0 ]`.

Zenoness of a Structured Discrete Automaton
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Zenoness is defined in terms of the execution of the automaton in :ref:`definition-sda-zeno`, just
as in :ref:`definition-ha-zeno`.

.. target:: definition-sda-zeno
   :label: Definition 3.1.6 (Discrete Zenoness)
  
**Definition 3.1.6 (Discrete Zenoness)**

  A structured discrete automaton is:

  1. locally zeno for :math:`q \in \mathbb{Q}` if :math:`\mathcal{E}^\infty(q) \neq \emptyset`.
  2. locally zeno for :math:`Q \subset \mathbb{Q}` if it is locally zeno :math:`\forall q \in Q`.
  3. globally zeno if it is locally zeno :math:`\forall q \in \mathbb{Q}`.

  An automaton that is not zeno is called a zenoless automaton.

The counting automata of :ref:`example-sda-counting` is the only zenoless example automata defined
so far. The sum of a set of variables that converges will also converge, and so an automaton
composed only of constant and counting variables will be zenoless.
  
Simulation of Structured Discrete Automata
------------------------------------------

The :term:`SDA` and an algorithm for constructing the execution of a globally deterministic
:term:`SDA` was implemented in :cite:p:`RHelset2022`. It was implemented as a C++ library with
Python bindings. The algorithm, illustrated in :numref:`figure-simulation-sda`, was implemented
using an event-based coroutine. It was used to generate the executions in :ref:`example-sda-ticker`,
:ref:`example-sda-delay`, and :ref:`example-sda-counting` as well as the executions in
:ref:`results-flow_network_da`.

.. graphviz::
   :align: center
   :caption: An event-based coroutine for simulating a Structured Discrete Automata
   :name: figure-simulation-sda

   digraph{
     label=""
     center=true
     node [style=filled;fillcolor="#f0c674";shape=box] 
     edge [minlen=1]
     v0[xlabel="Begin";label="";fillcolor="#000000";shape=point]
     y0[label="Await start\ncontinuation";shape=ellipse;fixedsize=true;width=1.5]
     v1[label="Compute guards\nand derivative"]
     v2[label="Zero\nderivative?";shape=hexagon;fixedsize=true;width=1.5;group=g2]
     y3[label="Await step\ncontinuation";shape=ellipse;fixedsize=true;width=1.5;group=g1]
     v3[label="Integrate state";group=g1]
     yN[label="Await stop\ncontinuation";shape=ellipse;fixedsize=true;width=1.5;group=g2]
     vN[xlabel="End  ";label="";fillcolor="#000000";shape=point]
     { rank=same;v2;y3; }
     v0 -> y0
     y0 -> v1
     v1 -> v2
     v2 -> y3[label="no  "]
     v1 -> v3[dir=back]
     v3 -> y3[dir=back]
     v2 -> yN[label="  yes"]
     yN -> vN
   }

.. raw:: latex

   \clearpage


