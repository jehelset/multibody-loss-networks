from dataclasses import dataclass,astuple
from itertools import chain
import pathlib
import cairo
from hysj import mathematics,simulators,automatons
from hysj.models.multibody_loss_networks import *
from hysj.models.multibody_loss_networks import layouts,diagrams,latex

def figure_path(name):
  try:
    return (pathlib.Path(__file__).parent.absolute() / name).with_suffix('.svg')
  except NameError:
    return (pathlib.Path('sources/results') / name).with_suffix('.svg')
width = 1080
height = 1080

def render(index,diagram):
  with cairo.SVGSurface(figure_path(f'discrete-model-state-{index}-figure.svg'),width,height) as surface:
    context = cairo.Context(surface)
    diagrams.paint_diagram(context = context,
                           diagram = diagram,
                           width = width,
                           height = height)

dautomatons = automatons.discrete  
dsolvers    = solvers.discrete  
dsimulators = simulators.discrete

program = mathematics.Program()
time    = program.next_symbol()

@dataclass
class Fixture:
  network: system.Network
  model: discrete.Model
  automaton: dautomatons.BasicAutomaton
  state: dsolvers.BasicState
  layout: layouts.Layout

def render_trajectory(index,fixture: Fixture):
  network,model,automaton,state,layout = astuple(fixture)

  render(index,
         diagrams.discrete_state_diagram(network   = network,
                                         model     = model,
                                         state     = state,
                                         layout    = layout))

fixture0_valuations = [
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_leg_control,0,discrete.ControlValue.on),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_leg_control,0,discrete.ControlValue.on),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.endpipe_source,1,discrete.LogicValue.top),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.minus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_leg_control,0,discrete.ControlValue.on),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_branching,0,discrete.EventValue.detected),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_leg_control,0,discrete.ControlValue.on),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_branching,0,discrete.EventValue.detected),
   (discrete.midpipe_colliding,0,discrete.EventValue.detected),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.tub_branching,0,discrete.EventValue.detected),
   (discrete.valve_seeding,0,discrete.EventValue.detected),
   (discrete.endpipe_seeding,0,discrete.EventValue.detected)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
]

def fixture0(subfixture: int):
  network = system.Network()
  valves = network(2)
  network(valves[0],valves[1])
  components = network.components()
  model     = discrete.Model(program,network,time)
  automaton = model.automaton()
  layout    = layouts.Layout(network = network)
  state = dsolvers.BasicState.origo(automaton)
  for tag,index,value in fixture0_valuations[subfixture]:
    state[0,model.to_variable_index((tag,index))] = value
  
  return Fixture(network = network,
                 model = model,
                 automaton = automaton,
                 state = state,
                 layout = layout)

fixtures = sum([list(fixture0(i) for i in range(len(fixture0_valuations)))],[])

# for i,fixture in enumerate(fixtures):
#   render_trajectory(i,fixture)
