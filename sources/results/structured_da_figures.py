import hysj
import numpy as np
import matplotlib.pyplot as plot
import pathlib

plot.rcParams['font.size'] = 11

def figure_path(name):
  try:
    return (pathlib.Path(__file__).parent.absolute() / name).with_suffix('.svg')
  except NameError:
    return (pathlib.Path('sources/results') / name).with_suffix('.svg')

def colors(k):
  return ['#377eb8', '#ee6e00', '#4daf4a',
          '#f781bf', '#a65628', '#984ea3',
          '#999999', '#e41a1c', '#dede00'][k]

mathematics = hysj.mathematics
dautomatons = hysj.automatons.discrete
dsolvers    = hysj.solvers.discrete
dsimulators = hysj.simulators.discrete

program = mathematics.Program()

t = program.next_symbol()

def ticker_0():
  q = program.next_symbol()

  automaton = dautomatons.BasicAutomaton(
    variables = dautomatons.BasicVariables(
      independent = t,
      dependent = [
        dautomatons.DependentVariable(domain    = 2,
                                      operation = q)
      ]),
    transitions = [
      dautomatons.Transition(
        variable   = 0,
        difference = program.one(),
        guard      = program.true())
    ])

  problem = dsimulators.Problem(
    program = program,
    automaton = automaton,
    state = dsolvers.BasicState(independent = 0, dependent = [[0],[0]]),
    solver = dsolvers.BasicSolver())

  solution = dsimulators.solve(problem = problem,
                               control = lambda x,e:dsimulators.Event(view = e))

  steps = 10

  states = []
  while len(states) != steps:
    event = solution()
    if event is None:
      break
    match event.tag:
      case dsimulators.step:
        states.append(event.state)
      case dsimulators.stop:
        states.append(event.state)
        break
  T = [ s.independent     for s in states ]
  V = [ s.dependent[0][0] for s in states ]

  figure = plot.figure()
  axes = plot.axes()
  axes.grid(True)
  axes.set_xlabel(f'$t$')
  axes.set_xticks(np.arange(0,len(T),1))
  axes.set_ylabel(f'$q_0$')
  axes.set_yticks(np.arange(0,max(V) + 1,1))

  axes.fill_between(T, V, step='pre', alpha=0.5, color = colors(0))
  axes.step(T, V, color = colors(0))
  figure.savefig(figure_path('structured_da-ticker-0.svg'))
  plot.close(figure)

def counter_0():

  N = 3
  Q = [ program.next_symbol() for i in range(N + 1) ]

  difference = program.subtraction([program.addition(Q[1:]),Q[0]])

  automaton = dautomatons.BasicAutomaton(
    variables = dautomatons.BasicVariables(
      independent = t,
      dependent = [ dautomatons.DependentVariable(domain = N + 1, operation = Q[0]) ] \
                  + [ dautomatons.DependentVariable(domain = 2, operation = q) for q in Q[1:] ]),
    transitions = [
      dautomatons.Transition(
        variable   = 0,
        difference = difference,
        guard      = program.complement(program.equality(difference,program.zero())))
    ])

  problem = dsimulators.Problem(
    program = program,
    automaton = automaton,
    state = dsolvers.BasicState(independent = 0, dependent = [[0,1,0,1],[0,0,0,0]]),
    solver = dsolvers.BasicSolver())

  solution = dsimulators.solve(problem = problem,
                               control = lambda x,e:dsimulators.Event(view = e))

  states = []
  while True:
    event = solution()
    if event is None:
      break
    print(event)
    match event.tag:
      case dsimulators.step:
        states.append(event.state)
      case dsimulators.stop:
        states.append(event.state)
        break
  T = [ s.independent     for s in states ] + [ states[-1].independent + 1 ] 
  V = [ [ s.dependent[0][i] for s in states ] + [ states[-1].dependent[0][i] ]for i in range(N + 1) ] 

  figure,axes = plot.subplots(2,2)

  for i in range(N + 1):
    print(f'{T}, {V[i]}')
    axes.flat[i].fill_between(T, V[i], step='post', alpha=0.5, color = colors(i))
    axes.flat[i].step(T, V[i], where = 'post', color = colors(i))


  for i,a in enumerate(axes.flat):
    a.grid(True)
    a.set_xlabel(f'$t$')
    a.set_xticks(np.arange(0,len(T),1))
    a.set_ylabel(f'$q_{i}$')
    a.set_yticks(np.arange(0,max([ max(v) for v in V ]) + 1,1))

  plot.tight_layout()
  figure.savefig(figure_path('structured_da-counter-0.svg'))
  plot.close(figure)

def delay_0():
  q = program.next_symbol()

  automaton = dautomatons.BasicAutomaton(
    variables = dautomatons.BasicVariables(
      independent = t,
      dependent = [
        dautomatons.DependentVariable(domain    = 2,
                                      operation = q)
      ]),
    transitions = [
      dautomatons.Transition(
        variable   = 0,
        difference = program.zero(),
        guard      = program.true()),
      dautomatons.Transition(
        variable   = 0,
        difference = program.one(),
        guard      = program.inequality(program.constant(1),t))
    ])

  problem = dsimulators.Problem(
    program = program,
    automaton = automaton,
    state = dsolvers.BasicState(independent = 0, dependent = [[1],[0]]),
    solver = dsolvers.BasicSolver())

  solution = dsimulators.solve(problem = problem,
                               control = lambda x,e:dsimulators.Event(view = e))

  steps = 5

  states = []
  while len(states) != steps:
    event = solution()
    if event is None:
      break
    match event.tag:
      case dsimulators.step:
        states.append(event.state)
      case dsimulators.stop:
        states.append(event.state)
        break
  T = [ s.independent     for s in states ]
  V = [ s.dependent[0][0] for s in states ]

  figure = plot.figure()
  axes = plot.axes()
  axes.grid(True)
  axes.set_xlabel(f'$t$')
  axes.set_xticks(np.arange(0,len(T),1))
  axes.set_ylabel(f'$q_0$')
  axes.set_yticks(np.arange(0,max(V) + 1,1))

  axes.fill_between(T, V, step='pre', alpha=0.5, color = colors(0))
  axes.step(T, V, color = colors(0))
  figure.savefig(figure_path('structured_da-delay-0.svg'))
  plot.close(figure)

def counter_1():

  N = 3
  M = 2
  Q = [ program.next_symbol() for i in range(N + 1 + M + 1) ]

  difference0 = program.subtraction([program.addition(Q[1:N + 1]),Q[0]])
  difference1 = program.subtraction([program.addition(Q[N + 2:N + 2 + M + 1]),Q[N + 1]])

  automaton = dautomatons.BasicAutomaton(
    variables = dautomatons.BasicVariables(
      independent = t,
      dependent = [ dautomatons.DependentVariable(domain = N + 1, operation = Q[0]) ] \
                  + [ dautomatons.DependentVariable(domain = 2, operation = q) for q in Q[1:N + 1] ]
                  + [ dautomatons.DependentVariable(domain = M + 1, operation = Q[N + 1]) ] \
                  + [ dautomatons.DependentVariable(domain = 2, operation = q) for q in Q[N + 2:] ]),
    transitions = [
      dautomatons.Transition(
        variable   = 0,
        difference = difference0,
        guard      = program.complement(program.equality(difference0,program.zero()))),
      dautomatons.Transition(
        variable   = N + 1,
        difference = difference1,
        guard      = program.conjunction([program.equality(program.constant(1),t),
                                          program.complement(program.equality(difference1,program.zero()))]))
    ])

  problem = dsimulators.Problem(
    program = program,
    automaton = automaton,
    state = dsolvers.BasicState(independent = 0, dependent = [[0,1,0,1,0,1,0],[0,0,0,0,0,0,0]]),
    solver = dsolvers.BasicSolver())

  solution = dsimulators.solve(problem = problem,
                               control = lambda x,e:dsimulators.Event(view = e))

  states = []
  while True:
    event = solution()
    if event is None:
      break
    print(event)
    match event.tag:
      case dsimulators.step:
        states.append(event.state)
      case dsimulators.stop:
        states.append(event.state)
        break
  T = [ s.independent     for s in states ] + [ states[-1].independent + 1 ] 
  V = [ [ s.dependent[0][i] for s in states ] + [ states[-1].dependent[0][i] ]for i in range(N + 1 + M + 1) ] 

  figure,axes = plot.subplots(2,1)

  C = [0, N + 1]

  for i in range(2):
    axes.flat[i].fill_between(T, V[C[i]], step='post', alpha=0.5, color = colors(i))
    axes.flat[i].step(T, V[C[i]], where = 'post', color = colors(i))

  for i,a in enumerate(axes.flat):
    a.grid(True)
    a.set_xlabel(f'$t$')
    a.set_xticks(np.arange(0,len(T),1))
    a.set_ylabel(f'$q_{C[i]}$')
    a.set_yticks(np.arange(0,max([ max(V[c]) for c in C ]) + 1,1))

  plot.tight_layout()
  figure.savefig(figure_path('structured_da-counter-1.svg'))
  plot.close(figure)

try:
  ticker_0()
  counter_0()
  delay_0()
  counter_1()
except Exception as e:
  import traceback
  print(traceback.format_exc())
