.. _results-sha:

==========================
Structured Hybrid Automata
==========================

.. _results-sha-definition:

Definition of a Structured Hybrid Automaton
-------------------------------------------

The :term:`HA` from :ref:`definition-ha` is reconstructed as a :term:`SHA` in :ref:`definition-sha`.
The discrete parts of its definition is defined in terms of a :term:`SDA` from
:ref:`definition-sda`,

.. target:: definition-sha
   :label: Definition 3.2.1 (Structured Hybrid Automaton)

**Definition 3.2.1 (Structured Hybrid Automaton)**:

  Let :math:`\mathbb{I} \subseteq \mathbb{R}` be a compact interval, :math:`A^D \triangleq (Q,E)` be a
  :term:`SDA` from :ref:`definition-sda` with states :math:`\mathbf{\Upsilon}^D`.

  A structured hybrid automaton (:term:`SHA`) is a tuple :math:`A \triangleq (A^D,X,U,V,W)`:

  1. where :math:`X` is a finite collection of :math:`N` continuous variables taking values in
     :math:`\mathbb{X}`;

     - with derivatives :math:`\dot{X}` taking values in :math:`\dot{\mathbb{X}}`;
     - and where :math:`\mathbb{X},\dot{\mathbb{X}} \subseteq \mathbb{C}^{N}` are open sets;
     - and where :math:`\mathbf{\Upsilon}^C \triangleq \mathbb{I} \times \mathbb{X}`,
       :math:`\mathbf{\Upsilon} \triangleq \mathbb{Q} \times \mathbf{\Upsilon}^C`, and :math:`\mathbf{\Psi}^C
       \triangleq \mathbb{I} \times \mathbb{X} \times \dot{\mathbb{X}}`;

  2. where :math:`U \subset \mathbb{F} \times \mathbb{G}` is a finite collection of :math:`M`
     activities;

     - where :math:`\mathbb{F}` is the set of activity equations :math:`f \colon \mathbf{\Psi}^C \to
       \mathbb{C}`;
     - and :math:`\mathbb{G}` is the set of activity guards :math:`g \colon \mathbb{Q} \to \mathbb{B}`;

  3. where :math:`V \subset X \times \hat{\mathbb{H}} \times \hat{\mathbb{G}}` is a finite
     collection of :math:`P` actions;

     - where :math:`\hat{\mathbb{H}}` is the set of action functions :math:`\hat{h} \colon
       \mathbf{\Psi}^C \to \mathbb{C}`;
     - and :math:`\hat{\mathbb{G}}` is the set of action guards :math:`\hat{g} \colon
       \mathbf{\Upsilon}^D \to \mathbb{B}`;
     - where :math:`\mathbf{\Upsilon}^D` are the states of :math:`A^D`;

  4. where :math:`W \subset Q \times \check{\mathbb{H}} \times \check{\mathbb{G}}` is a finite
     collection of :math:`O` roots;

     - where :math:`\check{\mathbb{H}}` is the set of root functions :math:`\check{h} \colon \mathbf{\Psi}^C \to
       \mathbb{C}`;
     - and :math:`\check{\mathbb{G}}` is the set of root guards :math:`\check{g} \colon \mathbb{Q}
       \to \mathbb{B}`;
     - and if :math:`w_0,w_1 \in W` then :math:`q_0 = q_1 \implies w_0 = w_1`.
       
  The sets :math:`\mathbf{\Upsilon}^C` and :math:`\mathbf{\Psi}^C` are called the continuous states
  and continuous solutions of :math:`A` respectively, and the set :math:`\mathbf{\Upsilon}` is
  called the states of :math:`A`.

The continuous dynamics of the :term:`SHA` is a :term:`DAE` defined in line with
:ref:`definition-dae`. The domain of the :term:`HA`, :math:`D(q)`, has been dropped, as it is
redundant and introduces additional complexity to :ref:`definition-ha-determinism`. As discussed in
:ref:`results-hst-determinism`, if it is the case that :math:`x \in D(q) \cap G(e)`, the
construction of the hybrid execution could either continue constructing the current continuous
execution, or start constructing a new discrete execution. This is in addition to the ambiguity
caused by :math:`x \in G(e_0) \cap G(e_1)` with :math:`e_0 \neq e_1`, where the construction could
branch into either transition. The former ambiguity is redundant, because the domain is, after all,
just another subset. A modeller could always construct a transition whose guard is :math:`X /
D(q)`. Finally, one has to deal with the case where :math:`x` is about to leave :math:`D(q)`, but
:math:`\nexists e` such that :math:`x \in G(e)`. Instead of having different types of bifurcations,
the domain is dropped, leaving only the ambiguity of :math:`x \in G(e_0) \cap G(e_1)`.

The continuous dynamics, which was represented as a vector field in :ref:`definition-ha`, has been
structured into a set of tuples of equation and guard, just like the discrete dynamics in
:ref:`definition-sda`. The :term:`SHA` can be represented directly in a computation with a large
:math:`\mathbb{Q}` as this set is decoupled from the definition of the continuous dynamics. The
guards of the activities, :math:`g \in \mathbb{G}`, are assumed to be of the same form as the guards
of a :term:`SDA`. The active and inactive activities are defined in the same manner as the
transitions of a :term:`SDA`. Thus an activity is active in a discrete state if :math:`g(q) =
\top`. :math:`G` is used as a shorthand for :math:`G^\top` and similarly for :math:`F`. The active
activity equations form a system of :math:`m` differential algebraic equations
:eq:`equation-sha-activity-equations`, which determine :math:`\dot{x}`, or :math:`\psi^C`, in
:math:`q`.

.. math::
   :label: equation-sha-activity-equations

   F(\psi^C) = 0

The resets of :ref:`definition-ha` are here called the actions of the automaton. The actions have
been structured into tuples of variable, function, and guard. The same notation used for active and
inactive activities is used to denote the active and inactive actions. :math:`\hat{x}` and
:math:`\hat{H}` deontes the action variables and functions active in :math:`\upsilon^D`. For the
sake of simplicity it will be assumed that :math:`\nexists (\upsilon^D,v_0,v_1) \in
\mathbf{\Upsilon}^D \times V \times V` such that :math:`v_0 \neq v_1` and :math:`x_0 = x_1`.

Roots are a new addition to the hybrid automaton. The guards of the :term:`SDA` are functions of
:math:`\Upsilon^D`. The :term:`SHA` needs to mediate the continuous and discrete dynamics of the
automaton. This is done in a similar way to how the relation :math:`[ f(x) > 0 ] \leftrightarrow [
\delta = 1 ]` from the :term:`MLD`\ s of :cite:p:`Bemporad1999` bridged a (discretised) continuous
relation with a discrete relation. The introduction of roots, and their function in the execution of
the automaton, is a formalisation of the event detecting approach to the simulation of a hybrid
system discussed in :ref:`background-hst-simulation`. For the sake of simplicity it was assumed in
the definition of :ref:`definition-sha` that there are no two roots referring to the same discrete
variable. The same notation used for active and inactive activities is used to denote the active and
inactive roots. :math:`\check{q}` and :math:`\check{H}` denotes the root variables and functions
active in :math:`q`.

Executions of a Structured Hybrid Automaton
-------------------------------------------

The execution of a :term:`SHA` is an interleaving of continuous and discrete executions. Two types
of specifically hybrid executions are used to mediate between the continuous and the discrete
executions. The action execution deals with the application of actions to the continuous state after
a discrete execution has converged. The root execution deals with the transformation of continuous
roots into discrete state. These four types of executions are termed subexecutions, and are the
components from which an execution of a :term:`SHA` is constructed. The discrete, action,
continuous, and root subexecutions are denoted :math:`\delta^D`, :math:`\delta^U`, :math:`\delta^C`,
and :math:`\delta^W` respectively. The length of a subexecution is defined as:

.. math::
   :label: equation-sha-subexecution-length

   |\delta| \triangleq \max(T) - \min(T)

Where :math:`T` is the discrete or continuous time trajectory of the subexecution. An infinite
subexecution has :math:`|\delta| = \infty`, a finite subexecution has :math:`|\delta| \neq \infty`,
an empty discrete subexecution has :math:`|\delta^D| = 1`, and a non-empty discrete subexecution has
:math:`|\delta^D| > 1`.

Before defining the root and continuous subexecutions, a helper function is constructed in
:ref:`definition-sha-root-function` which maps the sign of the root functions of a :term:`SHA` to
discrete values. This definition does not account for root functions that are identically zero. This
is a simplification in line with IDA from :cite:p:`Hindmarsh2005`, which is the software used in
this report for simulating :term:`DAE`\ s, :cite:p:`IDA2020`:

  | However, if an exact zero of any :math:`g_i` is found at a point :math:`t`, ida computes :math:`g` at :math:`t + \delta` for a small increment :math:`\delta`, slightly further in the direction of integration, and if any :math:`g_i(t + \delta) = 0`, ida stops and reports an error.

.. todo::

   Fix domain of :math:`\check{f}`.

.. raw:: latex

   \clearpage

.. target:: definition-sha-root-function
   :label: Definition 3.2.2 (Root Function)

**Definition 3.2.2 (Root Function)**:

  Let :math:`A` be a :term:`SHA`.

  The root function is a map :math:`\check{F} \colon \mathbb{Q} \times \mathbf{\Psi}^C \to
  \mathbb{Q}`:

  1. whose elements :math:`\check{f}_i \colon \mathbf{\Psi} \to \mathbb{Q}_i` is defined as:
     
     .. math::

       \check{f}_i \triangleq \begin{cases}
	  \exists \check{q}_j \in \check{q} | q_i = \check{q}_j \to \check{r}_j(\psi^C) \\
	  \nexists \check{q}_j \in \check{q} | q_i = \check{q}_j \to q_i \\
       \end{cases}

     - where :math:`R \colon \mathbf{\Psi}^C \to \mathbb{B}^o` is a map:

       .. math::

	 \check{r}_i \triangleq \begin{cases} 
	   [ \check{h}_i(\psi) > 0 ] \lor ([ \check{h}_i(\psi) = 0 ] \land [ \dot{\check{h}}_i(\psi) > 0 ]) \to \top \\
	   [ \check{h}_i(\psi) < 0 ] \lor ([ \check{h}_i(\psi) = 0 ] \land [ \dot{\check{h}}_i(\psi) < 0 ]) \to \bot
	 \end{cases}

     - and where :math:`\check{x}` and :math:`\check{H}(\psi^C)` are the :math:`o` active root variables
       and functions, with :math:`\check{H} \triangleq \check{H}^\top(q)` and :math:`\check{H}
       \colon \mathbf{\Psi} \to \mathbb{C}^o`.

The root subexecution in :ref:`definition-sha-root-subexecution` transfers the continuous roots detected
at some continuous solution, :math:`\psi^C_0`, to the current discrete state, :math:`q_0`. It is a
fixed size subexecution in two steps.

.. target:: definition-sha-root-subexecution
   :label: Definition 3.2.3 (Root Subexecution)

**Definition 3.2.3 (Root Subexecution)**:

  Let :math:`A` be a :term:`SHA`.

  A root subexecution is a tuple :math:`\delta \triangleq (q_0,\psi^C_0)`:

  1. with initial state :math:`q_0 \in \mathbb{Q}`, and :math:`\psi^C_0 \in \mathbf{\Psi}^C`;
  2. and time trajectory :math:`\mathbb{I} \triangleq \mathbb{Z} < 2`;
  3. and state trajectory :math:`q \colon \mathbb{I} \to \mathbb{Q}`, which is a map:

     .. math::

        q(i) \triangleq \begin{cases}
          [i = 0] \to q_0 \\
	  [i = 1] \to \check{F}(q_0,\psi^C_0)
        \end{cases}

     - where :math:`\check{F}` is the root function from :ref:`definition-sha-root-function`.

The execution of a :term:`SHA` will generate several :term:`DAE` initial value problems. The
continuous subexecution in :ref:`definition-sha-continuous-subexecution` restates the solution to such a
problem from :ref:`definition-dae-solvability`. This solutions defined to end on the detection of a
root. If a continuous subexecution ends with one or more roots, the next subexecution will be a root
subexecution to process these roots. There is no longer any ambiguity between the continued
construction of the continuous subexecution and the construction of a new discrete subexecution, as
was the case in a :term:`HA` when :math:`x \in D(q) \cap G(e)`. A continuous root of a :term:`SHA`
always ends the current continuous subexecution.

.. raw:: latex

   \clearpage

.. target:: definition-sha-continuous-subexecution
   :label: Definition 3.2.4 (Continuous Subexecution)

**Definition 3.2.4 (Continuous Subexecution)**:

  Let :math:`A` be a :term:`SHA`, and :math:`C^k(\mathbb{I},\mathbb{X})` denote the vector space of
  all :math:`k`-times continuously differentiable functions from the real interval
  :math:`\mathbb{I}` into the vector space :math:`\mathbb{X}`.

  A continuous subexecution is a tuple :math:`\delta \triangleq (\upsilon_0)`:

  1. with initial state :math:`\upsilon_0 \triangleq (q_0,t_0,x_0) \in \mathbf{\Upsilon}`;
  2. and time trajectory :math:`T \subseteq \mathbb{I}`, which is an interval with :math:`\min(T)
     \triangleq t_0`;
  3. and state trajectory :math:`x \in C^1(T,\mathbb{X})` which is a solution of the :term:`DAE`
     initial value problem :math:`(F,t_0,x_0)`,
     
     - where :math:`F \triangleq F^\top(q_0)` are the active activity equations of :math:`q_0`;

  4. and if :math:`\exists t_m \in T` such that :math:`\check{F}(q_0,\psi(t_0)) \neq
     \check{F}(q_0,\psi(t_m))`, then :math:`T \triangleq [t_0,t_m]`, else :math:`t_m \triangleq
     \infty` and :math:`T \triangleq [t_0,t_m)`;

     - where :math:`\psi^C(t) \triangleq (t,x(t),\dot{x}(t))` is the solution of :math:`x` at :math:`t`;
     - and where :math:`\check{F} \triangleq \check{F}^\top(q_0)` is the root function from
       :ref:`definition-sha-root-function`.

The detection of a root is one of the potential discrete events discussed in
:ref:`background-hst-simulation`. The corresponding change in discrete state, which was captured by
:ref:`definition-sha-root-subexecution`, is followed by a mode selection problem. The mode selection
problem is here called a discrete subexecution. The discrete subexecution needs to propagate the
continuous state of the hybrid execution. The discrete execution from
:ref:`definition-sda-execution` is extended with continuous state in
:ref:`definition-sha-discrete-subexecution`. A discrete subexecution is otherwise identical to a
discrete execution. For a discrete subexecution it will always be the case that there exists an
earlier subexecution that defines some continuous state trajectory. The discrete subexecution simply
passes this along.

.. target:: definition-sha-discrete-subexecution
   :label: Definition 3.2.5 (Discrete Subexecution)
	   
**Definition 3.2.5 (Discrete Subexecution)**:

  Let :math:`A` be a :term:`SHA`.

  A discrete subexecution is a tuple :math:`\delta \triangleq (\upsilon_0)` with:

  1. initial state :math:`\upsilon_0 \triangleq (q_0,t_0,x_0) \in \mathbf{\Upsilon}`;
  2. and that is otherwise equivalent to the discrete execution :math:`\delta^D \triangleq (q_0)`
     in :ref:`definition-sda-execution`.

At the end of a discrete subexecution the :term:`SHA` might apply a set of actions to the continuous
stat. This is done to, for example, ensure the satisfaction of the algebraic constraints of the new
active activity equations. This application is captured in a helper function in
:ref:`definition-sha-action-function`.

.. target:: definition-sha-action-function
   :label: Definition 3.2.6 (Action Function)

**Definition 3.2.6 (Action Function)**:

  Let :math:`A` be a :term:`SHA`.

  The continuous action function is a map :math:`\hat{F} \colon \mathbb{Q} \times \mathbf{\Psi}^C
  \to \mathbb{X}`:

  1. whose elements :math:`\hat{f}_i \colon \mathbf{\Psi}^C \to \mathbb{C}` are maps:

     .. math::

	\hat{f}_i(\psi^C) \triangleq \begin{cases}
	  \exists \hat{x}_j \in \hat{x} | x_i = \hat{x}_j \to \hat{h}_j(\psi^C) \\
	  \nexists \hat{x}_j \in \hat{x} | x_i = \hat{x}_j \to x_i \\
	\end{cases}

     where :math:`\hat{x}` and :math:`\hat{H}(\psi^C)` are the :math:`p` active action variables and
     functions, with :math:`\hat{H} \triangleq \hat{H}^\top(q)` and :math:`\hat{H} \colon
     \mathbf{\Psi} \to \mathbb{C}^p`.

The action subexecution of :math:`\upsilon` applies the active actions to the continuous state, using
the action function in :ref:`definition-sha-action-function`, and then recomputes the discrete state
based on the new continuous state, using the root function in
:ref:`definition-sha-root-function`. The root function was defined in terms of a continuous solution
:math:`\psi^C \in \mathbf{\Psi}^C`, which requires the computation of a continuous solution from
:math:`\upsilon`. This is captured in :ref:`definition-sha-implicit-continuous-solution`.

.. target:: definition-sha-implicit-continuous-solution
   :label: Definition 3.2.7 (Implicit Continuous Solution)

**Definition 3.2.7 (Implicit Continuous Solution)**:

  Let :math:`A` be a :term:`SHA`.

  The implicit continuous solution of :math:`\upsilon_0 \triangleq (q_0,t_0,x_0) \in
  \mathbf{\Upsilon}` is :math:`\phi \colon \mathbf{\Upsilon} \to \Psi^C`, which is a map:

  - where :math:`\phi(\upsilon_0) \triangleq (t_0,x(t_0),\dot{x}(t_0))` is a solution of :math:`x` at :math:`t_0`,
  - and where :math:`x` is a solution to the :term:`DAE` initial value problem :math:`(F,t_0,x_0)`;
  - and where :math:`F \triangleq F^\top(q_0)` are the active activity equations of :math:`q_0`.

The action subexecution is a fixed size subexecution in two steps. It might require consistent
initialisation of the continuous state to correctly compute the next discrete state. Consistent
initialisation was discussed in :ref:`Simulation of a DAE` and is also relevant to the continuous
subexecution. It is still a matter of ergonomics, as the modeller could instead be required to
ensure that actions result in a continuous state consistent with the new continuous dynamics. The
modeller could also be required to specify an initial continuous state that is consistent with the
initial continuous dynamics. Ergonomic practical computation requires consistent initialisation to
be done, when possible, by the implementation, for both continuous and action subexecutions.

.. target:: definition-sha-action-subexecution
   :label: Definition 3.2.8 (Action Subexecution)

**Definition 3.2.8 (Action Subexecution)**:

  Let :math:`A` be a :term:`SHA`.

  An action subexecution is a tuple :math:`\delta \triangleq (\upsilon_0)`:

  1. with initial state :math:`\upsilon_0 \triangleq (q_0,t_0,x_0) \in \mathbf{\Upsilon}`;
  2. and time trajectory :math:`\mathbb{I} \triangleq \mathbb{Z} < 2`;
  3. and continuous state trajectory :math:`x \colon \mathbb{I} \to \mathbb{X}`, which is a map:

     .. math::

        x(i) \triangleq \begin{cases}
          [i = 0] \to x_0 \\
	  [i = 1] \to \hat{F}(q_0,\psi^C_0) 
        \end{cases}
  
     - where :math:`\psi^C_0 \triangleq \phi(\upsilon_0)` is the implicit continuous solution of
       :math:`\upsilon_0` from :ref:`definition-sha-implicit-continuous-solution`;

  4. and discrete state trajectory :math:`q \colon \mathbb{I} \to \mathbb{Q}`, which is a map:

     .. math::

        q(i) \triangleq \begin{cases}
          [i = 0] \to q_0 \\
	  [i = 1] \to \check{F}(q_0,\psi^C_1)
        \end{cases}
  
     - where :math:`\psi^C_1 \triangleq \phi(\upsilon^\prime_0)` is the implicit continuous solution
       of :math:`\upsilon^\prime_0 \triangleq (q_0,t_0,x(1))` from
       :ref:`definition-sha-implicit-continuous-solution`;
     - and :math:`x` is the continuous state trajectory defined above.

The execution of a :term:`SHA` is a sequence of subexecutions of a particular structure. A finite
continuous subexecution will be followed by a root subexecution, an empty discrete subexecution will be
followed by a continuous subexecution, and so on. This structure is shown on diagram form in
:numref:`figure-sha-execution`.

.. graphviz::
   :align: center
   :caption: The execution of a Structured Hybrid Automaton
   :name: figure-sha-execution

   digraph{
     label=""
     center=true
     node [style=filled;fillcolor="#f0c674";shape=box]
     edge [minlen=1]
     v0[label="";fillcolor="#000000";shape=point]
     v1[label="Root\nSubexecution";group=g1]
     v2[label="Discrete\nSubexecution"]
     v3[label="Action\nSubexecution";group=g1]
     v4[label="Continuous\nSubexecution"]
     {
       rank=same;v1;
     }
     {
       rank=same;v2;v4;
     }
     v0 -> v1
     v1 -> v2
     v2 -> v4[label="|&#948;| = 1"]
     v2 -> v3[dir=back;label="  1 < |&#948;| < &#8734; "]
     v2 -> v3
     v1 -> v4[dir=back;label="  |&#948;| &#8800; &#8734;"]
   }

The execution of a :term:`SHA` is finally stated in :ref:`definition-sha-execution`. Note that since
:math:`|\delta^U| = 2` and :math:`|\delta^W| = 2`; it is never the case that a root or action
subexecution is the last subexecution in such an execution.

.. raw:: latex

   \clearpage

.. target:: definition-sha-execution
   :label: Definition 3.2.9 (Hybrid Execution)
	   
**Definition 3.2.9 (Hybrid Execution)**:

  Let :math:`A` be a :term:`SHA`.

  A hybrid execution is a tuple :math:`\omega \triangleq (\upsilon_0,\Delta)`:

  1. with initial state :math:`\upsilon_0 \in \mathbf{\Upsilon}`;

  2. and subexecutions :math:`\Delta \triangleq \{ \delta_0, \ldots \}`, with initial subexecution
     :math:`\delta^W_0 \triangleq (q_0,\psi^C_0)`:

     - where :math:`q_0` is the initial discrete state of :math:`\upsilon_0`;
     - and where :math:`\psi^C_0 \triangleq \phi(\upsilon_0)` is the implicit continuous solution
       of :math:`\upsilon_0` from :ref:`definition-sha-implicit-continuous-solution`;

  3. and time trajectory :math:`I \subseteq \mathbb{Z}`, which is a sequence with :math:`\min(I) \triangleq 0`:

     - and if :math:`\exists i \in I` such that :math:`|\delta_i| = \infty`, then :math:`i_m
       \triangleq i` and :math:`I \triangleq [i_0,i_m]`;
     - else :math:`i_m \triangleq \infty` and :math:`I \triangleq [i_0,i_m)`;
       
  4. and :math:`\forall \delta^C_i \triangleq (q_{i,0},t_{i,0},x_{i,0}) \in \Delta`, where :math:`i \neq i_m`,
     there :math:`\exists \delta^W_{i+1} \triangleq (q_{i,0},\psi^C_{i,m}) \in \Delta`:

     - where :math:`\psi^C_{i,m} \triangleq (t_{i,m},x(t_{i,m}),\dot{x}(t_{i,m}))` is the solution of
       :math:`x` at :math:`t_{i,m} \triangleq \max(T)`;
     - and where :math:`x` and :math:`T` are the continuous state and time trajectories of
       :math:`\delta^C_i`;

  5. and :math:`\forall \delta^W_i \triangleq (q_{i,0},\psi^C_{i,0}) \in \Delta` there :math:`\exists
     \delta^D_{i+1} \triangleq (q_{i,m},t_{i,0},x_{i,0}) \in \Delta`:
     
     - where :math:`q_{i,m} \triangleq q(t_{i,m})` is the state of :math:`q` at :math:`t_{i,m} \triangleq \max(T)`;
     - and where :math:`q` and :math:`T` is the discrete state and time trajectories of
       :math:`\delta^W_i`;
     - and where :math:`t_{i,0}` and :math:`x_{i,0}` is the state of :math:`\psi^C_{i,0} \triangleq
       (t_{i,0},x_{i,0},\dot{x}_{i,0})`;

  6. and :math:`\forall \delta^D_i \triangleq (q_{i,0},t_{i,0},x_{i,0}) \in \Delta`, where :math:`i \neq i_m
     \land |\delta^D_i| \neq 1`, there :math:`\exists \delta^V_{i+1} \triangleq (q_{i,m},t_{i,0},x_{i,0}) \in
     \Delta`:

     - where :math:`q_{i,m} \triangleq q(t^D_m)` is the state of :math:`q` at :math:`t^D_{i,m} \triangleq \max(T)`;
     - and where :math:`q` and :math:`T` is the discrete state and time trajectories of
       :math:`\delta^D_i`;
       
  7. and :math:`\forall \delta^V_i \triangleq (q_{i,0},t_{i,0},x_{i,0}) \in \Delta` there :math:`\exists
     \delta^D_{i+1} \triangleq (q_{i,m},t_{i,0},x_{i,m}) \in \Delta`:

     - where :math:`q_{i,m} \triangleq q(t_{i,m})` and :math:`x_{i,m} \triangleq x(t_{i,m})` is the state of :math:`q`
       and :math:`x` at :math:`t_{i,m} \triangleq \max(T)`;
     - and where :math:`q`, :math:`x`, and :math:`T` are the discrete state, continuous state, and
       time trajectories of :math:`\delta^V_t`;

Simulation of Structured Hybrid Automata
----------------------------------------

Here the simulation algorithms of Omola and Sol are revisited in light of the execution of a
:term:`SHA`.

This execution is a formalisation of the event detecting approach to the simulation of hybrid
systems. It can be mapped to the blocks of the event detecting simulation algorithm described in
:numref:`figure-ha-simulation-omola`. The "Solve DAE problem" block corresponds to the continuous
subexecutions. Final time was dropped from the definition of the continuous subexecutions, and all
executions in this report are maximal by definition. The root and discrete subexecution covers the
"Check Invariants" and "Any events" blocks, and the action subexecution covers the "Fire Event" and
"Find Consistent Initial Values" block.

In the dynamic processing algorithm of Sol, the :term:`DDP` transforms :term:`DAE`\ s to
:term:`ODE`\ s. This is done in response to changes in the continuous dynamics of the model.  This
is a relatively expensive procedure compared to integration, and one that should be avoided if
possible. The execution of a :term:`SHA` formalises the notion of potential structural change. Any
discrete subexecution with state trajectory :math:`q`, with activity guards :math:`G(q_0) \neq
G(q_m)`, is a potential structural change. The execution of a :term:`SHA` identifies the moments the
:term:`DDP` needs to be run. This is only in the case of a continuous subexecution, or an action
subexecution with a non-empty action set. In the former case it needs to run because a continuous
subexecution is about to be constructed. In the latter case it needs to run because the action
subexecution needs the implicit solution of the current state defined in
:ref:`definition-sha-implicit-continuous-solution`. This gives a modeller and algorithm a better
chance of synchronising structural changes of the continuous dynamics.

.. raw:: latex

   \clearpage

