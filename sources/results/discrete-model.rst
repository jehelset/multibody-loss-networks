.. _results-flow_network_da:

===========================
Discrete Flow Network Model
===========================

The discrete model constructed here describes arbitrary distributions of waterbodies, and patterns
of flow, in a network of pipes and valves. It is a discrete model of a multibody flow network
constructed using the :term:`SDA` framework defined in :ref:`results-sda`. The construction is done
with a :term:`SHA` model of a multibody flow network in mind. It is assumed that the continuous
dynamics of the waterbodies of this :term:`SHA` are inelastic.
 
Discrete Domains, Variables and States
--------------------------------------

The set of discrete domains of the variables in the model is :math:`\mathbb{Q}^z \subset
\mathbb{Z}`, with :math:`z \in Z`. Each domain and its members are associated with a particular
semantic: volumes, flow, and so on. The domains are tabulated in
:numref:`table-results-discrete-domains`. The variables are defined in terms of sets,
:math:`Q^\mathbf{w}`, where :math:`W = \{ \mathbf{w} \ldots \}`. :math:`z^\mathbf{w} \in Z` refers
to the corresponding domain index of a variable :math:`q^\mathbf{w}_{\mathbf{i}}`, and
:math:`Z^\mathbf{w}` denotes the corresponding domain. The variables of the automaton is union in
equation :eq:`equation-sda-model-variables`, and the domain of the automaton the product in equation
:eq:`equation-sda-model-domain`

.. math::
   :label: equation-sda-model-variables

   \displaystyle \bigcup_{\mathbf{w} \in W} Q^\mathbf{w}

.. math::
   :label: equation-sda-model-domain

   \mathbb{Q} = \displaystyle \prod_{\mathbf{w} \in W} \mathbb{Q}^{z^\mathbf{w}}

Every discrete variable is associated with a system component from :ref:`results-flow_network`. More
than one discrete variable can be associated with a component. The subscripts of each variable refer
to the subscripts of the corresponding component. If variables of type :math:`Q^{\mathbf{w}}` are
associated with halfpipes, then the discrete variable :math:`q^{\mathbf{w}}_{\mathbf{i}}` is
associated with the halfpipe :math:`c^{1,1}_{\mathbf{i}}`. :math:`c^{\mathbf{w}}` denotes the
corresponding type of the component of a discrete variable, and :math:`C^{\mathbf{w}}` denotes the
set of such components. The sets of discrete variables are tabulated in
:numref:`table-results-discrete-tub-variables` through
:numref:`table-results-discrete-endpipe-variables`.  These variables are split into two groups. The
first group consists of variables representing waterbodies. This group consists of volumes, joints,
flows, and control variables. The second group of variables detect events. This group consists of
logic and event variables. The cardinalities of :math:`|Q|` and :math:`|\mathbb{Q}|` are defined in
equation :eq:`equation-sda-variable-cardinality` and equation :eq:`equation-sda-domain-cardinality`.

.. math::
   :label: equation-sda-variable-cardinality

   |Q| = \displaystyle \sum_{\mathbf{w} \in W} |C^{\mathbf{w}}| = 14|V| + 25|E|

.. math::
   :label: equation-sda-domain-cardinality
  
   |\mathbb{Q}| = \displaystyle \prod_{\mathbf{w} \in W}|Z^{\mathbf{w}}|^{|C^{\mathbf{w}}|} = 2^{14|V|} \cdot 2^{25|E|})

For the simplest, non-trivial network, as seen in :numref:`figure-results-system-0`, there is
:math:`2^{14 \cdot 2} \cdot 2^{25 \cdot 1} = 9007199254740992` different discrete states, which
underlines the rationale of :ref:`definition-sda`.

.. _table-results-discrete-domains:

.. table:: Discrete Domains
   :align: center

   +--------------------------------------------------------------------------------------------------------+
   |Domain                                                                                                  |
   +-----------+---------------------+-----------------------------------------------+----------------------+
   |Semantic   |Notation             |Members                                        |Count                 |
   +===========+=====================+===============================================+======================+
   |Volume     |:math:`\mathbb{Q}^0` |:math:`\begin{cases} 0 \to \textit{dry} \\ 1   |:math:`|\mathbb{Q}^z| |
   |           |                     |\to \textit{wet} \end{cases}`                  |= 2`                  |
   |           |                     |                                               |                      |
   +-----------+---------------------+                                               |                      |
   |Joint      |:math:`\mathbb{Q}^1` |                                               |                      |
   |           |                     |                                               |                      |
   |           |                     |                                               |                      |
   +-----------+---------------------+-----------------------------------------------+                      |
   |Flow       |:math:`\mathbb{Q}^2` |:math:`\begin{cases} 0 \to                     |                      |
   |           |                     |\textit{non-negative} \\ 1 \to                 |                      |
   |           |                     |\textit{non-positive}\end{cases}`              |                      |
   +-----------+---------------------+-----------------------------------------------+                      |
   |Logic      |:math:`\mathbb{Q}^3` |:math:`\begin{cases} 0 \to \textit{bot} \\ 1   |                      |
   |           |                     |\to \textit{top} \end{cases}`                  |                      |
   |           |                     |                                               |                      |
   +-----------+---------------------+-----------------------------------------------+                      |
   |Event      |:math:`\mathbb{Q}^4` |:math:`\begin{cases} 0 \to \textit{none} \\ 1  |                      |
   |           |                     |\to \textit{detected} \end{cases}`             |                      |
   |           |                     |                                               |                      |
   |           |                     |                                               |                      |
   |           |                     |                                               |                      |
   +-----------+---------------------+-----------------------------------------------+                      |
   |Control    |:math:`\mathbb{Q}^5` |:math:`\begin{cases}0 \to \textit{off} \\ 1    |                      |
   |           |                     |\to \textit{on} \end{cases}`                   |                      |
   +-----------+---------------------+-----------------------------------------------+----------------------+

.. _table-results-discrete-valve-variables:

.. table:: Discrete Valve Variables
   :align: center

   +------------------------------------------------------------------------------+
   |Discrete Valve Variables                                                      |
   +---------------------+-----------+--------------------+-----------------------+
   |Domain               |Semantic   |Description         |Notation               |
   +=====================+===========+====================+=======================+
   |:math:`\mathbb{Q}^1` |Joint      |Joint               |:math:`q^{0,0}_i`      |
   +---------------------+-----------+--------------------+-----------------------+
   |:math:`\mathbb{Q}^4` |Event      |Seeding             |:math:`q^{0,1}_i`      |
   |                     |           +--------------------+-----------------------+
   |                     |           |Colliding           |:math:`q^{0,2}_i`      |
   |                     |           +--------------------+-----------------------+
   |                     |           |Receding            |:math:`q^{0,3}_i`      |
   +---------------------+-----------+--------------------+-----------------------+

.. _table-results-discrete-tub-variables:

.. table:: Discrete Tub Variables
   :align: center

   +------------------------------------------------------------------------+
   |Discrete Tub Variables                                                  |
   +---------------------+-----------+---------------+----------------------+
   |Domain               |Semantic   |Description    |Notation              |
   +=====================+===========+===============+======================+
   |:math:`\mathbb{Q}^0` |Volume     |Volume         |:math:`q^{0,0,0}_i`   |
   +---------------------+-----------+---------------+----------------------+
   |:math:`\mathbb{Q}^1` |Joint      |Joint          |:math:`q^{0,0,1}_i`   |
   +---------------------+-----------+---------------+----------------------+
   |:math:`\mathbb{Q}^2` |Flow       |Flow           |:math:`q^{0,0,2}_i`   |
   +---------------------+-----------+---------------+----------------------+
   |:math:`\mathbb{Q}^3` |Logic      |Source         |:math:`q^{0,0,3}_i`   |
   +---------------------+-----------+---------------+----------------------+
   |:math:`\mathbb{Q}^4` |Event      |Branching      |:math:`q^{0,0,4}_i`   |
   |                     |           +---------------+----------------------+
   |                     |           |Seeding        |:math:`q^{0,0,5}_i`   |
   |                     |           +---------------+----------------------+
   |                     |           |Cutting        |:math:`q^{0,0,6}_i`   |
   |                     |           +---------------+----------------------+
   |                     |           |Receding       |:math:`q^{0,0,7}_i`   |
   +---------------------+-----------+---------------+----------------------+
   |:math:`\mathbb{Q}^5` |Control    |Leg Control    |:math:`q^{0,0,8}_i`   |
   |                     |           +---------------+----------------------+
   |                     |           |Flow Control   |:math:`q^{0,0,9}_i`   |
   +---------------------+-----------+---------------+----------------------+

.. _table-results-discrete-midpipe-variables:

.. table:: Discrete Midpipe Variables
   :align: center

   +--------------------------------------------------------------------------+
   |Discrete Midpipe Variables                                                |
   +---------------------+-----------+--------------------+-------------------+
   |Domain               |Semantic   |Description         |Notation           |
   +=====================+===========+====================+===================+
   |:math:`\mathbb{Q}^1` |Joint      |Joint               |:math:`q^{1,0,0}_i`|
   +---------------------+-----------+--------------------+-------------------+
   |:math:`\mathbb{Q}^4` |Event      |Branching           |:math:`q^{1,0,1}_i`|
   |                     |           +--------------------+-------------------+
   |                     |           |Colliding           |:math:`q^{1,0,2}_i`|
   |                     |           +--------------------+-------------------+
   |                     |           |Receding            |:math:`q^{1,0,3}_i`|
   +---------------------+-----------+--------------------+-------------------+
   |:math:`\mathbb{Q}^5` |Control    |Joint Control       |:math:`q^{1,0,4}_i`|
   +---------------------+-----------+--------------------+-------------------+

.. _table-results-discrete-halfpipe-variables:

.. table:: Discrete Halfpipe Variables
   :align: center

   +-------------------------------------------------------------------------------+
   |Discrete Halfpipe Variables                                                    |
   +---------------------+-----------+--------------------+------------------------+
   |Domain               |Semantic   |Description         |Notation                |
   +=====================+===========+====================+========================+
   |:math:`\mathbb{Q}^1` |Volume     |Leg                 |:math:`q^{1,1,0}_{i,j}` |
   +---------------------+-----------+--------------------+------------------------+
   |:math:`\mathbb{Q}^3` |Control    |Leg Control         |:math:`q^{1,1,1}_{i,j}` |
   +---------------------+-----------+--------------------+------------------------+

.. _table-results-discrete-endpipe-variables:

.. table:: Discrete Endpipe Variables
   :align: center

   +-------------------------------------------------------------------------------+
   |Discrete Endpipe Variables                                                     |
   +---------------------+-----------+--------------------+------------------------+
   |Domain               |Semantic   |Description         |Notation                |
   +=====================+===========+====================+========================+
   |:math:`\mathbb{Q}^1` |Joint      |Joint               |:math:`q^{1,2,0}_{i,j}` |
   +---------------------+-----------+--------------------+------------------------+
   |:math:`\mathbb{Q}^2` |Flow       |Flow                |:math:`q^{1,2,1}_{i,j}` |
   +---------------------+-----------+--------------------+------------------------+
   |:math:`\mathbb{Q}^3` |Logic      |Source              |:math:`q^{1,2,3}_{i,j}` |
   +---------------------+-----------+--------------------+------------------------+
   |:math:`\mathbb{Q}^4` |Event      |Branching           |:math:`q^{1,2,4}_{i,j}` |
   |                     |           +--------------------+------------------------+
   |                     |           |Seeding             |:math:`q^{1,2,5}_{i,j}` |
   |                     |           +--------------------+------------------------+
   |                     |           |Receding            |:math:`q^{1,2,6}_{i,j}` |
   |                     |           +--------------------+------------------------+
   |                     |           |Cutting             |:math:`q^{1,2,7}_{i,j}` |
   +---------------------+-----------+--------------------+------------------------+
   |:math:`\mathbb{Q}^5` |Control    |Flow Control        |:math:`q^{1,2,8}_{i,j}` |
   +---------------------+-----------+--------------------+------------------------+

Discrete Waterbody Variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The control variables correspond to the discrete half of the roots of in :ref:`definition-sda`.
This is the mechanism whereby the discretisation of continuous roots propagate into the discrete
automaton and drive the discrete dynamics. Control variables are in this automaton modelled as
impulses that are cleared out after the first timestep.

Volume variables describe which halfpipes and tubs are part of some waterbody. A waterbody is
composed by volume variables called legs. The joint variables connect legs together. Even if both
legs of a halfpipe are wet they are only part of the same waterbody if the midpipe joint is wet.
Similarly, for the valve joints and the legs of the incident components (
:ref:`definition-incident-components`). Flow variables of wet legs describe the direction of flow.
In a hybrid model the corresponding flow control variables might track the sign of the continuous
flow. The flow variables of dry legs will limit the seeding of waterbodies. Perhaps the outside
pressure of a dry tub is high enough to prevent a waterbody internal to the pipe network from
entering the tub. It might even be about to push water away from the valve, potentially cutting a
waterbody into several pieces.

To visualise the discrete state of the model, discrete state diagrams will be used. These annotate
the network diagrams of :ref:`results-flow_network`, like :numref:`figure-results-system-0`, with
discrete variables in some particular state. These diagrams are also programatically generated with
:cite:p:`RHelset2022`. Variables of a particular component is drawn on top of the component. Blue
coloring indicates wet volumes, joints or active sources, while white coloring means dry. The arrows
indicate the value of the directions of flow. Non-negative states point downwards. Note that
non-negative flow in a tub variable means the tub is not draining.

Consider for example a structured discrete automaton, :math:`A_0`, of the system :math:`G_0` from
:ref:`example-network-0` in the discrete state, :math:`q_0`, as visualised in
:numref:`figure-results-discrete-state-0`. Water is entering the pipe from both valves, but the pipe
is not yet filled, as can be seen by the white coloring of the midpipe. The blue, downward pointing
triangles of each tub indicates that both tubs are sources, and are not draining water from the
connected waterbody. In a hybrid simulation, during the course of the construction of a continuous
trajectory, the pipe would eventually fill. This would cause a root to be detected for :math:`[
V_{max} - (V^{1,1}_{0,0} + V^{1,1}_{0,1}) < 0 ]`, where :math:`V^{1,1}_{0,j}` is the continuous
variable of the volume in the halfpipe. The hybrid execution would break out of the continuous
subexecution, and construct a discrete subexecution, :math:`\delta^D`. The final state of
:math:`\delta^D`, :math:`q(t_m)`, would eventually be used to construct the next continuous
subexecution.

The initial state :math:`q(t_0)` of this discrete trajectory would have the midpipe joint control
variable set high, :math:`q^{1,0}_{0} = 1`, to indicate that the pipe has just been filled. This
would cause a collision event to be detected at the midpipe, and a action subexecution would apply
actions to ensure that the new set of algebraic constraints are satisfied. For an inelastic
continuous model, this might mean computing new values for the flows in the system. After the
collision, the flow through :math:`c^{0,0}_0` must now be equal the flow through
:math:`c^{0,0}_1`. The eventual discrete state of an inelastic collision, :math:`q_1`, is visualised
in :numref:`figure-results-discrete-state-0`. The midpipe joint variable is here wet and colored
blue.

.. figure:: discrete-model-state-0-figure.svg
   :align: center
   :name: figure-results-discrete-state-0

   Visualisation of :math:`q_0` for :math:`A_0` before collision. Water is entering the network from
   both valves.

.. figure:: discrete-model-state-1-figure.svg
   :align: center
   :name: figure-results-discrete-state-1

   Visualisation of :math:`q_1` for :math:`A_0` after collision. Water is flowing through the
   network from the top valve to the bottom valve.

Discrete Event Detecting Variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The logic and event variables together detect events during the course of an execution. Event
variables are set high whenever an event is detected. An event might spuriously be detected during
the construction of an execution, but disappear before the execution is complete. Only the final
state of a discrete execution determines which events were detected. The events detected in the
previous executions be set high in the first timestep of the next discrete execution. All event
variables are equipped with an initial transition that resets detected events. This mechanism
structures the execution. In the initial step of an execution, reactions to the previously detected
events are computed. In the eventual steps these reactions propagate and new events will be
detected.

Source variables are logic variables that describe whether or not the incident component of a valve
(:ref:`definition-incident-components`) is supplying water to the valve. The count of these
variables is used to determine whether a dry valve is about to become wet, or whether a wet valve is
about to become dry. This counting principle was illustrated in :ref:`example-sda-counting`. In
:numref:`figure-results-discrete-state-0` both tubs are sourcing their valve and their flow
triangles are shaded blue. Neither halfpipe is sourcing and so their flow triangles are shaded
white. After the collision :numref:`figure-results-discrete-state-1` the flow through the lower
halfpipe and valve have changed sign. The lower tub is no longer a source to the valve, instead the
lower halfpipe has become a source.

Event variables come in five types. Branching and seeding describe the imminent expansion of a
waterbody. Branch events are detected when a waterbody is about to expand into a valve, or a
midpipe. This might cause water to flow into dry halfpipes and tubs. This is represented by seed
events. Receding events capture the drying up of wet tubs and halfpipes. This might cause water to
stop flowing into wet halfpipes and tubs. This is represented by cutting events. Collision events
describe when a waterbody branches into an already wet valve, or when more than one waterbody
branches into a dry valve or midpipe.

Lets revisit the example in :numref:`figure-results-discrete-state-0`. This is a collision of two
waterbodies, and a collision event was detected. This detection would happen in two steps. First,
because the midpipe joint control variable was set high in the initial state, :math:`q_0`, the
transition that guards the branching event would evaluate to true, and in the next discrete state,
:math:`q_2`, the branching event variable for the midpipe would go high, as seen in
:numref:`figure-results-discrete-state-2`. Branching is highlighted in dark yellow. A collision
event would then be detected as both halfpipes are already wet. In the next state, :math:`q_3`, the
midpipe collision event would also go high, as seen in :numref:`figure-results-discrete-state-3`.

.. figure:: discrete-model-state-2-figure.svg
   :align: center
   :name: figure-results-discrete-state-2

   Visualisation of :math:`q_2` for :math:`A_0`. The pipe has become completely full, and a midpipe
   branching event has been detected.

.. figure:: discrete-model-state-3-figure.svg
   :align: center
   :name: figure-results-discrete-state-3

   Visualisation of :math:`q_3` for :math:`A_0`. A midpipe collision event has been detected, as the
   midpipe was branching, and both sides of the midpipe were wet.

Speeding happens only in and around valves, and not midpipes. All dry incident components of a valve
can potentially be seeded. A common scenario is when the tub of a completely dry network suddenly
becomes wet. Perhaps a gate was opened, and power production is about to start. The waterbody in the
tub branches into the valve, and causes the valve and endpipe :math:`c^{1,2}_{0,0}` to seed.  The
resulting end-state, :math:`q_4`, is visualised in
:numref:`figure-results-discrete-state-4`. Branching events are colored in dark orange, and seeding
events in bright orange.

.. figure:: discrete-model-state-4-figure.svg
   :align: center
   :name: figure-results-discrete-state-4

   Visualisation of :math:`q_4` for :math:`A_0`. 

Receding events are controlled with the halfpipe and tub leg control variables, and would be
controlled by continuous dynamics. A wet tub recedes, for example, when the water level in the tub
sinks below the valve. Recall that valves also represent gates. A wet halfpipe recedes when its
volume goes to zero. If a recession causes the number of sources in a valve to be zero it causes a
cutting event in any other wet leg incident to the valve. A hybrid model would then set the flows
into these legs, or tubs, to zero.
   
Discrete Transitions
--------------------

The symbols of the discrete automaton in :cite:p:`Hopcroft1979` were dropped in
:ref:`definition-sda`, yet are still useful to describe the dynamics of the model. Every transition
is associated with a symbol. The symbols of the discrete model are grouped into :math:`|Z|`
different sets, :math:`S^z = \{ s^z_y \ldots \}`, each uniquely associated with a discrete
domain. The set of the symbols, or the alphabet, of the discrete model, is the union in equation
:eq:`equation-da-symbols`, and the symbols are tabulated in table
:numref:`table-results-discrete-symbols`.

.. math:: 
   :label: equation-da-symbols

   S = \displaystyle \bigcup_{z \in Z} S^z

.. _table-results-discrete-symbols:

.. table:: Discrete Symbols
   :align: center

   +----------------------------------------------------------------------+
   |Symbols                                                               |
   +----------+----------------------------------------+------------------+
   |Semantic  |Notation                                |Count             |
   +==========+========================================+==================+
   |Volume    |:math:`S^0 = \begin{cases} s^0_0 \to    |:math:`|S^0| = 2` |
   |          |\textit{seed} \\ s^0_1 \to \textit{reap}|                  |
   |          |\end{cases}`                            |                  |
   +----------+----------------------------------------+------------------+
   |Joint     |:math:`S^z = \begin{cases} s^z_0 \to    |:math:`|S^z| = 1` |
   |          |\textit{flip} \end{cases}`              |                  |
   +----------+                                        |                  |
   |Flow      |                                        |                  |
   +----------+                                        |                  |
   |Logic     |                                        |                  |
   +----------+                                        |                  |
   |Event     |                                        |                  |
   +----------+                                        |                  |
   |Control   |                                        |                  |
   +----------+----------------------------------------+------------------+


A transition, :math:`e^{\mathbf{w},j}_{\mathbf{i}}`, will be defined in terms of a variable,
:math:`q^{\mathbf{w}}_{\mathbf{i}}` and a symbol, :math:`s_j \in S^{z^\mathbf{w}}`. The transition
equations, :math:`f`, of every transition :math:`e^{\mathbf{w},j}_\mathbf{i}` is defined in equation
:eq:`equation-dm-transition-equation`. If the top level expression of a transition guard is a
disjunction, as in equation :eq:`equation-dm-transition-equation`, the :math:`K` operands of the
disjunction are called guard alternatives denoted :math:`h^{\mathbf{w},j,k}`. The transitions of the
automaton is the union in equation :eq:`equation-da-transitions`.

.. math::
   :label: equation-dm-transition-equation

   f(q) \triangleq \dot{q}^\mathbf{w}_\mathbf{i} = 1

.. math::
   :label: equation-dm-transition-guard

   g^{\mathbf{w},j}_\mathbf{i}(q) = \displaystyle \bigwedge_{k \in K} h^{\mathbf{w},j,k}_\mathbf{i}(q)

.. math::
   :label: equation-da-transitions

   E = \displaystyle \bigcup_{\mathbf{w} \in W} Q \times S^{\mathbf{z}^\mathbf{w}}
       
The number of transitions, :math:`|E|` for the simplest, non-trivial network, :math:`G_0` is
:math:`57`, which is tiny compared to the size of the discrete domain. This again, underlines the
rationale of :ref:`definition-sda`.

Discrete Control, Flow and Event Reset Guards
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The guards of both control and flow is determined by the control variables. As mentioned in
:ref:`Discrete Waterbody Variables`, control variables are reset in the first timestep. Let
:math:`\mathbf{w}` denote the superscript of a control variable:

.. math::

   g^{\mathbf{w},0}_\mathbf{i} \triangleq [ t = 0 ] \land [ q^\mathbf{w}_\mathbf{i} = 1 ]

Flow is flipped in reaction to a high flow control variable. Let :math:`\mathbf{w}_0` denote the
superscripts of a flow variable, and let :math:`\mathbf{w}_1` denote the corresponding superscript
of the flow control variable, then:

.. math::

   g^{\mathbf{w}_0,j}_{\mathbf{i}} \triangleq [ q^{\mathbf{w}_1,0}_{\mathbf{i}} = 1 ]

Events are reset in the first timestep.

.. math::

  h^{\mathbf{w},0}_\mathbf{i} \triangleq [ t = 0 ] \land [ q^\mathbf{w}_\mathbf{i} = 1 ]

Every event variable transition has a reset guard alternative in addition to the alternatives that
detect the event and clear out spurious detections. These will be treated in turn below.

Discrete Source Guards
^^^^^^^^^^^^^^^^^^^^^^

The tub source guard has two alternatives. The first one changes a false variable to true if the tub
leg is wet and the tub flow is positive. Recall that a positive tub flow means the tub is not
draining water from the pipe network. The second changes a true variable to false whenever the tub
leg is dry, or the tub flow is negative. The guards of the endpipe source variables are specified in
the same manner, with respect to the halfpipe leg and endpipe flow. However, the flow through the
endpipe is not necessarily sourcing if the flow is positive, as was the case with the tub. It
depends on which endpipe it is. The sourcing flow constant, :math:`l`, is defined in equation
:eq:`equation-dm-sourcing-flow` to compute the sourcing flow for any source.

.. math::
   :label: equation-dm-sourcing-flow

   l^\mathbf{w}_\mathbf{i} \triangleq \begin{cases} \mathbf{w} = (0,0,3) &\to 1 \\ \mathbf{w} = (1,2,1) \land \mathbf{i} = (i,j) &\to \neg j \end{cases} \\ 

Let :math:`\mathbf{w}_0`, :math:`\mathbf{w}_1`, :math:`\mathbf{w}_2` denote the superscripts of the
source, tub and flow variables respectively, with subscript :math:`\mathbf{i}`. Let the sourcing
flow be :math:`l = l^\mathbf{w}_\mathbf{i}`. The two alternatives of a source guard are then:

.. math::

   h^{\mathbf{w}_1}_\mathbf{i} &\triangleq [ q^{\mathbf{w}_0}_\mathbf{i} = 0 ] \land [ q^{\mathbf{w}_1}_\mathbf{i} = 1 ] \land [ q^{\mathbf{w}_2}_\mathbf{i} = l ] \\
   h^{\mathbf{w}_0,0,1}_\mathbf{i} &\triangleq [ q^{\mathbf{w}_0}_\mathbf{i} = 1 ] \land \neg ([ q^{\mathbf{w}_1}_\mathbf{i} = 1 ] \land [ q^{\mathbf{w}_2}_\mathbf{i} = l ])

Discrete Leg Guards
^^^^^^^^^^^^^^^^^^^

A tub leg is seeded and reaped via the tub leg control. This control would be set high or low by a
hybrid model when a waterbody external to the system connects or disconnects from the valve.

.. math::

   g^{0,0,0,j}_{\mathbf{i}} \triangleq [ q^{0,0,0}_{\mathbf{i}} = j ] \land [ q^{0,0,9}_{\mathbf{i}} = 1 ]

Halfpipe legs are wetted whenever some wet incident valve starts pushing water into it, or when the
opposite halfpipe has completely filled the pipe. A
halfpipe leg variable can be seeded either from its incident valve or through the midpipe. It is
reaped in reaction to its endpipe receding or when the halfpipe leg control is set high:

.. math::

   g^{1,1,0,0}_{i,j} &\triangleq [ t = 0 ] \land [ q^{1,1,0}_{i,j} = 0 ] \land ([ q^{1,2,5}_{i,j} = 1 ] \lor [ q^{1,0,1}_i = 1 ]) \\
   g^{1,1,0,1}_{i,j} &\triangleq [ t = 0 ] \land [ q^{1,1,0}_{i,j} = 1 ] \land ([ q^{1,0,3}_{i,j} = 1 ] \lor [ q^{0,0,9}_i = 1 ])

Discrete Joint Guards
^^^^^^^^^^^^^^^^^^^^^

Tub joints wet whenever the corresponding tub is either seeding or branching. They dry when the tub
is either cutting or receding.  Similarly the endpipe joint wets when the endpipe is either seeding
or branching, and dries when the endpipe is either cutting or receding:

.. math::

   h^{0,0,1,0}_{\mathbf{i}} &\triangleq [ t = 0 ] \land [ q^{0,0,1}_\mathbf{i} = 0] \land ([q^{0,0,4}_\mathbf{i} = 1] \lor [q^{0,0,5}_\mathbf{i} = 1]) \\
   h^{0,0,1,1}_{\mathbf{i}} &\triangleq [ t = 0 ] \land [ q^{0,0,1}_\mathbf{i} = 1] \land ([q^{0,0,6}_\mathbf{i} = 1] \lor [q^{0,0,7}_\mathbf{i} = 1]) \\
   h^{1,2,0,0,0}_\mathbf{i} &\triangleq [ t = 0 ] \land [ q^{1,2,0}_\mathbf{i} = 0 ] \land ([ q^{1,2,4}_\mathbf{i} = 1 ] \lor [ q^{1,2,5}_\mathbf{i} = 1 ]) \\
   h^{1,2,0,0,1}_\mathbf{i} &\triangleq [ t = 0 ] \land [ q^{1,2,0}_\mathbf{i} = 0 ] \land ([ q^{1,2,6}_\mathbf{i} = 1 ] \lor [ q^{1,2,7}_\mathbf{i} = 1 ])

The valve joint variable wets in response to a seeding event, and dries in response to a receding
event:

.. math::

   h^{0,0,0,0}_\mathbf{i} \triangleq [ t = 0 ] \land [ q^{0,0}_\mathbf{i} = 0 ] \land [ q^{0,2}_\mathbf{i} = 1 ] \\
   h^{0,0,0,1}_\mathbf{i} \triangleq [ t = 0 ] \land [ q^{0,0}_\mathbf{i} = 1 ] \land [ q^{0,4}_\mathbf{i} = 1 ]

The midpipe joint variable wets in reaction to a branching event and dries in reaction to a
recession event:

.. math::

   h^{1,0,0,0,0}_\mathbf{i} &\triangleq [ t = 0 ] \land [ q^{1,0,0}_\mathbf{i} = 0 ] \land [ q^{1,0,1}_\mathbf{i} = 1 ] \\
   h^{1,0,0,0,1}_\mathbf{i} &\triangleq [ t = 0 ] \land [ q^{1,0,0}_\mathbf{i} = 1 ] \land [ q^{1,0,3}_\mathbf{i} = 1 ]


Discrete Branching Event Guards
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A midpipe branching event is detected when for a dry midpipe joint when the joint control variable
is high. This control variable would be set high by a root of the :term:`SHA` when the volume of the
halfpipes fill the entire pipe:

.. math::

   h^{1,0,1,0,1}_\mathbf{i} \triangleq [ q^{1,0,0}_\mathbf{i} = 0 ] \land [ q^{1,0,4}_\mathbf{i} = 1 ] 

A tub branching event is detected when a wet tub with flow into the network branches into a dry tub
joint:

.. todo::

   This will be blocked by previous event!

.. math::

   h^{0,0,4,0,1}_{\mathbf{i}} \triangleq [ q^{0,0,4}_{\mathbf{i}} = 0 ] \land [ q^{0,0,0}_{\mathbf{i}} = 1 ] \land
     [ q^{0,0,2}_{\mathbf{i}} = 1] \land [ q^{0,0,1}_{\mathbf{i}} = 0 ]

An endpipe branching event is detected in a dry endpipe joint when the corresponding halfpipe is
part of a waterbody about to enter its incident valve. Let :math:`l = l^{1,2,0}_\mathbf{i}` denote
the sourcing flow from equation :eq:`equation-dm-sourcing-flow`, the guard alternative is then:

.. math::

   h^{1,2,4,0,1}_\mathbf{i} \triangleq [ q^{1,2,4}_\mathbf{i} = 0 ] \land [ q^{1,2,0}_\mathbf{i} = 0 ] \land [ q^{1,0,0}_\mathbf{i} = 1 ] \land [ q^{1,2,1}_\mathbf{i} = l ] 

Discrete Seeding Event Guards
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Seeding happens as a result of branching. When a waterbody branches into a dry valve it is seeded.
This might cause the dry tubs and endpipes incident to the valve to be seeded as well. The seeding
of a valve is defined in terms of a branching count, :math:`b`, in equation
:eq:`equation-dm-branching-count`. Seeding is detected in a dry valve with a positive branching
count, and the event is disabled if the branching count goes to zero:

.. math::

   h^{0,1,0,0}_\mathbf{i} &\triangleq [ q^{0,1}_\mathbf{i} = 0 ] \land [ q^{0,0}_\mathbf{i} = 0 ] \land [ b^{0,0}_\mathbf{i} > 1 ] \\
   h^{0,1,0,1}_\mathbf{i} &\triangleq [ q^{0,1}_\mathbf{i} = 1 ] \land [ b^{0,0}_\mathbf{i} = 0 ]

Let :math:`\mathbf{j}` denote the subscript of the incident valve of a tub or endpipe. Let :math:`l
= l^\mathbf{w}_\mathbf{i}` denote the sourcing flow from equation :eq:`equation-dm-sourcing-flow`.
The guard alternatives of seeding event detection for tubs and endpipes is then defined in equation
:eq:`equation-dm-seeding-detect`. In addition a seeding event in a tub is disabled if a branching
event is detected. This alternative is defined in equation :eq:`equation-dm-seeding-disable`.

.. math::
   :label: equation-dm-seeding-detect

   h^{0,0,5,0,1}_{\mathbf{i}} &\triangleq [ q^{0,0,5}_{\mathbf{i}} = 0 ]
     \land [ q^{0,0,1}_\mathbf{i} = 0 ] \land [ q^{0,0,2}_{\mathbf{i}} = \neg l ]
     \land [ q^{0,0,4}_\mathbf{i} = 0 ] \land [ q^{0,1}_\mathbf{j} = 1 ] \\
   h^{1,2,5,0,1}_\mathbf{i} &\triangleq [ q^{1,2,5}_\mathbf{i} = 0 ]
     \land [ q^{1,2,0}_\mathbf{i} = 0 ] \land [ q^{1,2,1}_{\mathbf{i}} = \neg l ]
     \land [ q^{1,1,1}_{\mathbf{i}} = 0 ] \land ([ q^{0,1}_{\mathbf{j}} = 1 ] \lor [ q^{0,0,1}_{\mathbf{j}} = 1 ])

.. math::
   :label: equation-dm-seeding-disable

   h^{0,0,5,0,2}_{\mathbf{i}} \triangleq [ q^{0,0,5}_\mathbf{i} = 1 ] \land [ q^{0,0,4}_\mathbf{i} = 1 ]

Discrete Collision Event Guards
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Valve collisions are defined in terms of a counter called the branching count. Let
:math:`B^\mathbf{w}_\mathbf{i}` denote the incident branching event variables of a valve variable
:math:`q^\mathbf{w}_\mathbf{i}`, where the incident components are defined in
:ref:`definition-incident-components`. The branching count of a valve variable is defined in
equation :eq:`equation-dm-branching-count`.

.. math::
   :label: equation-dm-branching-count

   b^\mathbf{w}_\mathbf{i} \triangleq \displaystyle \sum_{q \in B^{\mathbf{w}}_\mathbf{i}} [ q = 1 ]

A valve collision is detected when a wet valve has a positive branching count, or when the branching
count is greater than one. It is disabled when either a wet valve has zero branching count, or when
the collision count is less than one. This guard is a tracking guard, similar to the automaton in
:ref:`example-sda-counting`.

.. math::

   h^{0,2,0,1}_\mathbf{i} &\triangleq [ q^{0,2}_\mathbf{i} = 0 ] \land (([ q^{0,0}_\mathbf{i} = 1 ] \land [ b^{0,0}_\mathbf{i} > 1 ])
     \lor ([ q^{0,0}_\mathbf{i} = 0 ] \land [ b^{0,0}_\mathbf{i} > 2 ])) \\
   h^{0,2,0,1}_\mathbf{i} &\triangleq [ q^{0,2}_\mathbf{i} = 1 ] \land \neg (([ q^{0,0}_\mathbf{i} = 1 ] \land [ b^{0,0}_\mathbf{i} > 1 ])
     \lor ([ q^{0,0}_\mathbf{i} = 0 ] \land [ b^{0,0}_\mathbf{i} > 2 ]))

A midpipe collision happens if a branching has been detected, and there is a waterbody in each
halfpipe. This is the case when each halfpipe has either a wet halfpipe leg, or a seeding endpipe on
each side.

.. math::

   h^{1,0,2,0,1}_{\mathbf{i}} \triangleq [ q^{1,0,1}_{\mathbf{i}} = 1 ] \land ([ q^{1,1,0}_{\mathbf{i},0} = 1 ] \lor [ q^{1,2,5}_{\mathbf{i},0} = 1 ]) \land ([ q^{1,1,0}_{\mathbf{i},1} = 1 ] \lor [ q^{1,2,5}_{\mathbf{i},1} = 1 ])

Discrete Cutting Event Guards
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cutting events detect the severing of a waterbody. A tub or endpipe is cut whenever it is sourcing
its incident valve, and its leg control is set high. Let :math:`\mathbf{w}_0`, :math:`\mathbf{w}_1`,
:math:`\mathbf{w}_2` denote the cutting event, control and source variables respectively. Then both
:math:`h^{0,0,6,0,1}` and :math:`h^{1,2,7,0,1}_{\mathbf{i}}` are defined as:

.. math::

   h^{\mathbf{w}_0}_\mathbf{i} \triangleq [ t = 0 ] \land [ q^{\mathbf{w}_0} = 0 ] \land [ q^{\mathbf{w}_1}_\mathbf{i} = 1 ] \land [ q^{\mathbf{w}_2}_\mathbf{i} = 1 ]

Discrete Receding Event Guards
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tubs, valves, midpipes, and endpipes all recede when the waterbodies that inhabit them are about to
leave. Recession spreads from valves and midpipes. Receding events will be defined in terms of a
source count, :math:`s`. This is defined in a similar manner to the branching count in equation
:eq:`equation-dm-branching-count`. Let :math:`S^\mathbf{w}_\mathbf{i}` denote the incident source
event variables of a valve variable :math:`q^\mathbf{w}_\mathbf{i}`. The incident components are
defined as in :ref:`definition-incident-components`. The source count of a valve variable is then
defined in equation :eq:`equation-dm-source-count`.

.. math::
   :label: equation-dm-source-count

   s^\mathbf{w}_\mathbf{i} \triangleq \displaystyle \sum_{q \in S^{\mathbf{w}}_\mathbf{i}} [ q = 1 ]

Receding is detected in wet valves with zero source count:

.. math::

   h^{0,3,0,1}_\mathbf{i} \triangleq [ q^{0,3}_\mathbf{i} = 1 ] \land [ s^{0,0}_\mathbf{i} = 0 ]

A tub recedes when its incident valve is receding. This variable tracks the valve recession and if
the valve at some later point stops receding then the tub will no longer recede:

.. math::

   h^{0,0,7,0,1}_{\mathbf{i}_0} &\triangleq [ q^{0,0,7}_{\mathbf{i}_0} = 0 ] \land [
     q^{0,0,6}_{\mathbf{i}_0} = 0 ] \land [ q^{0,4}_{\mathbf{i}_1} = 1 ] \\
   h^{0,0,7,0,2}_{\mathbf{i}_0} &\triangleq [ t \neq 0 ] \land [ q^{0,0,7}_{\mathbf{i}_0} = 1 ] \land 
     \neg [ q^{0,4}_{\mathbf{i}_1} = 1 ]

A midpipe receding event is detected when any of its endpipes are receding. In this scenario the
pipe is about to go from fully to partially submerged; the valve supplying water to the pipe has
just run dry. This is a tracking event, and only done in eventual timesteps, based on events
detected in the current execution.

.. math::

   h^{1,0,3,0,1}_{\mathbf{i}} &\triangleq [ t \neq 0 ] \land [ q^{1,0,3} = 0 ] \land ([ q^{1,2,6}_{\mathbf{i},0} = 1 ] \lor [ q^{1,2,6}_{\mathbf{i},0} = 1 ]) \\
   h^{1,0,3,0,2}_{\mathbf{i}} &\triangleq [ t \neq 0 ] \land [ q^{1,0,3} = 1 ] \land \neg ([ q^{1,2,6}_{\mathbf{i},0} = 1 ] \lor [ q^{1,2,6}_{\mathbf{i},0} = 1 ])

The endpipe recede variable tracks the recession of its valve, but is disabled in the event of a
cutting:

.. math::

   h^{1,2,6,0,1}_{\mathbf{i}} &\triangleq [ q^{1,2,6}_{\mathbf{i}} = 0 ]
     \land [ q^{1,2,7}_{\mathbf{i}} = 0 ] \land [ q^{0,3}_{\mathbf{j}} = 1 ] \\
   h^{1,2,6,0,2}_{\mathbf{i}} &\triangleq [ t \neq 0 ] \land [ q^{1,2,6}_{\mathbf{i}} = 1 ]
     \land [ q^{0,3}_{\mathbf{j}} =  0 ]
   
Determinism Of The Discrete Model
---------------------------------

Recall from :ref:`results-sda-determinism` that the determinism of a :term:`SDA` was defined in
terms of the solvability of the active set of transitions, :math:`F^\top` for some
:math:`\upsilon`. The transitions of the model were defined in terms of variables and symbols in
equation :eq:`equation-da-transitions`. For a given variable :math:`q^\mathbf{w}_\mathbf{i}` there
were :math:`|S^{z^\mathbf{w}}|` transitions, one per domain symbol. Lets denote this set
:math:`E^{\mathbf{w}}_\mathbf{i}`. Each transition equation in this set is on the form :math:`f
\triangleq \dot{q}^\mathbf{w}_\mathbf{i} = 1`, and the transition guards in this set are mutually
exclusive. This means that if :math:`j_0,j_1 \in S^{z^\mathbf{w}}` for some variable
:math:`q^\mathbf{w}_\mathbf{i}`, then:

.. math::
   :label: equation-dm-transition-guard-mutex

   g^{\mathbf{w},j_0}_{\mathbf{i}}(\upsilon) \implies \neg g^{\mathbf{w},j_1}_{\mathbf{i}}(\upsilon)

As a consequence, :math:`\forall q \in \mathbb{Q}`, :math:`F \triangleq F^\top(\upsilon)` will be on
the form :math:`\dot{\bar{q}} = F(\upsilon)`, and :math:`\nexists k_0, k_1 \in |F^\top(q)|` such
that :math:`\bar{q}_{k_0}`, and :math:`\bar{q}_{k_1}` are the same variables. That would contradict
equation :eq:`equation-dm-transition-guard-mutex`. Therefore the automaton is globally
deterministic. Here, because the right hand side is a constant one, even if such :math:`k_0` and
:math:`k_1` existed, :math:`F` would still be uniquely solvable as one of them is a redundant
equation. However, the more interesting result is that an automaton constructed in this manner, is
globally deterministic for any right hand side :math:`f(\upsilon)`.

Zenolessness Of The Discrete Model
----------------------------------

Eventually it will be the case that :math:`\nexists (f,g) \in E` such that :math:`g(\upsilon) =
\top`; eventual :math:`F^\top(\upsilon) = \emptyset`, and the execution will stop. The zenolessness
of the model is showed by exhaustion. Every variable will eventually converge, and since every
variable converges the model is zenoless.

The guards in :ref:`Discrete Control, Flow and Event Reset Guards` are only potentially active in
:math:`t = 0`. In addition any guard reacting to a detected event is constrained to the first
timestep. These guards are termed event reacting guards. As discussed in :ref:`Discrete Control,
Flow and Event Reset Guards`, the detected events from the last discrete execution are active in the
first timestep.  execution.

The guards in :ref:`Discrete Leg Guards` are either conjoined to an active control variable, which
converges to zero after one step, or are event reacting. All the guards in :ref:`Discrete Joint
Guards`, :ref:`Discrete Cutting Event Guards` and :ref:`Discrete Cutting Event Guards` are also
event reacting.

The :ref:`Discrete Source Guards` are logic variables defined in terms of the leg and flow of a tub,
or halfpipe, and endpipe. Because the leg and flow will converge after the first timestep, the
source variables will also converge. The equation :eq:`equation-dm-source-count` will, thus, also
converge.

The guard of the midpipe branching event in :ref:`Discrete Branching Event Guards` is conjoined with
a control variable being high, and thus the midpipe branching event will converge in the first
timestep. Tub branching events conjoins a tub leg, tub flow, and valve joint, which all converge in
the first timestep. Thus tub branching events also converge. Similarly endpipe branching events
conjoins a halfpipe leg, endpipe flow, and valve joint which all converge in the first timestep. Thus
all branching events converge. This means that the branching count in equation
:eq:`equation-dm-branching-count` will also converge.

Finally the guards of :ref:`Discrete Seeding Event Guards` and :ref:`Discrete Collision Event
Guards` are all defined in terms of convergent branching counts and variables. The guards of
:ref:`Discrete Receding Event Guards` are all defined in terms of a convergent source count and
convergent variables. Thus all these guards and the variables whose transitions they guard also
converge. And since every variable converges, the model is zenoless.

Implementing The Discrete Model
-------------------------------

The discrete model was implemented in :cite:p:`RHelset2022`. Structured discrete automata were
constructed, and executions generated for a sample of initial value problems. The sequence of
discrete simulations mirror the expected discrete subexecutions that a hybrid execution of a
hypothetical :term:`SHA` would produce. The events detected in these sequences would be instrumented
by a :term:`SHA` with actions to ensure that the continuous state would satisfy the next continuous
subexecution. In the diagrams of these executions, :math:`q_{i,j}` will be used denote the
:math:`j`\ th state of the :math:`i`\ th discrete subexecution. Similarly for the discrete time
trajectories and :math:`t_{i,j}`.

The initial state of the first execution in each of these sequences, :math:`q_{0,0}` is known
a priori, and the initial state of the :math:`i`\ th execution, :math:`q_{i,0}`, is the final state
of the :math:`i - 1`\ th execution, :math:`q_{i-1,m}`, sometimes modified with certain control
variables set to one. These control variables are roots of the :term:`SHA` and would be computed by
the root subexecution.

The sequences are visualised with programatically drawn state diagrams using
:cite:p:`RHelset2022`. Control variables are not drawn.

.. _results-discrete_model-sftd:

Filling The Simple Network Top Down
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this sequence the simple system in :ref:`example-network-0` is filled from top down. The discrete
model starts dry and empty in :math:`q_{0,0}`, with the upstream tub leg control variable set high.
The executions eventually fill the pipe network from top to bottom, detecting branching and seeding
events in the process. The sequence of :math:`q_{0,0}`, :math:`q_{i,m}` is visualised in the figures
:numref:`figure-discrete-model-simulation-0` through :numref:`figure-discrete-model-simulation-5` in
:ref:`appendices-discrete_diagrams-sftd`.

.. _results-discrete_model-dftd:

Draining The Simple Network Top Down
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this sequence the simple system in :ref:`example-network-0` starts out filled, just like it ended
in :ref:`results-discrete_model-sftd`. The tub control of :math:`c^{0,0}_{0,0}` is set high,
indicating that the tub has just run dry. The sequence eventually drains the pipe network from top
to bottom, detecting cutting and receding events in the process. Continuous flow and volume
variables of the receding components would for example be set to zero. The sequence of
:math:`q_{0,0}`, :math:`q_{i,m}` is visualised in the figures
:numref:`figure-discrete-model-simulation-6` through :numref:`figure-discrete-model-simulation-10`
in :ref:`appendices-discrete_diagrams-dftd`.

.. raw:: latex

   \clearpage
