.. _results-flow_network:

=====================================
Logical Description of a Flow Network
=====================================

A flow network is here described logically in terms of a :term:`DAG` of valves and pipes (see
:cite:p:`Diestel2016` for an introduction to :term:`DAG`\ s and graph theory). This network can
contain multiple bodies of water. The valve can represent both gates and actual valves, and each
valve is potentially connected to an outside system. For a hydropower plant, gates would typically
be connected to a reservoir in a real system and valves internal to its tunnel system would have no
external connection. Openings between valves and pipes, and between valves and the outside system,
are assumed to be closeable. The elements of the graph are annotated with auxiliary logical
entities, called components. This serve as a frame of reference for the variables of the discrete
and continuous model. The flow network representation was implemented in :cite:p:`RHelset2022`, and
the diagrams below were programatically generated.

Flow Network Graph
------------------

The flow network is described by a :term:`DAG`, :math:`G \triangleq (V,E)`. Its vertices, :math:`V`,
and edges, :math:`E`, represent valves and pipes respectively. The directions of the graph are
denoted :math:`D \triangleq Z < 2`, where :math:`d_0` is in, and :math:`d_1` is out. The complement
of a direction is denoted :math:`d^\prime`. These definitions are made with hydropower producing
watercourses in mind. These watercourses admit a natural direction along the flow of water, and the
edges are assumed to point downstream.

.. target:: definition-flow-network-graph
   :label: Flow Network

**Definition 3.3.1 (Flow Network Graph)**:

  A flow network graph is a directed graph :math:`G \triangleq (C^0,C^1)`, where:

  - :math:`C^0`, :math:`C^1` are the valves and pipes of the network respectively.

.. target:: example-network-0
   :label: Example 3.3.1 (Basic Flow Network)

**Example 3.3.1 (Basic Flow Network)**

  The simplest possible, non-trivial system is the graph:

  .. math::

     G_0 \triangleq (\{c^0_0,c^0_1\},\{(c^0_0,c^0_1)\})
     
  A diagram of the system can be seen in :numref:`figure-results-system-0`.

.. figure:: system-figure-0.svg
   :align: center
   :width: 80%
   :name: figure-results-system-0

   Diagram of the system :math:`G_0` in :ref:`example-network-0` annotated with labels of valves and
   pipes. Edge directions are not drawn, as they all go from top to bottom in the diagrams of this
   report.

.. raw:: latex

   \clearpage

Flow Network Components
-----------------------

For the purposes of the subsequent modelling the elements of the network are annotated with
components. Each element is a component in its own right, and the valves and pipes are termed the
primary components of the network. The other components are termed auxiliary components. Each valve,
:math:`c^0_i` is associated with an outside source, called a tub, :math:`c^{0,0}_i`. Each pipe,
:math:`c^1_i`, is associated with a midpipe, :math:`c^{1,0}_i`, two halfpipes,
:math:`c^{1,1}_{i,j}`, and two endpipes, :math:`c^{1,2}_{i,j}`, where :math:`j \in D`. The halfpipe
and endpipe :math:`c^{0,k}_{i,0}` is physically located incident to the source of the corresponding
pipe, and :math:`c^{0,k}_{i,1}` is incident to its target. The union of valves, tubs, pipes,
midpipes, halfpipes, and endpipes are called the components of the system, and are tabulated in
:numref:`table-results-system-components`. The auxiliary components of :math:`G_0` are illustrated
in :numref:`figure-results-system-1`.

.. _table-results-system-components:

.. table:: Flow Network Components

   +--------+----------------------+------------------------------------+-------------------------------------+
   |Name    |Symbol                |Domain                              |Count                                |
   +========+======================+====================================+=====================================+
   |Valve   |:math:`c^{0}_i`       |:math:`Z^{0}_c = \mathbb{Z} < |V|`  |:math:`N^{0}_c = |V|`                |
   +--------+----------------------+------------------------------------+-------------------------------------+
   |Tub     |:math:`c^{0,0}_i`     |:math:`Z^{0,0}_c = Z^{0}_c`         |:math:`N^{0,0}_c = N^{0}_c`          |
   +--------+----------------------+------------------------------------+-------------------------------------+
   |Pipe    |:math:`c^{1}_i`       |:math:`Z^{1}_c = \mathbb{Z} < |E|`  |:math:`N^{1}_c = |E|`                |
   +--------+----------------------+------------------------------------+-------------------------------------+
   |Midpipe |:math:`c^{1,0}_{i}`   |:math:`Z^{1,0}_c = Z^1_c`           |:math:`N^{1,0}_c = N^{1}_c`          |
   +--------+----------------------+------------------------------------+-------------------------------------+
   |Halfpipe|:math:`c^{1,1}_{i,j}` |:math:`Z^{1,1}_c = Z^1_c \times D`  |:math:`N^{1,1}_c = 2 \cdot N^{1}_c`  |
   +--------+----------------------+------------------------------------+-------------------------------------+
   |Endpipe |:math:`c^{1,2}_{i,j}` |:math:`Z^{1,2}_c = Z^1_c \times D`  |:math:`N^{1,2}_c = 2 \cdot N^{1}_c`  |
   +--------+----------------------+------------------------------------+-------------------------------------+
   
.. figure:: system-figure-1.svg
   :align: center
   :width: 80%
   :name: figure-results-system-1

   Diagram of the tubs, midpipes, halfpipes, and endpipes of :math:`G_0`.

.. target:: definition-network-components
   :label: Definition 3.3.2 (Flow Network Components)

**Definition 3.3.2 (Flow Network Components)**:

  The components of a flow network, :math:`G \triangleq (C^0,C^1)` is a tuple :math:`C(G) \triangleq
  (C^0,C^1,C^{0,0},C^{1,0},C^{1,1},C^{1,2})`:

  - where :math:`C^{0,0} \triangleq \{ c^{0,0}_i \mid i \in Z < |C^0| \}`;
  - and :math:`C^{1,0} \triangleq \{ c^{1,0}_i \mid i \in Z < |C^1| \}`;
  - and :math:`C^{1,1} \triangleq \{ c^{1,1}_{i,j} \mid (i,j) \in Z < |C^1| \times D \}`;
  - and :math:`C^{1,2} \triangleq \{ c^{1,2}_{i,j} \mid (i,j) \in Z < |C^1| \times D \}`.

Flow Network Incidence
^^^^^^^^^^^^^^^^^^^^^^

During the definition of the discrete model, discrete expressions and equations will need to refer
to groups of variables associated with components that are topologically near each other. The
necessary incidence relations are preemptively defined below.

.. target:: definition-incident-valves
   :label: Definition 3.3.3 (Incident Valves)

**Definition 3.3.3 (Incident Valves)**:

  - The incident valves of a pipe component :math:`c^{\mathbf{k}}_0 \triangleq (c^0_0,c^0_1)` is the
    set :math:`\Pi^{\mathbf{k}}_0 \triangleq \{ c^0_0, c^0_1 \}`.
  - The directed incident valve of a pipe or midpipe :math:`c^{\mathbf{k}}_0 \triangleq
    (c^0_0,c^0_1)` is denoted :math:`\pi^{\mathbf{k}}_{d \mid 1}(c^{\mathbf{k}}_0) \triangleq
    c^0_d`, where :math:`d \in D`.
  - The incident (singular) valve of an halfpipe or endpipe, :math:`c^{1,k}_{i,j}` is :math:`\pi^{1,k}_{j \mid 0}(c^{1,k}_i)`.

.. target:: definition-incident-pipes
   :label: Definition 3.3.4 (Incident Pipes)

**Definition 3.3.4 (Incident Pipes)**:

  - The directed incident pipe or midpipe of a valve :math:`c^0_i` is defined as :math:`\pi^0_{d
    \mid \mathbf{k}}(c^0_i) \triangleq \{c^{\mathbf{k}}_{j} \in C^{\mathbf{k}} \mid
    \pi^{\mathbf{k}}_{d^\prime \mid 0}(c^{\mathbf{k}}_j) \triangleq c^0_i \}`, where :math:`d \in D`.
  - The directed incident halfpipes or endpipes of a valve :math:`c^0_i` is defined as :math:`\pi^0_{d
    \mid \mathbf{k}}(c^0_i) \triangleq \{c^{\mathbf{k}}_{j,d^\prime} \in C^{\mathbf{k}} \mid
    \pi^{\mathbf{k}}_{d^\prime \mid 0}(c^{\mathbf{k}}_{j,d^\prime}) \triangleq c^0_i \}`, where :math:`d \in D`.
  - The incident pipes, midpipes, halfpipes or endpipes of valve is the union
    :math:`\Pi^0_{\mathbf{k}}(c^0_i) \triangleq \displaystyle \bigcup_{d \in D} \pi^0_{d \mid
    \mathbf{k}}(c^0_i)`.

**Example 3.3.2 (Incident Pipes)**
  
   For the system, :math:`G_1 \triangleq
   (\{c^{0}_1,c^{0}_1,c^{0}_2,c^{0}_3\},\{(c^{0}_0,c^{0}_1),(c^{0}_1,c^0_2),(c^{0}_1,c^{0}_3)\})`,
   shown in :numref:`figure-results-system-2`, the incident endpipes of :math:`c^{0}_1` are
   :math:`\{ c^{1,2}_{0,1},c^{1,2}_{1,0},c^{1,2}_{1,0} \}`, or with indices instead of subscripts:
   :math:`\{ c^\mathbf{5}_{\mathbf{1}},c^\mathbf{5}_{\mathbf{2}},c^\mathbf{5}_{\mathbf{4}} \}`

.. target:: definition-incident-tubs
   :label: Definition 3.3.5 (Incident Tubs)

**Definition 3.3.4 (Incident Tubs)**:

  The incident tubs of a valve :math:`c^0_i` is :math:`\pi^0_{0,0}(c^0_i) \triangleq c^{0,0}_i`.
  Conversely the incident valve of a tub :math:`c^{0,0}_i` is :math:`\pi^{0,0}_{0}(c^{0,0}_i) \triangleq c^0_i`.

.. target:: definition-incident-components
   :label: Definition 3.3.5 (Incident Components)

**Definition 3.3.5 (Incident Components)**:

  The incident components of a valve :math:`c^0_i` is :math:`\Pi^0(c^0_i) \triangleq \{ \pi^0_{0,0}(c^0_i) \} \bigcup \Pi^{0}_{1,2}(c^0_i)`.

**Example 3.3.3 (Incident Components)**
  
  For the system, :math:`G_0`, shown in :numref:`figure-results-system-1`, the incident components of
  :math:`c^0_1` is the set :math:`\{ c^{0,0}_1 \} \bigcup \{ c^{1,2}_{0,1} \}`.

.. figure:: system-figure-2.svg
   :align: center
   :width: 80%
   :name: figure-results-system-2

   Diagram of the system defined by :math:`G_1`, with labeled pipes and valves.

.. raw:: latex

   \clearpage

..  LocalWords:  RHelset
