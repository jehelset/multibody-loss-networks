from dataclasses import dataclass,astuple
from itertools import chain
import pathlib
import cairo
from hysj import mathematics,simulators,automatons
from hysj.models.multibody_loss_networks import *
from hysj.models.multibody_loss_networks import layouts,diagrams,latex

def figure_path(name):
  try:
    return (pathlib.Path(__file__).parent.absolute() / name).with_suffix('.svg')
  except NameError:
    return (pathlib.Path('sources/results') / name).with_suffix('.svg')
width = 1080
height = 1080

def render(name,diagram):
  with cairo.SVGSurface(figure_path(f'discrete-model-simulation-{name}-figure.svg'),width,height) as surface:
    context = cairo.Context(surface)
    diagrams.paint_diagram(context = context,
                           diagram = diagram,
                           width = width,
                           height = height)

dautomatons = automatons.discrete  
dsolvers    = solvers.discrete  
dsimulators = simulators.discrete

program = mathematics.Program()
time    = program.next_symbol()

@dataclass
class Fixture:
  name: str
  subfixture: int
  network: system.Network
  model: discrete.Model
  automaton: dautomatons.BasicAutomaton
  initial_state: dsolvers.BasicState
  layout: layouts.Layout

def render_trajectory(fixture: Fixture):
  name,subfixture,network,model,automaton,initial_state,layout = astuple(fixture)

  problem = dsimulators.Problem(
    program   = program,
    automaton = automaton,
    state     = initial_state,
    solver    = dsolvers.BasicSolver())

  solution = dsimulators.solve(problem,
                               control = lambda x,e:dsimulators.Event(view = e))

  events = []
  try: 
    while solution:
      event = solution()
      if event.tag in [dsimulators.step,dsimulators.stop]:
        events.append(event)
      if event.tag == dsimulators.stop:
        break
  except Exception as e:
    import traceback
    print(traceback.format_exc())

  rendered_events = [] 
  if subfixture in [0,5,8]:
    rendered_events = [ events[0], events[-1] ]
  else:
    rendered_events = [ events[-1] ]
    
  for index,event in enumerate(rendered_events):
    render(f'{name}-{subfixture}-{index}',
           diagrams.discrete_state_diagram(network   = network,
                                           model     = model,
                                           state     = event.state,
                                           layout    = layout))
  return events

fixture0_initial_valuations = [
  [(discrete.tub_leg_control,0,discrete.ControlValue.on)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.tub_branching,0,discrete.EventValue.detected),
   (discrete.valve_seeding,0,discrete.EventValue.detected),
   (discrete.endpipe_seeding,0,discrete.EventValue.detected)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint_control,0,discrete.ControlValue.on)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_branching,0,discrete.EventValue.detected)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_source,1,discrete.LogicValue.top),
   (discrete.endpipe_branching,1,discrete.EventValue.detected),
   (discrete.valve_seeding,1,discrete.EventValue.detected),
   (discrete.tub_seeding,1,discrete.EventValue.detected)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_branching,0,discrete.EventValue.detected),
   (discrete.tub_leg_control,1,discrete.ControlValue.on)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.VolumeValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint_control,0,discrete.ControlValue.on),
   (discrete.tub_leg_control,1,discrete.ControlValue.on)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint_control,0,discrete.ControlValue.on),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_flow,1,discrete.FlowValue.minus),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.tub_leg,0,discrete.VolumeValue.wet),
   (discrete.tub_leg_control,0,discrete.ControlValue.on),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_source,1,discrete.LogicValue.top),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.minus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.tub_cutting,0,discrete.EventValue.detected),
   (discrete.tub_joint,0,discrete.JointValue.wet),
   (discrete.tub_source,0,discrete.LogicValue.top),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.valve_receding,0,discrete.EventValue.detected),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_receding,0,discrete.EventValue.detected),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint,0,discrete.JointValue.wet),
   (discrete.midpipe_receding,0,discrete.EventValue.detected),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_source,1,discrete.LogicValue.top),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.minus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.halfpipe_leg_control,1,discrete.ControlValue.on),
   (discrete.endpipe_source,1,discrete.LogicValue.top),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.minus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.endpipe_cutting,1,discrete.EventValue.detected),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.valve_receding,1,discrete.EventValue.detected),
   (discrete.tub_flow,1,discrete.FlowValue.minus),
   (discrete.tub_receding,1,discrete.EventValue.detected),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.tub_flow,1,discrete.FlowValue.minus),
   (discrete.tub_flow_control,1,discrete.ControlValue.on),
   (discrete.tub_leg,1,discrete.VolumeValue.wet)],
  [(discrete.valve_seeding,1,discrete.EventValue.detected),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_leg,1,discrete.VolumeValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top),
   (discrete.tub_branching,1,discrete.EventValue.detected)],
  [(discrete.endpipe_flow_control,1,discrete.ControlValue.on),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top)],
  [(discrete.endpipe_flow,1,discrete.FlowValue.minus),
   (discrete.endpipe_seeding,1,discrete.EventValue.detected),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top)],
  [(discrete.midpipe_joint_control,0,discrete.ControlValue.on),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.endpipe_flow,1,discrete.FlowValue.minus),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top)],
  [(discrete.endpipe_flow_control,0,discrete.ControlValue.on),
   (discrete.midpipe_branching,0,discrete.EventValue.detected),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.endpipe_flow,1,discrete.FlowValue.minus),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top)],
  [(discrete.tub_flow_control,0,discrete.ControlValue.on),
   (discrete.valve_seeding,0,discrete.EventValue.detected),
   (discrete.endpipe_branching,0,discrete.EventValue.detected),
   (discrete.endpipe_source,1,discrete.LogicValue.top),
   (discrete.endpipe_flow,0,discrete.FlowValue.minus),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.endpipe_flow,1,discrete.FlowValue.minus),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top)],
  [(discrete.tub_seeding,0,discrete.EventValue.detected),
   (discrete.tub_flow,0,discrete.FlowValue.minus),
   (discrete.valve_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_source,0,discrete.LogicValue.top),
   (discrete.endpipe_joint,0,discrete.JointValue.wet),
   (discrete.endpipe_flow,0,discrete.FlowValue.minus),
   (discrete.halfpipe_leg,0,discrete.VolumeValue.wet),
   (discrete.midpipe_joint,0,discrete.JointValue.wet),
   (discrete.halfpipe_leg,1,discrete.VolumeValue.wet),
   (discrete.endpipe_joint,1,discrete.JointValue.wet),
   (discrete.endpipe_flow,1,discrete.FlowValue.minus),
   (discrete.valve_joint,1,discrete.JointValue.wet),
   (discrete.tub_flow,1,discrete.FlowValue.plus),
   (discrete.tub_joint,1,discrete.JointValue.wet),
   (discrete.tub_leg,1,discrete.VolumeValue.wet),
   (discrete.tub_source,1,discrete.LogicValue.top)]
]

def fixture0(subfixture: int):
  network = system.Network()
  valves = network(2)
  network(valves[0],valves[1])
  components = network.components()
  model     = discrete.Model(program,network,time)
  automaton = model.automaton()
  layout    = layouts.Layout(network = network)
  initial_state = dsolvers.BasicState.origo(automaton)
  for tag,index,value in fixture0_initial_valuations[subfixture]:
    initial_state[0,model.to_variable_index((tag,index))] = value
  
  return Fixture(name = f'0',
                 subfixture = subfixture,
                 network = network,
                 model = model,
                 automaton = automaton,
                 initial_state = initial_state,
                 layout = layout)

fixtures = sum([list(fixture0(i) for i in range(len(fixture0_initial_valuations)))],[])

traces = []
for fixture in fixtures:
  traces.append(list(render_trajectory(fixture)))
