.. raw:: latex

   \appendix

.. toctree::
   :hidden:
   :glob:

   appendices/notation.rst
   appendices/discrete-model-diagrams.rst
   appendices/external-content.rst
