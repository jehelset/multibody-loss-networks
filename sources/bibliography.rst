.. only:: html

   ============
   Bibliography
   ============

.. bibliography::
   :all:
