**********
Background
**********

The background is divided into three parts. The first part, :ref:`background-related_modelling`,
reviews existing models that are interesting, similar, or relevant to the model worked towards in
:ref:`Results`. The second part, :ref:`Differential Algebraic Equations`, introduces the framework
that is used to represent continuous dynamics of a :term:`HA` in :ref:`Hybrid System Theory`, as
well as in :ref:`Results`. The third part, :ref:`Hybrid System Theory`, provides an overview of some
hybrid system theoretical frameworks, and in particular the hybrid automaton, which will be used in
:ref:`Results`. The simulation of the :term:`HA` defined in :ref:`ha-example` was done with using
the hybrid system theoretical framework defined and implemented in :ref:`results-sha`.

.. toctree::
   :hidden:
   :maxdepth: 1

   background/related-modelling.rst
   background/differential-algebraic-equations.rst
   background/hybrid-system-theory.rst


