*******
Results
*******

The results are divided in two parts. The first part is about modelling frameworks and consists of
:ref:`results-sda` and :ref:`results-sha`. Here the :term:`HA` framework is decomposed and refined
into what is called a structured discrete automaton (:term:`SDA`) and a structured hybrid automaton
(:term:`SHA`). This separates the discrete dynamics from the hybrid dynamics of the :term:`HA`, so
that one can construct and reason about the discrete aspects of the automaton in isolation. These
re-definitions more directly lend themselves to practical computations for automata with a high
level of discrete detail. The second part is about modelling and consists of
:ref:`results-flow_network` and :ref:`results-flow_network_da`. It gives a logical description of a
tunnel-network in terms of a :term:`DAG`, and sets out a discrete model in the form of a structured
discrete automaton, which could form the basis of a :term:`SHA` model of the tunnel systems of a
watercourse.

.. toctree::
   :hidden:
   :maxdepth: 1

   results/structured-discrete-automata.rst
   results/structured-hybrid-automata.rst
   results/system.rst
   results/discrete-model.rst

..  LocalWords:  toctree maxdepth
