**********
Discussion
**********

The overarching question of the report, as set out in :ref:`Goals`, was the organisation of
computations and in particular the construction of a suite of interrelated computational models
supporting a technical labour process. It was assumed that construction of computational models
through left-to-right transformations of system models was an efficient way to do this. Practical
computation was highlighted as a useful system modelling tool. The framing of the report was one of
hydropower production planning, and the goal was to construct a system model of a watercourse. The
:term:`DAE` and :term:`HA` were identified as suitable system modelling frameworks for this
construction. The :term:`HA` enables the combination of the :term:`DAE` flow networks in
:cite:p:`Jansen2014` with the hybrid system theoretical approach to the modelling of temporary flow
paths of :cite:p:`Ocampo2010`. The former meets the challenges of topological complexity and the
latter meets the challenges of complex waterbody distributions and transient patterns of flow. These
two challenges were assumed to be main obstacles to the direct implementation of a satisfactory
computational model for simulation, and the cause of ad-hoc solutions in computational software.  It
was assumed that direct implementation was preferable to ad-hoc solutions.

============================================
Modelling Framework and Modelling Experience
============================================

This report is a snapshot of a work in progress - the construction of a new hybrid system
theoretical model called a multibody flow network. This model pushes the envelope of hybrid automata
in terms of discrete detail. The tooling required to support such a complex construction, the
tooling that enables practical computation with hybrid automata, is an area of active research
(:cite:p:`Zimmer2010`, :cite:p:`Kunkel2018`, :cite:p:`Chupin2021`). During the construction of the
discrete part of this model there was a definite need for practical computation to debug, verify,
and test designs - and the lack of a directly representable model made it difficult. The modelling
frameworks set out in :ref:`results-sda` and :ref:`results-sha` was a response to that problem. The
observation in :cite:p:`Campbell1995` will be repeated for a second time:

  | Being able to do engineering design and computer simulation directly on these original equations would lead to faster simulation and design, permit easier model variation, and allow for more complex models.

The discrete and hybrid structured automata were designed with an eye on direct computation in the
face of highly detailed discrete dynamics. The separation of the hybrid automaton into a discrete
and hybrid part enabled the definition and analysis of the discrete aspects of the model to be done
separately from the hybrid construction. Defining the transition guards as functions of discrete
time made it easier to construct a model whose zenoness could be reasoned about. The execution of a
:term:`SHA` formalises the event detecting approach described in :ref:`background-hst-simulation`
and enables a direct implementation of its construction. Future work in this direction is outlined
in :ref:`Iterating on Structured Automata`.

The discrete multibody flow network defined in :ref:`results-flow_network_da` is the discrete part
of a hybrid multibody flow network model. It can represent arbitrary distributions of waterbodies
and patterns of flow in a network of valves and pipes and is a basis for the continuous dynamics of
a :term:`SHA`. It can detect the discrete events a :term:`SHA` would need to handle the changing
continuous algebraic constraints of a multibody flow network. In this respect, it is a sufficient
basis for the construction of the :term:`SHA`. Executions of the discrete model were successfully
constructed and illustrated using an implementation of the algorithm in
:ref:`figure-simulation-sda`. The discrete model was shown to be deterministic and zenofree, which
are useful results in the context of practical computation. However, it does fall short of being a
satisfactory basis for the construction of a system model of a tunnel network in the context
hydropower production planning. It thus falls even shorter of being a system model for a hydropower
producing watercourse. The latter was set out as an explicit goal in :ref:`introduction-goals`, and
has not been met. There is still some way to go before such a model can be constructed. This will be
addressed in :ref:`Iterating the Discrete Model`, :ref:`Constructing the Hybrid Model`,
:ref:`Constructing a Watercourse Model`, and :ref:`Controlling a Watercourse Model`.
 
=========================
Implementation Experience
=========================

The implementation of these frameworks were, like :cite:p:`Zimmer2010`, written in C++. In this
report a library-based approach was taken. This sidesteps the issue of designing a modelling
language, which is a common approach when it comes to the design of computational software for
hybrid systems. Both :cite:p:`Zimmer2010` and :cite:p:`Chupin2021` take a language-based approach
and discuss previous language-based solutions. A dedicated modelling language has the potential for
ergonomic and elegant model definitions in a way that is hard to replicate, even with a
well-designed library :term:`API`. However, in an operative context, like the one assumed in this
report (see :numref:`figure-introduction-metamodel-pp`), no computation or model transformation can
assume to have a monopoly on the representation of a system model. Its representation needs to be
accessible for a plethora of different computations and applications. A textual representation, in
the form of a modelling language designed for humans, is not a practical data format in this
setting. General purpose data formats designed for programmatic consumption, supported across a
variety of multi-purpose languages, is likely to be used instead.

A library for an existing multi-purpose programming language, alternatively combined with bindings
in an existing scripting language, lets the modeller take advantage of an already existing ecosystem
of tooling and packages. A language and ecosystem that the modeller is, likely, already somewhat
comfortable with. This makes practical computation more ergonomic and flexible without the need for
intermediate formats to define models and store computational results. There is no contradiction
between programmatic and language-based construction, but the benefits of the latter do not seem to
be worth the effort in this setting, and here, a library-based approach is more pragmatic.

Structured hybrid automata were defined in terms of structured discrete automata. As a result the
implementation of the latter and the construction of its execution could be directly reused by the
implementation of the former. The algorithms for constructing executions were implemented as
event-based coroutines. The construction of the continuous executions were implemented using the
root finding :term:`DAE`-solver IDA from :cite:p:`Hindmarsh2005`. The library was fitted with Python
bindings. The events of a construction are coroutine suspension points, the events are derived
directly from the defintions of the executions. This in turn means that the constructions are
suspendable. This enable fast iteration as a construction can be suspended according to
experiment-specific logic.

Python has a rich ecosystem of data analysis and visualisation tools which can be used directly
during the course of such a construction. A modeller is able to define an event continuation in
Python that can suspend the construction and / or inspect its current state. The state of the
construction includes access to a symbolic representation of the constructions components. This is a
consequence of the modelling framework being directly implemented. The computational software itself
needs a direct symbolic representation of these components to construct the execution. The discrete
diagrams seen in :ref:`results-flow_network_da` and :ref:`appendices-discrete_diagrams` were
programmatically generated in this manner by combining the logical description of the system, with
the state of the construction, and rendering these with Python packages :cite:p:`Cairo` and
:cite:p:`Graphviz`. The visualisation of complex discrete states in diagram form was of great help
during the construction of the discrete model. The simulations, and associated diagrams ,of
:ref:`ha-example` were also generated in a similar manner using the Python package
:cite:p:`Hunter2007`. Another useful tool was the rendering of the currently active set of
transitions, activities, actions, and roots in LaTeX for debugging purposes during the course of an
execution.

Coroutines is a new addition to C++ (see :cite:p:`Belson2021` for a recent application) and can not
be considered to be a production-ready feature yet. The implementation turned out more difficult
than it had to be, as the compiler kept running into internal compiler errors. Nevertheless, being
able to suspend the construction of an execution is a useful quality in the context of practical
computation. Further work in this direction is described in :ref:`Iterating on Structured Automata`.

===========
Future work
===========

Iterating on Structured Automata
--------------------------------

This is the initial iteration of structured automata. There are no doubt errors, both in definitions
and implementation, which need to be worked out. In addition this report left out the definition of
determinism and zenoness of the :term:`SHA`, which are important qualities in the context of
practical computation and system modelling. Even though a :term:`SDA` is zenoless, the hybrid
execution can still get stuck in a sequence of discrete executions. This potential zenoness is
inherent to the :ref:`definition-sha-execution`, and made obvious in :numref:`figure-sha-execution`.

In :ref:`results-sda` the case of an overdetermined set of transition equations was set aside.
:ref:`definition-sda-execution` was constructed in terms of solvable transition equations. An
overdetermined set of transition equations has an obvious interpretation; that of nondeterminism. A
more powerful definition of the execution of a :term:`SDA` would also include executions that
bifurcate as a result of overdetermination. This would be needed be able to represent :math:`x \in
D(q) \cap G(e)` as :math:`x \in G(e_0) \cap G(e_1)`. The convergence of a discrete execution was
defined with respect to :math:`\dot{q}` instead of :math:`G^\top`, which is less ergonomic in the
case one wants to construct a non-blocking automaton, as it requires a slightly more complex
discrete dynamics than one defined in terms of :math:`G^\top`.

In :ref:`results-sha` the actions were defined in terms of a variable, function, and guard. It was
assumed that no two actions could be active at the same time, for the same variable. A more powerful
definition would use an implicit equation instead of a variable and function. This would be similar
to the definitions of the continuous dynamics of the :term:`SHA` or discrete dynamics of the
:term:`SDA`. This creates potential for ambiguity and bifurcation, but it is a necessary
generalization to efficiently represent the actions required by a :term:`SHA` of a multibody flow
network. In particular it would be required when handling collisions between inelastic
waterbodies. These collisions require solving affine and acausal systems of equations, constructed
based on the current discrete state. It was also assumed that no two roots referred to the same
discrete variable. This is an unnecessary simplification, but a more powerful definition will again
create potential for ambiguity and bifurcation.

The discrete multibody flow network model was manually shown to be zenofree and deterministic by
considering the convergence of guards, variables and counting variables. This is a tedious and
fragile approach which should be improved. The manual approach suggests an algorithmic solution for
:term:`SDA`\ s like the discrete multibody flow network. In this case determinism could be
sufficiently checked, for example, by checking mutual exclusivity of the transition guards of a
variable, combined with uniqueness of solution of the corresponding transition equations.

Structured automata are designed for systems modelling, and only need to enable practical
computation as a modelling tool. However, the computations of production planning
(:ref:`introduction-computations_of_pp`) are done in an operative setting with highly varied
hardware, and a high degree of regularity. Computational input varies only slightly in two
consecutive computations. This is an interesting context for investigating the efficient
computational architecture of hybrid systems. A :term:`JIT`-compilation based approach, as
demonstrated in :cite:p:`Chupin2021`, can simplify the software distribution model, and at the same
time generate efficient executable code in the face of hardware variation. The high degree of
regularity of computations provides interesting possibilities with regards to memoisation of
causalised & compiled continuous dynamics in commonly inhabited discrete states of hybrid
executions. This would come at the cost of the :term:`JIT`-compilation itself, as well as the added
complexity of distributing a :term:`JIT`-compilation framework.

The construction of the executions of a nondeterministic :term:`SDA` or :term:`SHA` will need to
handle bifurcation points. The initial implementation took the liberty of assuming that the automata
were deterministic. :ref:`background-hst-zenoness` quoted Schaft discussing the handling of such a
bifurcation :cite:p:`Schaft2000`:

  | In such situations the simulation software should provide a warning to the user, and if it is difficult to make a definitive choice between several possibilities perhaps the solver should even work out all reasonable options in parallel.

Implementation approaches to the construction of nondeterministic executions of structured automata
is a very interesting problem, particularly in the light of the implementation strategy in this
report. The frame of a C++ coroutine does not, currently, have value-semantics. It is an opaque
object, and there is no simple way to copy the entire state of a coroutine if the members of the
frame are not trivial. However, if coroutine frames had value-semantics, such that a coroutine frame
was copyable if all its members were copyable, it would become trivial to implement
bifurcations. The current coroutine-frame of an execution, suspended at a bifurcation event could be
copied, and each copy instructed to follow a different execution.

Iterating the Discrete Model
----------------------------

The discrete model in :ref:`results-flow_network_da` has a shortcoming as it does not model valve
openings. The modelling of valve openings is crucial for an accurate representation of a
watercourse, and a necessary addition before constructing a watercourse system model that meets the
demands of :ref:`introduction-goals`. The upside is that the logical description of the flow network
system already can facilitate the opening and closing of valves on an per-incident component basis
(:ref:`definition-incident-components`) by adding suitable discrete endpipe and tub variables. In
addition it would greatly increase the expressive power of the model.

Nor is this iteration of the discrete model likely to be a minimal expression of its discrete
dynamics, and might have both redundancies and errors. The sequencing of transitions were done in a
coarse manner, and the guards of the transitions were designed to be mutually exclusive so that it
would become easier to reason about determinism and zenoness. Perhaps there is potential for
relaxation and improvement. One such example would be to make the tracking variables function as
PD-regulators instead of P-regulators. This would ensure faster convergence of the discrete
state. On the other hand it would also cause a :math:`F^\top(\upsilon)` that was linear in
:math:`\dot{q}` to have a more complex structure, and impose sequencing on its computation.

Nor is the proposed discrete model able to support elastic waterbodies. Consider again the discrete
sequence of :numref:`figure-results-discrete-state-0` and :numref:`figure-results-discrete-state-1`.
Here waterbodies were assumed to be inelastic, and so the endpipe flow variables of the pipe would
be aligned during the course of collision resolution. In the case of an elastic waterbody, the flow
directions would remain the same until the pressure of the water column in the pipe eventually flips
one or both of the continuous flows. Now imagine the scenario where, at the point of collision, both
valves recede. The waterbody in the pipe would float midair, as it were. To model this state
discretely, one might need to add a midpipe volume or droplet variable. This is not required for
being an adequate model for the computations of hydropower production planning, where only the
aggregate movement of water, on a scale much larger than the volume of a pipe, is of interest.

Finally, there is an issue with discrete "droplets". Small waterbodies might become stuck in the
pipe network. A waterbody can only move through a pipe if it is big enough to fill the entire pipe,
even though it can always retreat back into the valve. In the case where gravity is pulling water
into the pipe network from a tub, and the tub runs dry, or the valve is shut before the emergent
waterbody is large enough to fill a pipe, it will simply get stuck. It will stay there until the tub
starts feeding it water again, or it is merged with another waterbody from below. The scale of
droplets is also too small to cause much concern in this context.

Constructing the Hybrid Model
-----------------------------
  
The discrete multibody flow network is the point of departure for a hybrid multibody flow
network. The addition of opening and closing of valves will require the consideration of over- and
underdetermined continuous dynamics. Consider for example a pipe that is fully submerged but closed
at both ends. A construction of the continuous dynamics of a :term:`SHA`, based only on local
information, might add two algebraic constraints - one for each end of the pipe. For an inelastic
body of water, where the flow into a submerged pipe is equal to the flow out of a submerged pipe,
this will lead to an overdetermined system of equations with redundant algebraic
constraints. Similarly, with no connected valves, the equations governing the pressure in model with
quasi-stationary continuous dynamics will be underdetermined. This underdetermination is not just a
transient consequence of symbolic processing, as in :cite:p:`Zimmer2013`, it is the persistent
continuous dynamics of the system which last until one of the valves are opened. Elastic waterbodies
would have consequences on the discrete model as mentioned in :ref:`Iterating the Discrete Model`.

The computational software used to solve the consistent initialisation problem and to construct the
continuous execution in this report is based on regular :term:`DAE`\ s, and use a :term:`DAE` index
that is not suitable for over- and underdetermined systems, :cite:p:`Kunkel2006`:

  | Although the concept of the differentiation index is widely used, it has a major drawback, since it is not suited for over- and underdetermined systems. The reason for this is that it is based on a solvability concept that requires unique solvability.

The unique and global solvability result of :cite:p:`Jansen2015` requires at least one demand and
pressure node connected to the network. The problem of over- and underdetermination of the
:term:`DAE`\ s, generated during the construction of an execution of a hybrid automaton, will likely
become a point of focus for the construction of the hybrid model.

Another problem that needs to be dealt with is that of continuous actions. The discrete model
detects branching, collisions, separations, and recessions. Recessions and separations are easy
enough to deal with; they simply require setting the corresponding continuous flow variables to
zero. Collisions and branching require more careful consideration. In particular, the collision of
two inelastic waterbodies requires solving an affine system of algebraic equations of flow.  This
system of equations must be constructed based on the discrete state at the point of collision. The
challenges of event detection and zero-crossing, described in :cite:p:`Zhang2008`, poses yet another
problem; is it even feasible to simulate a model with such a high level of discrete detail?
 
Constructing a Watercourse Model
--------------------------------

A :term:`SHA` of a multibody flow network is one of many system models that make up a
watercourse. Constructing a system model of an entire hydropower producing watercourse was the goal
set out in :ref:`introduction-goals`. Models of reservoirs, catchments, river systems, and power
production remains to be constructed. These systems, in the context of production planning, are not
required to be modelled at a high level of discrete detail, and are trivial constructions in
comparison to the one in :ref:`results-flow_network_da`. With a :term:`SHA` of a watercourse in
hand, the watercourse models, of the associated computational models, could then be constructed
through suitable left-to-right transformations.

Controlling a Watercourse Model
-------------------------------

Optimal control is a cornerstone computation in the context of production planning. The question of
control is even relevant for plan simulations. It is not always the case that the production
planning computation of :ref:`introduction-computations_of_pp` computes plans detailed enough for
simulation. The plan might be computed for a lumped group of units, whereas the simulation treats
each unit separately, which requires the plan simulation itself to deal with the problem of optimal
control.

This might lead to ad-hoc solutions in simulation software, which faces the problems described in
:ref:`introduction-tunnel_systems`. Some producers do not do production planning per se, and rely
only on this limited planning functionality in the plan simulation; maiming two very different
animals with one stone. :cite:p:`Ocampo2010` formulates an :term:`MPC` problem based on a hybrid
model for wastewater systems. Treating the limited planning that the plan simulation does as a
wastewater system, and simply limiting the flood, while softly satisfying production plans might be
a good starting point for constructing a formal model of this limited form of planning. An
:term:`MLD` similar to the one used in :cite:p:`Ocampo2010` could be constructed by applying
left-to-right model transformations of a watercourse :term:`SHA`.
