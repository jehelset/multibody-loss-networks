#######################
Multibody Flow Networks
#######################

.. toctree::
   :hidden:
   :numbered:
   :maxdepth: 2

   introduction.rst
   background.rst
   results.rst
   discussion.rst
   conclusion.rst

   appendices.rst
   glossary.rst
   bibliography.rst

.. todolist:: 
   
