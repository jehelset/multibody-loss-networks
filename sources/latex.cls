%
% latex.cls for Sphinx (https://www.sphinx-doc.org/)
%

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{latex}[2022/01/02 v0.0.0 Document class]

\PassOptionsToClass{openright}{latex}

\newif\if@oneside
\DeclareOption{oneside}{\@onesidetrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{latex}}
\ProcessOptions\relax

\if@oneside
\else
\PassOptionsToClass{twoside}{latex}
\fi

\LoadClass{book}

\setcounter{secnumdepth}{2}
\setcounter{tocdepth}{1}

\DeclareRobustCommand{\and}{%
    \end{tabular}\kern-\tabcolsep
    \allowbreak
    \hskip\dimexpr1em+\tabcolsep\@plus.17fil\begin{tabular}[t]{c}%
}

\newcommand{\sphinxmaketitle}{
  \let\sphinxrestorepageanchorsetting\relax
  \ifHy@pageanchor\def\sphinxrestorepageanchorsetting{\Hy@pageanchortrue}\fi
  \hypersetup{pageanchor=false}
  \begin{titlepage}%
    \let\footnotesize\small
    \let\footnoterule\relax
    \begingroup
    \def\endgraf{ }\def\and{\& }%
    \pdfstringdefDisableCommands{\def\\{, }}
    \hypersetup{pdfauthor={\@author}, pdftitle={\@title}}%
    \endgroup
    \begin{flushleft}
      \sphinxlogo \par
      \noindent\rule{\textwidth}{1pt}\par
      {\Huge \sphinxinstitution \par} 
      \py@HeaderFamily
      \vfill
      {\Huge \@title \par}
      \vfill
      {\LARGE
        \begin{tabular}[t]{c}
          \@author
        \end{tabular}\kern-\tabcolsep
        \par}
      \vfill\vfill
      {\large \sphinxsupervisor \hfill \@date \par }
    \end{flushleft}
    \@thanks
  \end{titlepage}
  \setcounter{footnote}{0}%
  \let\thanks\relax\let\maketitle\relax
  \clearpage
  \hspace{0pt}
  \vfill
  \begin{center}
    \sphinxacknowledgements
  \end{center}
  \vfill
  \hspace{0pt}
  \clearpage
  \sphinxabstract
  \if@openright\cleardoublepage\else\clearpage\fi
  \sphinxrestorepageanchorsetting
}

\newcommand{\sphinxtableofcontents}{%
  \pagenumbering{roman}%
  \begingroup
    \parskip \z@skip
    \sphinxtableofcontentshook
    \tableofcontents
  \endgroup
  % before resetting page counter, let's do the right thing.
  \if@openright\cleardoublepage\else\clearpage\fi
  \pagenumbering{arabic}%
}


% This is needed to get the width of the section # area wide enough in the
% library reference.  Doing it here keeps it the same for all the manuals.
%
\newcommand{\sphinxtableofcontentshook}{%
  \renewcommand*\l@section{\@dottedtocline{1}{1.5em}{2.6em}}%
  \renewcommand*\l@subsection{\@dottedtocline{2}{4.1em}{3.5em}}%
}

% Fix the bibliography environment to add an entry to the Table of
% Contents.
% For a report document class this environment is a chapter.
%
\newenvironment{sphinxthebibliography}[1]{%
  \if@openright\cleardoublepage\else\clearpage\fi
  % \phantomsection % not needed here since TeXLive 2010's hyperref
  \begin{thebibliography}{#1}%
  \addcontentsline{toc}{chapter}{\bibname}}{\end{thebibliography}}

% Same for the indices.
% The memoir class already does this, so we don't duplicate it in that case.
%
\@ifclassloaded{memoir}
 {\newenvironment{sphinxtheindex}{\begin{theindex}}{\end{theindex}}}
 {\newenvironment{sphinxtheindex}{%
    \if@openright\cleardoublepage\else\clearpage\fi
    \phantomsection % needed as no chapter, section, ... created
    \begin{theindex}%
    \addcontentsline{toc}{chapter}{\indexname}}{\end{theindex}}}
