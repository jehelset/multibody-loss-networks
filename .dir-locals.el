((nil
  (eval
   .
   (if
       (not (boundp 'multibody-loss-networks-loaded))
       (progn
         (setq projectile-project-configure-cmd "rm -rf __pycache__ public latex")
         (setq projectile-project-run-cmd "sphinx-autobuild -a sources public")
         (setq projectile-project-compilation-cmd "sphinx-build sources public")
	 (yas-reload-all)
	 (setq yas-snippet-dirs (append yas-snippet-dirs (list (concat (projectile-project-root) "snippets"))))
	 (yas-reload-all)
         (setq hysj-dir (file-name-as-directory (concat (file-name-directory (directory-file-name (projectile-project-root))) "hysj")))
         (setq hysj-builddir (concat hysj-dir "build"))
         (setq hysj-bindir (concat (file-name-as-directory hysj-builddir) "stage/bin"))
         (setq hysj-libdir (concat (file-name-as-directory hysj-builddir) "stage/lib"))
         (setq hysj-pydir (concat (file-name-as-directory hysj-libdir) "python3.10/site-packages"))
	 (setenv "CMAKE_BUILD_PARALLEL_LEVEL" "32")
         (setenv "PATH" (string-join `(,hysj-bindir
                                       ,hysj-libdir
                                       ,(getenv "PATH"))
                                     ":"))
         (setenv "PYTHONPATH" (concat hysj-pydir ":" hysj-bindir ":" hysj-libdir ":" (getenv "PYTHONPATH"))) 
         (setenv "LD_LIBRARY_PATH" (string-join `(,hysj-bindir
                                                  ,hysj-libdir
                                                  ,(getenv "LD_LIBRARY_PATH"))
                                                ":"))
         (setq python-shell-extra-pythonpaths (list hysj-bindir hysj-libdir))

         (setq multibody-loss-networks-loaded 1)))))
 (rst-mode .
  ((rst-preferred-adornments . ((35 over-and-under 0)
				(42 over-and-under 0)
				(61 over-and-under 0)
				(45 simple 0)
				(94 simple 0)
				(34 simple 0)
				(126 simple 0))))))
