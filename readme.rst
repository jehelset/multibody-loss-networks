Workbench for my Master-thesis at `NTNU <https://www.ntnu.no>`_.

Latest version available in `pdf <https://jehelset.gitlab.io/multibody-loss-networks/files/multibody-flow-networks.pdf>`_ and `html <https://jehelset.gitlab.io/multibody-loss-networks>`_.

Written in ReST with `Sphinx <https://www.sphinx-doc.org/en/master/>`_.

Layout
######

- `container <container>`_ - Definition of container used for running `automated jobs <.gitlab-ci.yml>`_.
- `documents <documents>`_ - Roadmap, notes, minutes, etc.
- `sources <sources>`_     - Sources for thesis.

Style
#####

- `#`, with overline, for title
- `*`, with overline, for parts
- `=`, with overline, for chapters
- `-`, for sections
- `^`, for subsections
- `"`, for subsubsections
- `~`, for paragraphs
