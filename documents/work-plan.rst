#########
Work Plan
#########

*******
Context
*******

The simulation models of hydropower production in the context of production planning that I have
worked with are not able to capture all relevant behaviour of the system that is modeled, and are to
some extent underspecified. As a consequence the model used for computation is split in two. It has
an explicit part defined in terms of a formal model, but also an implicit part, defined in terms of
an implementation. Ensuring that the *actual* model used for computation is formally specified is
useful since a formal specification is precise and unambiguous, and does not require implementation
specfic competence.

A simulation in the context of production planning should approximately reflect the trajectory of a
hydropower production system for some given production plans, inflow forecasts and reservoir
volumes, etc. The models I have worked with can be improved in at least two aspects: topological
expressiveness and water-body connectivity. The former pertains to the topological combinations of
tunnels, gates, and units that the model can represent (there are a lot of different hydropower
plants, and not all of them conform to the typical turnip-like layout), and the latter to the
assumption that tunnel-systems are always fully submerged, and that its water-body is connected to
each of the adjacent reservoirs.

To this end I will propose a hybrid system theoretical model, in the form of a hybrid automaton.
This formalism has f.ex. been used to model wastewater systems which are quite similar. The model
will represent multibody flow in a general network topology described in the form of a directed
acyclic graph.

*****
Goals
*****

- Create a hybrid automaton for multibody loss networks that meets the two challenges of topological
  complexity and waterbody connectivity

  - Analyze determinism, stability and zeno-behaviour of this model

- Show that the model can be used as a building block for both historical inflow estimation, and
  simulation.

  - Provide a definition of historical inflow estimation
  - Show that the continuous systems in these two computations are compatible with existing simulation software.

- Hold weekly meetings with supervisor whenever possible.

