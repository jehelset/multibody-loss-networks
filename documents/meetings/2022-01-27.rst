#######
Meeting
#######

Location: `Teams`

Date: `2022-01-27`

Time: `13:30`

Attendees: John Eivind Helset, Sverre Hendseth

*************
Status Update
*************

Have been focusing on the background, which currently is named the prologue (do I need to use more
common naming?). I have been trying to find some background papers on zeno-behaviour, and found a
very fitting paper in [Zhang2001]_, and also noticed it was treated somewhat in [Egeland2002]_
(specifically for friction systems).

Currently I wanted to first introduce some concepts related to modeling & computation (1.1), and
then go from there to modeling & computation in the context of the paper (1.2). Here I wanted to
drill down a bit into 3 relevant models ([Skjelbred2019]_, [Svarstad2014]_, [Ocampo2010]_) from
other papers, and then introduce the specific challenges that my model attempts to solve. The first
two papers examplify variation of the topological expressiveness of models, and the latter uses the
same formalism that I intend to use (and has the same topological expressiveness) to another type of
system (but does not consider waterbody connectivity). And then finally in (1.3) introduce the
hybrid system theoretical framework at the heart of the model.

I am struggling a bit to motivate my model and concepts. My approach and background is highly
practical, I arrived at this model mostly through working with other models, and the challenges that
they pose, not through academic work. So I am not quite sure to what extent and how I could actually
use my background.

**********************
Proposed Work Schedule
**********************

See `Work Plan <../work-plan.rst>`_ and **Fig 1.1** in [Helset2022]_. 

******************************
Potential Area of Contribution
******************************
===============
Hybrid Automata
===============

**Figure 1.** in [Zhang2001]_ is good example. At the top level we have a discrete system, an automaton, which models
the discrete dynamics of the model. The model is associated with a set of discrete variables, and each state corresponds
to a unique valuation. The depicted system has a single discrete variable :math:`q \in \{ q_1, q_2 \}`. Each location
also corresponds to a particular continuous model (a classical system theoretical model). In addition they typically
have an invariant set, which I don't see the point of and ignore.

The transitions model the switching between continuous dynamics. See video in `Remodeling a Flower
System <https://jehelset.gitlab.io/posts/remodeling-a-flower-system>`_ for animated example. A
transition from :math:`q0 \to q1` changes the discrete state by :math:`q1 - q0`. In my model I will
only work with autonomous automatons, which means that the symbols of the transitions are produced
by the continuous evolution of the system. Each symbol is guarded by a logical expression which
represents a set, whenever the continuous trajectory enters the set, the symbol is produced, and the
transition might be taken. See figure `Solving continuous problems with structured guard
<https://jehelset.gitlab.io/posts/structured-transition-guards/>`_ for hybrid continuous simulator
architecture & simulation technique. In this paper I introduce "immediate transitions", that is,
transitions whose guard is simply :math:`\top`, and is always true. So whenever the state enters a
location with an immediate transitions it can be taken.

See figure `An augmented simulation algorithm for Hybrid Models
<https://jehelset.gitlab.io/posts/lazy-hybrid-automata/>`_ for hybrid simulator architecture.

===========
Determinism
===========

A hybrid automaton is deterministic according to [Lunze2009]_:

  | Whenever both invariant and guard condition are simultaneously true, there is a choice
  | between continuous and discrete evolution. A transtion is therefore said to be deterministic,
  | if (1) it has a unique destination and (2) whenever this transition is possible, continuous
  | evolution is impossible.Determinism or nondeterminism of transition is hence a modeling
  | issue.


As mentioned I introduce "immediate transitions". So whever the state enters a location with more
than one immediate transition, we are by the above description in a state of non-determinism. In
addition the continuous evolution might be such that two transitions are available at any given time
(two tunnels fill up at the same time).

However, after transitively taking all the transitions that need be taken, we will end up in the
same location, regardless of the order one takes the transitions in. So the execution will be unique
modulo the order in which the transitions are taken.

==============
Zeno behaviour
==============

Definition of Zeno in [Zhang2001]_:

  | An excution is called *finite* if :math:`\tau` is a finite sequence ending with a compact interval,
  | it is called *infinite* if :math:`\tau` is either an infinite sequence of if :math:`\mathcal{T}(\mathcal{X}) = \infty`,
  | and it is called *Zeno* if it is infinite but :math:`\mathcal{T}(\mathcal{X}) < \inf`.
  
In the same vein as `Determinism`_, I want to show that the immediate transitions do not introduce Zeno-behaviour. That
there are no loops consisting of immediate transitions.

*****
Notes
*****

Work plan looks ok, but was supposed to have a timeline. It's still ok.

Feedback on structure of report. Technical report has 4 parts, intro, background, result, discussion. Background
and result can be multiple chapters. Maybe some of the issues wrt. to tone of background info can be amended
by moving more the more handwavy stuff into the intro, stating assumptions there, etc. Also educational-work
is admissible content.

Need to guide the reader to the goal. Recall again the nominal reader.

Focus rather on monotonically increasing background part, than finding area of contribution. Could try to
have a readable version sent in prep for meetings (maybe also specify which part has been worked on).

Next meeting 3/2 - 13:30 to 14:00.

************
Bibliography
************

.. [Helset2022] "John Eivind Helset", "Multibody Loss Networks"
.. [Zhang2001] "Zhang, Jun and Johansson, Karl Henrik and Lygeros, John and Sastry, Shankar", "Zeno hybrid systems"
.. [Egeland2002] "Egeland, Olav and Gravdahl, Jan Tommy", "Modeling and simulation for automatic control"
.. [Svarstad2014] "Svarstad, Magni Fjørtoft", "Pumpe- og turbinkarakteristikker i fire kvadranter"
.. [Ocampo2010] "Ocampo-Martinez, Carlos", "Model Predictive Control of Wastewater Systems"
.. [Skjelbred2019] "Skjelbred, Hans Ivar", "Unit-based short-term hydro scheduling in competitive electricity markers"
.. [Lunze2009] "Lunze, Jan and Lamnabhi-Lagarrigue, Françoise", "Handbook of Hybrid Systems Control"
