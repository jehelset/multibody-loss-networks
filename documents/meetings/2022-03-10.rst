#######
Meeting
#######

Location: `Teams`

Date: `2022-03-10`

Time: `13:30`

Attendees: John Eivind Helset, Sverre Hendseth

*************
Status Update
*************

Slow progress. Still not done with background! Wouldn't mind just working with background until
done. Sleep schedule was drifting a bit last period, so ended up working some night-shifts. However
seems to have rotated back into place now. 

Happy that I can see my model as sort of a combination of combining he flow networks of
[Jansen2014]_ with the notion of temporary flow paths of [Ocampo2010]_. Does seem to be original
development, or at least application, but maybe for a good reason (could be just a bad idea). Happy
with my rationale for introducing using the HA-formalism, since I think I have found corroborating
litterature for DAEs (and therefore HAs) being a good choice for a system models.

Not happy that I lack a lot of understanding of the DAE-framework. It will take practice (which
for me means implementation) to develop a good enough understanding to feel comfortable, and I can't
do that now. Not happy with the gap between conceptual and written results. As a consequence I am
adjusting my expectations, and expect to not be able to produce everything I wanted.

Currently jumping a bit back and forth between DAE and HA background material. In particular I need
to pull (and have a reasonable understanding of) definitions of continuous determinism & regularity
of a DAE. I want this because the simulation library I use ([Hindmarsh2005]_) needs it, and I want
to see if [Jansen2015]_ can establish global unique solvability for my model, and because I want to
relax the concept of hybrid determinism in my results section, and try to show that my model is
deterministic in this relaxed sense. This latter point does not really require a detailed
understanding as it is mostly a discrete result. If I complete determinism & regularity, and put
stability on ice for now, then I feel I can shift main focus to results.

Dropped most appendices for now, think they are mostly for the nominal reader. Might re-introduce
later when required. Tentative appendix on simulation technique, to describe how examples were
simulated, etc. Not sure how to attach code, since it's a rather large library that I am
using. Maybe can just attach the scripts generating diagrams and link to the gitlab of the library?

*****
Notes
*****

Consider separating discussion and conclusion. Conclusion could be 3-4 well presented bullets.

What is the relation between bond-graphs and DAE / HA? 

************
Next Meeting
************

Will be in Trondheim next week. Need to put in some hours at work, so I don't expect too much
progress, but will try to find 20 hours at least. Would be nice to use the opportunity to have an
in-person meeting if possible.

`2022-03-16` at `14:30`!

************
Bibliography
************

.. [Jansen2014] "Jansen, Lennart and Tischendorf, Caren", "A Unified (P)DAE Modeling Approach for Flow Networks"
.. [Jansen2015] "Jansen, Lennart and Matthes, Michael and Tischendorf, Caren", "Global unique solvability for memristive circuit DAEs of Index 1"
.. [Hindmarsh2005] "Hindmarsh, Alan", "SUNDIALS: Suite of nonlinear and differential/algebraic equation solvers"
.. [Ocampo2010] "Ocampo-Martinez, Carlos", "Model Predictive Control of Wastewater Systems"
