latex-build:
	mkdir -p latex
	sphinx-build -b latex sources latex
	cd latex && $(MAKE)

latex-clean:
	rm -rf latex

latex-show:
	xdg-open latex/multibody-flow-networks.pdf &

latex: latex-clean latex-build latex-show

figures-results:
	python sources/results/figures.py
figures: figures-results
