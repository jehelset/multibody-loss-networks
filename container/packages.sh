#!/bin/sh
set -e 

declare -a packages=(
    git graphviz curl
    texlive-core texlive-fontsextra texlive-latexextra
    python python-sphinx python-sphinx-alabaster-theme python-sphinxcontrib-jsmath python-sphinxcontrib-bibtex)

sudo pacman -S --noconfirm --needed ${packages[@]}
sudo pacman -Scc --noconfirm
