#!/bin/sh
set -e 

declare -a aur_packages=(python-sphinxcontrib-svg2pdfconverter)

tmpdir=$(mktemp -d)
pushd ${tmpdir}

for package in ${aur_packages[@]}
do
    rm -rf ${package}
    curl -L https://aur.archlinux.org/cgit/aur.git/snapshot/${package}.tar.gz | tar -xz
    pushd ${package}
    makepkg --noconfirm -scfir
    popd
done

popd
rm -rf ${tmpdir}
